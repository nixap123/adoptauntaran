<?php
require_once 'core/core.php';
require_once 'services/User.php';

if (Request::isGet() && empty($_GET['code']))
{
    Url::redirect('login.php');
}

$user_sercice = new User();

$user = reset($user_sercice->getWhere(['RecoveryCode' => $_GET['code']], 1));

if (empty($user['ID']))
{
    Url::redirect('login.php');
}

if (isset($_POST['Save']))
{
    $Password = Validator::validate('Password', Validator::ValidateEmpty, 'NewPassword');
    $PasswordConfirmation = Validator::validate('PasswordConfirmation', Validator::ValidateEmpty, 'RepeatNewPassword');
    
    if (!Validator::hasErrors())
    {
        if ($Password !== $PasswordConfirmation)
        {
            Validator::setError('Password != PasswordConfirmation');
        }
    }
    
    if (Validator::hasErrors())
    {
        exit(json_encode([
            'result' => '',
            'errors' => Validator::showMessages()
        ]));
    }
    else
    {
        $user_sercice->update(['Password' => $Password, 'RecoveryCode' => ''], ['ID' => $user['ID']]);
        
        Validator::setSuccess(Lang::t('YouPasswordHasBeenChanged.YouCan') . "<a href=\"" . Url::link('login.php') . "\">" . Lang::t('LoginWithNewPassword') ."</a>");
        exit(json_encode([
            'errors' => '',
            'result' => Validator::showMessages()
        ]));
    }
}

require_once 'template/head.php';
?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('PasswordReset') ?></h2>
        <div class="row">
            <form id="login-form" class="login-form col-md-6 form-horizontal" method="post">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= Lang::t('NewPassword') ?></label>
                        <div class="col-md-9">
                            <input type="password" name="Password" placeholder="*********" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= Lang::t('ConfirmPassword') ?></label>
                        <div class="col-md-9">
                            <input type="password" name="PasswordConfirmation" placeholder="*********" />
                        </div>
                    </div>
                </div>
                <input type="hidden" name="Save" value="1" />
                <div class="form-group text-right">
                    <input id="login-button" value="Salvati" class="btn btn-danger" /><br /><br />
                </div>
                <div class="form-group text-center">
                    <p><a href="<?= Url::link('/termeni')?>"><?= Lang::t('TermsAndConditions') ?></a></p>			
                    <p><a href="<?= Url::link('/confidentialitate') ?>"><?= Lang::t('PrivacyPolicy') ?></a></p>
                </div>
            </form>
        </div>
    </div>
</section>	


<section class="home-block">
    <div class="container">
        <a href="<?= Url::link('/register') ?>" class="arrows-btn">
            <?= Lang::t('RegisterHere') ?><br />
            <?= Lang::t('AdoptAPeasant') ?>!
        </a>
    </div>
</section>

<script>

    var Login = {
        doLogin: function ()
        {
            $('.alert').remove();
            $('.has-error').removeClass('has-error');

            var valid = true;
            var form = $('#login-form');

            if (form.find('input[name=Password]').val() == "")
            {
                form.find('input[name=Password]').closest('label').addClass('has-error');
                valid = false;
            }

            if (form.find('input[name=PasswordConfirmation]').val() == "")
            {
                form.find('input[name=PasswordConfirmation]').closest('label').addClass('has-error');
                valid = false;
            }

            if (valid)
            {
                $.post('<?= Url::link('password_reset.php', ['code' => $_GET['code']]) ?>', form.serialize(), function (json) {
                    if (json.errors === '')
                    {
                        form.before(json.result);
                        form.trigger('reset');
                    }
                    else
                    {
                        form.before(json.errors);
                    }
                }, 'json');
            }

            return false;
        }

    };

    $(document).ready(function () {
        $('#login-button').click(Login.doLogin);
    });

</script>

<?php require_once 'template/footer.php'; ?>