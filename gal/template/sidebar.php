<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <a href="">
            <!--img src="<?= Url::link('img/logo.jpg') ?>" class="img-responsive"-->
        </a>
        <br><br>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="">
            <a href="<?= Url::link('admin/index.php') ?>">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="<?= Url::link('admin/producatori/index.php') ?>">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> GAL / Producatori </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="<?= Url::link('admin/produse/index.php') ?>">
                <i class="menu-icon fa fa-barcode"></i>
                <span class="menu-text"> Produse </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="<?= Url::link('admin/setari/index.php') ?>">
                <i class="menu-icon fa fa-cogs"></i>
                <span class="menu-text"> Setari </span>
            </a>

            <b class="arrow"></b>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>