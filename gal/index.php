<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';


$user_service = new User();

$producatori = $user_service->getProducatoriOfGal($_SESSION['UserID']);

if (isset($_GET['deleteID']))
{
    $user_service->deleteUserByID((int)$_GET['deleteID']);
    Url::redirect('admin/producatori/index.php');
}

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/template/head.php'; ?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Manufacturers') ?></h2>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <a href="<?= Url::link('gal/edit') ?>" class="red-btn"><i class="fa fa-plus"></i> <?= Lang::t('Add') ?></a>
                    <hr />
                    <table class="table table-striped table-responsive dataTable">
                        <thead>
                            <tr>
                                <th><?= Lang::t('ManufacturerGal') ?></th>
                                <th><?= Lang::t('Type') ?></th>
                                <th><?= Lang::t('County') ?></th>
                                <th class="hidden"><?= Lang::t('Location') ?></th>
                                <th><?= Lang::t('Phone') ?></th>
                                <th><?= Lang::t('Products') ?></th>
                                <th><?= Lang::t('Rating') ?></th>
                                <th><?= Lang::t('Comments') ?></th>
                                <th><?= Lang::t('Edit') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($producatori as $producator) { ?>
                            <tr>
                                <td><a href="<?= Url::link('gal/edit.php', ['id' => $producator['ID']]) ?>" target="_blank"><?= $producator['Name'] ?></a></td>
                                <td><?= $producator['Type'] ?></td>
                                <td><?= $producator['JudetName'] ?></td>
                                <td class="hidden"><?= $producator['CityName'] ?></td>
                                <td><?= $producator['Phone'] ?></td>
                                <td><?= $producator['Products'] ?></td>
                                <td><?= $producator['Rating'] ?></td>
                                <td><?= $producator['Comments'] ?></td>
                                <td>
                                    <a href="<?= Url::link('gal/edit.php', ['id' => $producator['ID']]) ?>" class="btn btn-xs"><i class="fa fa-pencil"></i></a>
                                    <?php if (empty($_SESSION['TempUserID'])) { ?>
                                    <a href="<?= Url::link('gal/account_view.php', ['id' => $producator['ID']]) ?>" class="btn btn-xs"><i class="fa fa-play"></i></a>
                                    <?php } ?>
                                    <a onclick="return confirm('Confirm action?')" href="<?= Url::link('gal/index.php', ['deleteID' => $producator['ID']]) ?>" class="btn btn-xs"><i class="fa fa-trash"></i></a>
                                    <?php if ($producator['Type'] == User::TypeGal) { ?>
                                    <a href="<?= Url::link('gal/edit.php', ['galID' => $producator['ID']]) ?>" class="btn btn-xs"><i class="fa fa-plus-square"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>	
                        </tbody>
                    </table>



                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </div>
</section><!-- /.main-content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/template/footer.php'; ?>