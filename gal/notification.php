<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Notification.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$notification_service = new Notification();

$notification_list = $notification_service->getList($_SESSION['UserID']);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Notification') ?> <span class="notif-count unread-notifications"><?= $unread_notifications_count ?></span></h2>
        <div class="row">
            <div class="col-md-12 notification-inn">
                <div class="row">
                    <?php foreach ($notification_list as $type => $notifications) { ?>
                    <div class="notification-item col-md-4">
                        <?php foreach ($notifications as $notification) { ?>
                        <a class="bg-<?= strtolower($type) ?>">
                            <i class="fa fa-<?= $notification['Category'] ?>"></i> 
                            <strong><?= $notification['Title'] ?></strong> 
                            <span><?= DateHelper::toDMYHI($notification['Date']) ?></span>
                            <?php if ($notification['Unread'] == 1) { ?>
                            <span class="text-danger text-right"><?= Lang::t('Unread') ?> (<click onclick="readNotification(<?= $notification['ID'] ?>, this)" style="cursor: pointer;"><?= Lang::t('MakeAsReaded') ?></click>)</span>
                            <?php } ?>
                        </a>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</section>	
<script>

    function readNotification(id, elem)
    {
        $.post('<?= Url::link('ajax/notification/read.php') ?>', {id: id}, function(){
            $(elem).closest('span').remove();
            $('.unread-notifications').each(function(){
                $(this).text(parseInt($(this).text()) - 1);
            });
        });
    }

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/template/footer.php'; ?>