<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

$user_service = new User();

$gal = $user_service->getByID($_SESSION['UserID']);

if (empty($gal['ID']) || !in_array($gal['Type'], [User::TypeGal, User::TypeAdmin]))
{
    Url::redirect();
}

$userID = (int)$_GET['id'];

$producator = $user_service->getByID($userID);

if ($gal['ID'] != $producator['ParentID'] && $gal['Type'] != User::TypeAdmin)
{
    Url::redirect();
}

$_SESSION['TempUserID'] = $_SESSION['UserID'];
$_SESSION['UserID'] = $producator['ID'];

if ($producator['Type'] == User::TypeGal)
{
    Url::redirect('gal/index.php');
}
else
{
    Url::redirect('producator/index.php');
}