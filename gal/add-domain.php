<div id="add-domain">
    <div class="form-group">
        http://www.<input <?= empty($value) ? '' : 'readonly' ?> style="padding-left: 1px;" value="<?= $value ?>" type="text" name="settings[<?= $group ?>][<?= $settingName ?>]" placeholder="test-domain.com" />
    </div>
</div>

<script>

    $('#add-domain').closest('form').submit(function(event){
        
        <?php if (!empty($value)) { ?>
        return true; 
        <?php } ?>
        
        var domain = $('#add-domain input[type=text]').val().toLowerCase();
        
        if ($.trim(domain) !== "")
        {
            var pattern = new RegExp(/^[a-z0-9][a-z0-9-]+[a-z0-9]\.[a-z]{2,6}$/);
            
            var valid = pattern.test(domain);
            
            if (valid)
            {
                if (confirm('<?= Lang::t('ConfirmatiDomen') ?>: ' + domain))
                {
                    $('#add-domain').append('<input type="hidden" name="addDomain" value="1" />');
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                alert('<?= Lang::t('IncorrectNumeDomen') ?>');
                return false;
            }
        }
        
        return true;
    });

</script>