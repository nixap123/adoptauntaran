<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

$user_service = new User();

$gal = $user_service->getByID($_SESSION['TempUserID']);

if (empty($gal['ID']) || !in_array($gal['Type'], [User::TypeGal, User::TypeAdmin]))
{
    Url::redirect('logout.php');
}

$_SESSION['UserID'] = $_SESSION['TempUserID'];
unset($_SESSION['TempUserID']);

if ($gal['Type'] == User::TypeAdmin)
{
    Url::redirect('admin/producatori/index.php');
}
elseif ($gal['Type'] == User::TypeGal)
{
    Url::redirect('gal/index.php');
}

Url::redirect('logout.php');