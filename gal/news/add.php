<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/News.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/NewsLang.php';

$news_service = new News();
$news_lang_service = new NewsLang();

$newsID = (int)$_GET['id'];

if ($newsID > 0)
{
    $news = $news_service->getWhere(['ID' => $newsID], 1);
    $news = reset($news);
    
    if (empty($news['ID']))
    {
        Url::redirect('gal/news/add.php', ['id' => 0]);
    }
    
    $Date = DateHelper::toDMY($news['Date']);
    
    $newsLang = $news_lang_service->getByNewsID($newsID);
    
    foreach ($newsLang as $langID => $row)
    {
        $titles[$langID] = $row['Title'];
        $texts[$langID] = $row['Text'];
    }
}


if (Request::isPost())
{
    $titles = Request::post('title');
    $texts = Request::post('text');
    
    $title_ro = $titles[1];
    $text_ro = $texts[1];
    
    $date = Validator::validate('Date', Validator::ValidateEmpty, Lang::t('DataPublicarii'));
    
    if (empty($title_ro))
    {
        Validator::setError('Completati titlu pentru limba romana');
    }
    
    if (empty($text_ro))
    {
        Validator::setError('Completati text pentru limba romana');
    }
    
    if (!Validator::hasErrors())
    {
        $newsData = [];
        $newsData['Date'] = date('c', strtotime($date));
        
        if ($newsID > 0)
        {
            $news_service->update($newsData, ['ID' => $newsID]);
        }
        else
        {
            $newsID = $news_service->insert([
                'UserID' => $_SESSION['UserID'],
                'IsCommon' => 0,
                'Date' => $newsData['Date']
            ]);
        }
        
        $news_lang_service->delete(['NewsID' => $newsID]);
        
        foreach ($titles as $langID => $name)
        {
            $news_lang_service->insert([
                'NewsID' => $newsID,
                'LangID' => $langID,
                'Title' => isset($titles[$langID]) ? $titles[$langID] : '',
                'Text' => isset($texts[$langID]) ? $texts[$langID] : ''
            ]);
        }
        
        $_SESSION['saved'] = true;
        Url::redirect('gal/news/index.php');
    }
    
    if (!empty($_SESSION['saved']))
    {
        unset($_SESSION['saved']);
        Validator::setSuccess();
    }
}


require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/template/head.php';
?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('AddNews') ?></h2>
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <form method="post">
            <div class="form-group">
                <label class="control-label"><?= Lang::t('PublishedDate') ?></label>
                <input type="text" name="Date" value="<?= empty($Date) ? DateHelper::toDMY(date('c')) : $Date ?>" class="form-control datepicker" />
            </div>
            
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?php foreach (Config::$LangList as $langID => $lang) { ?>
                <li role="presentation" class="<?= reset(Config::$LangList) == $lang ? 'active' : '' ?>"><a href="#tab-<?= $langID ?>" aria-controls="tab-<?= $langID ?>" role="tab" data-toggle="tab"><?= $lang['name'] ?></a></li>
                <?php } ?>
            </ul>
            <br />
            <!-- Tab panes -->
            <div class="tab-content">
                <?php foreach (Config::$LangList as $langID => $lang) { ?>
                <div role="tabpanel" class="tab-pane <?= reset(Config::$LangList) == $lang ? 'active' : '' ?>" id="tab-<?= $langID ?>">
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Title') ?></label>
                        <input type="text" name="title[<?= $langID ?>]" value="<?= isset($titles[$langID]) ? $titles[$langID] : '' ?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Content') ?></label>
                        <textarea class="summer-note form-control" name="text[<?= $langID ?>]"><?= isset($texts[$langID]) ? $texts[$langID] : '' ?></textarea>
                    </div>
                </div>
                <?php } ?>
            </div>
            <br />
            <div>
                <button type="submit" class="btn btn-primary"><?= Lang::t('Save') ?></button>
            </div>
        </form>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/gal/template/footer.php'; ?>