<?php
require_once 'core/core.php';
require_once 'services/User.php';
require_once 'services/Judet.php';
require_once 'services/UserInfo.php';
require_once 'services/Bank.php';

$judet_service = new Judet();
$bank_service = new Bank();
$user_info_service = new UserInfo();

$judet_list = $judet_service->getAll();
$banks_list = $bank_service->getAll();

$judets = [];
foreach ($judet_list as $judet) {
    $judets[$judet['ID']] = $judet['Name'];
}

$banks = [];
foreach ($banks_list as $bank) {
    $banks[$bank['ID']] = $bank['Name'];
}

if (Request::isPost()) {
    $Email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    $Password = Validator::validate('Password', Validator::ValidateEmpty, 'Password');

    $ClientType = Validator::validate('ClientType', Validator::ValidateEmpty, 'ClientType');
    $AccountType = Validator::validate('AccountType', Validator::ValidateEmpty, 'InteresedIn');
    $Name = Validator::validate('Name', Validator::ValidateEmpty, 'Name');
    $Phone = Validator::validate('Phone', Validator::ValidateEmpty, 'Phone');

    $Address = Validator::validate('Address', Validator::ValidateEmpty, 'Address');
    $Judet = Validator::validate('Judet', Validator::ValidateEmpty, 'County');
    $Location = Validator::validate('Location', Validator::ValidateEmpty, 'Location');
    $PasswordConfirmation = Validator::validate('PasswordConfirmation', Validator::ValidateEmpty, 'PasswordConfirmation');
    $Accept = Validator::validate('Accept', Validator::ValidateEmpty, 'Accept');

    if ($ClientType == User::ClientTypePF) {
        $Cnp = Validator::validate('Cnp', Validator::ValidateEmpty, 'CNP');
    } else {
        $Cui = Validator::validate('Cui', Validator::ValidateEmpty, 'CUI/CIF');
        $BankAccount = Validator::validate('BankAccount', Validator::ValidateEmpty, 'BankAccount');
        $BankID = Validator::validate('BankID', Validator::ValidateNumber, 'Bank');
        $RegNumber = Validator::validate('RegNumber', Validator::ValidateEmpty, 'CompanyRegistratinNumber');
    }

    if (!Validator::hasErrors()) {
        Validator::validate_equal('Password', 'PasswordConfirmation', 'Password', 'PasswordConfirmation');

        if (!Validator::hasErrors()) {
            $user_service = new User();

            $user = $user_service->getWhere(['UserName' => $Email, 'Status' => User::StatusActive], 1);

            if (isset($user[0]['ID'])) {
                Validator::setError('EmailIsAlreadyRegistered');
            }
        }

        if (!Validator::hasErrors()) {

            $Photo = Upload::file('Photo', 'uploads/profile', ['jpg', 'png']);

            $userID = $user_service->insert([
                'UserName' => $Email,
                'Password' => $Password,
                'Type' => $AccountType,
                'ClientType' => $ClientType,
                'Status' => User::StatusActive
            ]);

            $user_info_service->insert([
                'UserID' => $userID,
                'Name' => $Name,
                'Phone' => $Phone,
                'Photo' => $Photo,
                'Address' => $Address,
                'JudetID' => $Judet,
                'LocationID' => $Location,
                'CNP' => $Cnp,
                'CUI' => $Cui,
                'RegNumber' => $RegNumber,
                'BankAccount' => $BankAccount,
                'BankID' => $BankID
            ]);

            Url::redirect('locations.php', ['id' => $userID]);
        }
    }
}
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('Suggest') ?></h2>
        <div class="row">
            <form id="login-form" class="login-form col-md-12 form-horizontal" method="post">

<?= Validator::showMessages() ?>
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Name') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="Name" value="<?= Request::post('Name') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Email') ?></label>
                                <div class="col-md-8">
                                    <input required type="email" name="Email" value="<?= Request::post('Email') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Phone') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="Phone" value="<?= Request::post('Phone') ?>"  />
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('ProfileImage') ?></label>
                                <div class="col-md-8">
                                    <input type="file" name="Photo" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('DescriptionOfFerm') ?></label>
                                <div class="col-md-8">
                                    <textarea rows="4" name="Desc">  </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Address') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="Address" value="<?= Request::post('Address') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('County') ?></label>
                                <div class="col-md-8">
<?= Html::form_dropdown('Judet', [0 => '- '. Lang::t('SelectCounty') .' -'] + $judets, '', 'onchange="Register.judetChange(this)"') ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Location') ?></label>
                                <div class="col-md-8">
<?= Html::form_dropdown('Location', ['0' => '- '. Lang::t('SelectLocation') .' -'], '', '') ?>
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('DeliveryIn') ?></label>
                                <div class="col-md-8">
                                    <input  type="text" name="Livrare" value="<?= Request::post('Livrare') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('WebSite') ?></label>
                                <div class="col-md-8">
                                    <input  type="text" name="PWeb" value="<?= Request::post('PWeb') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Facebook</label>
                                <div class="col-md-8">
                                    <input  type="text" name="Facebook" value="<?= Request::post('Facebook') ?>"  />
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <button type="submit" class="arrows-btn">
                    <?= Lang::t('Add') ?><br />
                    <?= Lang::t('Peasant') ?>
                </button>

                <div class="form-group text-center">			
                    <p><a href="<?= Url::link('/termeni')?>"><?= Lang::t('TermsAndConditions') ?></a></p>			
                    <p><a href="<?= Url::link('/confidentialitate') ?>"><?= Lang::t('PrivacyPolicy') ?></a></p>
                </div>
            </form>
        </div>
    </div>
</section>	



<script>

    var Register = {
        setFormPF: function ()
        {
            $('#cnp').show().find('input').attr('required', 'required');
            $('#cui, #reg_number, #banka, #cont_bancar, #pers_contact').hide().find('input').removeAttr('required');
        },
        setFormPJ: function ()
        {
            $('#cnp').hide().find('input').removeAttr('required');
            $('#cui, #reg_number, #banka, #cont_bancar, #pers_contact').show();
            $('#cui input').attr('required', 'required');
        },
        judetChange: function (select)
        {
            $.post('<?= Url::link('ajax/city-by-judet.php') ?>', {JudetID: $(select).val()}, function (html) {
                $('select[name=Location]').html(html);
            });
        }

    };

<?= Request::post('ClientType') == 'PJ' ? '$("input[name=ClientType]).click()' : 'Register.setFormPF();' ?>

</script>
<style>
    #cnp, #cui, #reg_number {display: none;}
</style>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>