<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php';     
?>


<section class="home-block">
    <div class="container">
        <h2 class="text-left center-title">Ultimele articole în presă</h2>
        <div class="row">
            <div class="news-item col-md-4">
                <a href="/news/view">
                    <h2>
                        "Adopta un taran", initiativa prin care orasenii au pornit in cautarea gustului pierdut al legumelor si fructelor
                    </h2>
                    <span>
                        Micii producatori cu gospodarii simple si produse sanatoase incep sa recastige respectul pe care il merita. Satui de fructe si legume fara gust, cei d...
                    </span>
                </a>
            </div>
            <div class="news-item col-md-4">
                <a href="/news/view">
                    <h2>
                        Campania „Adoptă un țăran!”, inițiativa inedită pentru salvarea producătorilor din mediul rural
                    </h2>
                    <span>
                        Preocuparea pentru o alimentaţie sănătoasă şi cât mai naturală a fost punctul de plecare pentru un proiect ce are deja succes. "Adoptă un ţă...
                    </span>
                </a>
            </div>
            <div class="news-item col-md-4">
                <a href="/news/view">
                    <h2>
                        „Adoptă un ţăran”, campania care sprijină micii fermieri din România
                    </h2>
                    <span>
                        La începutul lunii martie s-a născut pe Facebook proiectul „ Adoptă un ţăran ”, la iniţiativa Asociației Creștem România Împreună din B...
                    </span>
                </a>
            </div>
            <div class="news-item col-md-4">
                <a href="/news/view">
                    <h2>
                        "Adopta un taran", initiativa prin care orasenii au pornit in cautarea gustului pierdut al legumelor si fructelor
                    </h2>
                    <span>
                        Micii producatori cu gospodarii simple si produse sanatoase incep sa recastige respectul pe care il merita. Satui de fructe si legume fara gust, cei d...
                    </span>
                </a>
            </div>
            <div class="news-item col-md-4">
                <a href="/news/view">
                    <h2>
                        Campania „Adoptă un țăran!”, inițiativa inedită pentru salvarea producătorilor din mediul rural
                    </h2>
                    <span>
                        Preocuparea pentru o alimentaţie sănătoasă şi cât mai naturală a fost punctul de plecare pentru un proiect ce are deja succes. "Adoptă un ţă...
                    </span>
                </a>
            </div>
            <div class="news-item col-md-4">
                <a href="/news/view">
                    <h2>
                        „Adoptă un ţăran”, campania care sprijină micii fermieri din România
                    </h2>
                    <span>
                        La începutul lunii martie s-a născut pe Facebook proiectul „ Adoptă un ţăran ”, la iniţiativa Asociației Creștem România Împreună din B...
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>