<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
    
    if (empty($_SESSION['UserID']))
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php'; 
    }
    
    $user_service = new User();
    
    $USER = $user_service->getByID($_SESSION['UserID']);
    
    if ($USER['Type'] != User::TypeProducator)
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/head.php'; 
    }