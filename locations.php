<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserLocation.php';

    $user_service = new User();
    $user_info_service = new UserInfo();
    $city_service = new City();
    $user_location_service = new UserLocation();

    $user_info = $user_info_service->getByUserID((int)$_GET['id']);
    $user = $user_service->getByID($user_info['UserID']);
    
    if (empty($user_info['ID']) || empty($user['ID']))
    {
        Url::redirect('register.php');
    }
    
    if (Request::isPost())
    {
        $locations = $_POST['locations'];
        
        $user_location_service->delete(['UserID' => $user['ID']]);
        
        if (!Validator::is_empty($locations))
        {
            foreach ($locations as $locationID)
            {
                $user_location_service->insert([
                    'UserID' => $user_info['UserID'],
                    'LocationID' => $locationID
                ]);
            }
        }
        else
        {
            $location = $city_service->getByID($user_info['LocationID']);
            
            $nearest_cities = $city_service->getNearestCities($location['Lat'], $location['Long'], empty($user_info['Coverage']) ? 50 : $user_info['Coverage']);
            
            $insert = [];
            foreach ($nearest_cities as $row)
            {
                $insert[] = [
                    'UserID' => $user['ID'],
                    'LocationID' => $row['ID']
                ];
            }
            
            $user_location_service->insert_batch($insert);
        }
        
        if ($user['Type'] == User::TypeProducator)
        {
            Url::redirect('producator_info.php', ['id' => $user['ID']]);
        }
        else
        {
            Url::redirect('client/index');
        }
    }

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php'; ?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('Locations') ?></h2>
        <div class="row">
            <form class="login-form col-md-6 form-horizontal" method="post">
				<div class="loc-text text-center">
					Introdu localitatile unde doriti sa livrati exclusiv. <br />
					In cazul in care nu completati acest camp, platforma va genera <br />
					automat localitatile pe o raza de 50km de dumneavoastra<br /><br /><br />
				</div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= Lang::t('AddLocations') ?></label>
                        <div class="col-md-9">
                             <?= Html::form_dropdown('locations[]', [], [], 'multiple  id="localitati-choose" style=""') ?>
                            <input type="hidden" name="location-save" value="1" />
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <input type="submit" value="Salvează" class="btn btn-danger" style="width: 240px; display: inline-block !important;" /><br /><br />
                </div>
                
            </form>
        </div>
    </div>
</section>


	<script>
		$('#localitati-choose').select2({
                     ajax: {
                        url: "<?= Url::link('ajax/city-by-name-search.php') ?>",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                          return {
                            q: params.term, // search term
                            locationID: <?= $user_info['LocationID'] ?>,
                            distance: <?= $user_info['Coverage'] ?>,
                            page: params.page
                          };
                        },
                        processResults: function (data, params) {
                          // parse the results into the format expected by Select2
                          // since we are using custom formatting functions we do not need to
                          // alter the remote JSON data, except to indicate that infinite
                          // scrolling can be used
                          params.page = params.page || 1;
                          
                          console.log(data);

                          return {
                            results: data,
                            pagination: {
                              more: (params.page * 30) < data.total_count
                            }
                          };
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });
	</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>