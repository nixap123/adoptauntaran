<?php
require_once 'core/core.php';
require_once 'services/User.php';

$user_sercice = new User();

if (isset($_POST['Login'])) {
    $user = $user_sercice->doLogin($_POST['UserName'], $_POST['Password']);

    if (empty($user['ID'])) {
        Validator::setError('Incorrect username or password');
        exit(json_encode([
            'result' => '',
            'errors' => Validator::showMessages()
        ]));
    } else {
        exit(json_encode([
            'errors' => '',
            'result' => '<script>window.location.href="' . $user_sercice->accountLink($user) . '";</script>'
        ]));
    }
}

require_once 'template/head.php';
?>

<section class="home-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <!-- Cos Saptamanal -->	
                <h2 class="text-left center-title margin-10">Săptămâna aceasta</h2>
                <div class="week-cart-block">
                    <div class="row">
                        <div class="col-md-2 week-cart-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="col-md-5">
                            <strong>Coșul săptămânal conține:</strong>
                            <ul>
                                <li>1kg cas vaca</li>
                                <li>10 oua</li>
                                <li>2 legaturi patrunjel</li>
                                <li>15 verze</li>
                                <li>1kg carne porc</li>	
                            </ul>
                        </div>
                        <div class="col-md-5 week-cart-price">
                            <span>193 <sup>lei</sup></span>
                            <a href=""><i class="fa fa-plus-circle"></i> adaugă în coș</a>
                        </div>
                    </div>
                </div>

                <!-- Lista Produse -->					
                <h2 class="text-left center-title margin-10">Produsele zonei</h2>
                <div class="profile-products">
                    <ul>
                        <li><a data-toggle="collapse" href="#cat-branzeturi"><i class="fa fa-plus-circle"></i> Brânzeturi</a>
                            <ul class="collapse in" id="cat-branzeturi">
                                <li class="product-item">
                                    <strong>telemea oaie</strong>
                                    <span>10 lei / kg</span>
                                    <input type="number" name="" value="1">
                                    <a href=""><i class="fa fa-plus-circle"></i></a>
                                </li>
                                <li class="more-prods">
                                    <a data-toggle="collapse" href="#cat-zmeura"><i class="fa fa-plus-circle"></i></a>
                                    <strong>zmeura</strong>
                                    <span>10 - 15 lei / kg</span>
                                    <i>2 oferte</i></span>
                                    <ul class="collapse" id="cat-zmeura">											
                                        <li class="product-item">
                                            <strong>caș oaie</strong>
                                            <span>12 lei / kg</span>
                                            <input type="number" name="" value="1">
                                            <a href=""><i class="fa fa-plus-circle"></i></a>
                                        </li>
                                        <li class="product-item">
                                            <strong>telemea oaie</strong>
                                            <span>19 lei / kg</span>
                                            <input type="number" name="" value="1">
                                            <a href=""><i class="fa fa-plus-circle"></i></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="product-item">
                                    <strong>caș oaie</strong>
                                    <span>12 lei / kg</span>
                                    <input type="number" name="" value="1">
                                    <a href=""><i class="fa fa-plus-circle"></i></a>
                                </li>
                                <li class="product-item">
                                    <strong>telemea oaie</strong>
                                    <span>19 lei / kg</span>
                                    <input type="number" name="" value="1">
                                    <a href=""><i class="fa fa-plus-circle"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li><a href=""><i class="fa fa-plus-circle"></i> Legume</a></li>
                        <li><a href=""><i class="fa fa-plus-circle"></i> Fructe</a></li>
                    </ul>
                </div>					

                <!-- Zile Livare -->
                <h2 class="text-left center-title margin-10">Zile livrare</h2>
                <table class="table">
                    <tr><td><strong>Luni:</strong></td><td>Brașov, Făgăraș, Victoria</td></tr>
                    <tr><td><strong>Marți:</strong></td><td>Săcele, Sinaia</td></tr>
                    <tr><td><strong>Miercuri:</strong></td><td></td></tr>
                    <tr><td><strong>Joi:</strong></td><td></td></tr>
                    <tr><td><strong>Vineri:</strong></td><td>Brașov</td></tr>
                    <tr><td><strong>Sâmbătă:</strong></td><td>București</td></tr>
                    <tr><td><strong>Duminică:</strong></td><td></td></tr>
                </table>

                <!-- Rating -->
                <h2 class="text-left center-title margin-10">Rating Ion Marin</h2>
                <div class="profile-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>

                    <strong>4.3 <small>(412 voturi)</small></strong>
                </div>
            </div>

            <div class="col-md-6">

                <!-- Despre -->	
                <div class="profile-desc">
                    <h2 class="text-left center-title margin-10">Despre Ion Marin</h2>
                    <p><a>Aparține de GAL #3</a></p>
                    <p>Acum ceva vreme am primit un mesaj pe pagina noastră – fiul ne-a povestit despre părinții săi şi despre lucrurile speciale pe care le fac ei. Aşa am ajuns să aflăm despre satul Galeşu, din jud. Argeş. Satul este situat într-o zonă de poveste, la poalele Munților Făgăraş, partea sudică, lângă Valea Vâlsanului, arie protejată de interes național, care cuprinde peste 300 de specii de plante şi animale ocrotite de lege. În râul Vâlsan, ce străbate satul, a supraviețuit un peşte care nu se mai găseşte nicăieri altundeva în lume, fiind contemporan cu dinozaurii şi supraviețuind de mulți ani – aspretele.</p>
                    <a href="">vezi mai mult</a>
                </div>					

                <!-- Gallery -->
                <h2 class="text-left center-title margin-10">Galerie foto Ion Marin</h2>
                <div class="profile-gallery">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/1.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/2.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/3.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/4.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/1.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/2.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/3.jpg">
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                                <img src="images/items/4.jpg">
                            </a>
                        </div>
                    </div>		
                </div>	

                <!-- Contact -->
                <h2 class="text-left center-title margin-10">Contact Ion Marin</h2>
                <address class="profile-contacts">
                    <p><i class="fa fa-location-arrow"></i> str. Principală nr. 356, Crizbav jud. Brașov (<a href="">vezi hartă</a>)</p>
                    <p><i class="fa fa-phone"></i> +40 368 123-4567</p>
                    <p><i class="fa fa-envelope"></i> <a href="">Trimite un mesaj lui Ion Marin</a></p>
                </address>	

                <!-- News -->
                <h2 class="text-left center-title margin-10">Noutăți de la Ion Marin</h2>
                <div class="profile-news">
                    <article>
                        <a href="">Titlu noutate pentru producator</a>
                        <time>Adăugat de Ion Marin pe 29.08.2016</time>
                    </article>
                    <article>
                        <a href="">Titlu noutate pentru producator</a>
                        <time>Adăugat de Ion Marin pe 29.08.2016</time>
                    </article>
                    <a href="">vezi mai mult</a>
                </div>					
            </div>

        </div>



        <a href="" class="arrows-btn">
            Vezi toți<br />
            producătorii<br />
            din zonă
        </a>
    </div>
</section>	

<script>

    var Login = {
        doLogin: function ()
        {
            $('.alert').remove();
            $('.has-error').removeClass('has-error');

            var valid = true;
            var form = $('#login-form');

            if (form.find('input[name=UserName]').val() == "")
            {
                form.find('input[name=UserName]').closest('label').addClass('has-error');
                valid = false;
            }

            if (form.find('input[name=Password]').val() == "")
            {
                form.find('input[name=Password]').closest('label').addClass('has-error');
                valid = false;
            }

            if (valid)
            {
                $.post('<?= Url::link('login.php') ?>', form.serialize(), function (json) {
                    if (json.errors === '')
                    {
                        document.write(json.result);
                    } else
                    {
                        form.before(json.errors)
                    }
                }, 'json');
            }

            return false;
        }

    };

    $(document).ready(function () {
        $('#login-button').click(Login.doLogin);
    });

</script>

<?php require_once 'template/footer.php'; ?>