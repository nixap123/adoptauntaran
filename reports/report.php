<?php

require $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';

$report = isset($_GET['report']) ? $_GET['report'] : '';
$data = isset($_GET['data']) ? $_GET['data'] : [];

if (empty($report))
{
    exit('Unknown Report Type');
}

require $_SERVER['DOCUMENT_ROOT'] . '/libraries/tcpdf/tcpdf.php';
require $_SERVER['DOCUMENT_ROOT'] . '/libraries/tcpdf/fpdi.php';

$tcPdf = new TCPDF('P', 'mm', 'A4', true, 'utf-8');

$tcPdf->setPrintHeader(false);
$tcPdf->setPrintFooter(false);
$tcPdf->SetMargins(5, 5, 5);
$tcPdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcPdf->SetFont('opensans', '', 12);

$tcPdf->AddPage();

switch ($report)
{
    case 'client-order':
        $tcPdf->SetTitle('Order report');
        $tcPdf->setPageOrientation('P');
        ob_start();
        extract($data);
        require_once 'template/client-order.php';
        $content = ob_get_clean();
    break;

    case 'producator-order':
        $tcPdf->SetTitle('Order report');
        $tcPdf->setPageOrientation('P');
        ob_start();
        extract($data);
        require_once 'template/producator-order.php';
        $content = ob_get_clean();
    break;

    case 'qr-code':
        $userID = $data['UserID'];
        $codeSize = $data['size'];

        require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
        require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
        
        $user_service = new User();
        $USER = $user_service->getByID($userID);
        
        $user_info_service = new UserInfo();
        
         $user_info = $user_info_service->getByUserID($USER['ID']);
        
        if (empty($USER['ID']))
        {
            Url::redirect();
        }
        
        if ($USER['Type'] == User::TypeGal)
        {
            
            require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
            
            $image_service = new Image();
            
            $Image = $image_service->getByID($user_info['Photo']);
            
            if (!empty($Image))
            {
                $logo = $_SERVER['DOCUMENT_ROOT'] . '/uploads/profile/' . $Image;
            }
        }
        
        if (empty($logo))
        {
            $logo = $_SERVER['DOCUMENT_ROOT'] . '/assets/images/logo.png';
        }
    
        $link = Url::link($USER['ID'] . '-' . Url::strToUrl($user_info['Name']));
        
        
        $tcPdf = new FPDI();
        $tcPdf->setPrintHeader(false);
        $tcPdf->setPrintFooter(false);
        $tcPdf->SetMargins(5, 5, 5);
        $tcPdf->SetAutoPageBreak(false);
        $tcPdf->SetFont('opensans', '', 12);
        
        if ($codeSize == 'a4')
        {
            $tcPdf->SetTitle('QR Code A4');
            $tcPdf->AddPage('P', 'A4');
            $tcPdf->setSourceFile($_SERVER['DOCUMENT_ROOT'] . '/uploads/pdf/A4.pdf');
            $temp = $tcPdf->importPage(1);
            $size = $tcPdf->useTemplate($temp);
            $tcPdf->Image($logo, 55, 223, 40);
            $tcPdf->write2DBarcode($link, 'QRCODE,M', 66, 45, 80, 80, '', 'N');
        }
        else
        {
            $tcPdf->SetTitle('QR Code A7');
            $tcPdf->AddPage('L', 'A7');
            $tcPdf->setSourceFile($_SERVER['DOCUMENT_ROOT'] . '/uploads/pdf/A7.pdf');
            $temp = $tcPdf->importPage(1);
            $size = $tcPdf->useTemplate($temp);
            $tcPdf->Image($logo, 5, 7, 25);
            $style = array(
                'border' => false,
                'vpadding' => '',
                'hpadding' => '',
            );
            $tcPdf->write2DBarcode($link, 'QRCODE,M', 5, 40, 25, 25, $style);
        }
        
        $tcPdf->Output($report . '.pdf', 'I');
    break;
    
}

$tcPdf->writeHTML($content, true, false, false, false, '');

$tcPdf->Output($report . '.pdf', 'I');