<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$order_service = new Order();
$user_info_service = new UserInfo();

$order = $order_service->getByID($orderID);

$cartItems = $order_service->getOrderForReportClient($orderID);
?>

<style type="text/css">
<!--
body {
	font-family: Merriweather, Helvetica, sans-serif;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
        font-size: 9px;
}
.style1 {
	font-size: 9px;
}
.style2 {
	font-size: 10px;
	font-weight: bold;
}
.center {
    text-align: center;
}
-->
</style>

<table width="568" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#000000">
  <tr>
    <td valign="top">
        <br />
        FACTURA PROFORMA
        <div style="border: 1px solid #000000; margin-top: 6px;">
            Nr. Facturii fiscale: ADP<?= str_pad($orderID, 5, '0', STR_PAD_LEFT) ?><br />
            Data (ziua, luna, anul): <?= DateHelper::toDMY($order['Date']) ?>
        </div>
        <?php if ($producator['VATPayer']) { ?>
        <div class="style2">cota TVA= 20%</div>
        <?php } else { ?>
        <div class="style2">&nbsp;</div>
        <?php } ?>
      <table style="font-size: 10px;" width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000">
          <tr style="font-size: 9px;">
          <td width="5%" class="style1"><div align="center">Nr. crt. </div></td>
          <td width="47%" class="style1"><div align="center">Denumirea produselor <br />
              sau a serviciilor </div></td>
          <td width="5%" class="style1"><div align="center">U.M.</div></td>
          <td width="9%" class="style1"><div align="center">Cantitatea</div></td>
          <td width="12%" class="style1"><div align="center">Pretul unitar<br />
              -- lei --</div></td>
          <td width="11%" class="style1"><div align="center">Valoarea<br />
              -- lei -- </div></td>
          <td width="11%" class="style1"><div align="center">Valoarea TVA <br />
              -- lei -- </div>
          </td>
        </tr>
        <tr>
          <td class="style1"><div align="center">0</div></td>
          <td class="style1"><div align="center">1</div></td>
          <td class="style1"><div align="center">2</div></td>
          <td class="style1"><div align="center">3</div></td>
          <td class="style1"><div align="center">4</div></td>
          <td class="style1"><div align="center">5 (3x4)</div></td>
          <td class="style1"><div align="center">6</div></td>
        </tr>
            <?php
            $i = 0;
            $GrandTotalWithoutTVA = 0;
            $GrandTotalTVA = 0;
            $GrandTotal = 0;
            ?>
            <?php foreach ($cartItems['Carts'] as $cartID => $cart) { ?>
                <?php $GrandTotalWithoutTVA += $cart['cart']['TotalWithoutTVA']; ?>
                <?php $GrandTotalTVA += $cart['cart']['TotalTVA']; ?>
                <?php $GrandTotal += $cart['cart']['Subtotal']; ?>
                <?php $i++; ?>
                <tr>
                    <td><?= $i ?></td>
                    <td style="text-align: left;">
                        Cos saptamanal: <small><?= implode(', ', $cart['products']) ?></small><br>
                    </td>
                    <td>-</td>
                    <td>1</td>
                    <td><?= (float) $cart['cart']['CartPrice'] ?> lei</td>
                    <td><?= (float) $cart['cart']['CartPrice'] ?> lei</td>
                    <td><?= (float) $cart['cart']['TotalTVA'] ?> lei</td>
                </tr>
            <?php } ?>
            <?php foreach ($cartItems['Products'] as $ocpID => $product) { ?>
                <?php $GrandTotalWithoutTVA += $product['TotalWithoutTVA']; ?>
                <?php $GrandTotalTVA += $product['TotalTVA']; ?>
                <?php $GrandTotal += $product['Subtotal']; ?>
                <?php $i++; ?>
                <tr>
                    <td><?= $i ?></td>
                    <td  style="text-align: left;"><?= $product['ProductName'] ?> de la <?= $product['UserName'] ?></td>
                    <td><?= Unit::getName($product['UnitID']) ?></td>
                    <td><?= (float) $product['ProductQuantity'] ?></td>
                    <td><?= (float)$product['ProductPrice'] ?> lei</td>
                    <td><?= (float)($product['ProductPrice'] * $product['ProductQuantity']) ?> lei</td>
                    <td><?= (float) $product['TotalTVA'] ?> lei</td>
                </tr>
            <?php } ?>
      </table>
                
            <br />
      
      <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000">
        <tr>
            <td width="27%" rowspan="4" valign="top" style="font-size: 10px;" class="style1"><div align="center">Semnatura <br />
              si stampila<br />
              furnizorului</div>
            </td>
            <td  style="font-size: 9px; text-align: left; line-height: 16px;" width="38%" rowspan="4" valign="top" class="style1"><div style="text-align: center;"><b style="font-size: 10px;">Date privind expeditia</b></div>
            Numele delegatului ....................................................................................<br />
            B.I/C.I. seria ..... nr. .................... eliberat(a) ........... <br />
            Mijlocul de transport ....................... nr. ................. <br />
            Expedierea s-a efectuat in prezenta<br /> noastra la
            data de ............................. ora .............. <br />
            Semnaturile ............................................................. 
          </td>
          <td width="13%" rowspan="2" valign="top" style="font-size: 10px;">Total <br />
            din care:<br />
            accize
          </td>
          <td style="font-size: 10px;" width="11%" valign="top" align="center" class="style2"><?= $GrandTotal ?></td>
          <td style="font-size: 10px;" width="11%" valign="top" align="center" class="style2"><?= $GrandTotalTVA ?></td>
        </tr>
        <tr>
          <td valign="top" class="style1">&nbsp;</td>
          <td valign="top" class="style1"><div align="center"><strong>X</strong></div></td>
        </tr>
        <tr style="font-size: 10px;">
          <td rowspan="2" valign="top" class="style1">Semnatura de primire </td>
          <td colspan="2" valign="top" class="style1">Total de plata 
            (col. 5 ) </td>
        </tr>
        <tr>
          <td colspan="2" valign="top"><div align="center" class="style2"><?= $GrandTotal ?></div></td>
        </tr>
      </table></td>
  </tr>
</table>