<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$order_service = new Order();
$user_info_service = new UserInfo();

$order = $order_service->getByID($orderID);

$cumparator = $user_info_service->getForReport($order['UserID']);
$producator = $user_info_service->getForReport($_SESSION['UserID']);

$cartItems = $order_service->getOrderForReportProducator($orderID, $_SESSION['UserID']);
?>

<style type="text/css">
<!--
body {
	font-family: Merriweather, Helvetica, sans-serif;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
        font-size: 9px;
}
.style1 {
	font-size: 9px;
}
.style2 {
	font-size: 10px;
	font-weight: bold;
}
.center {
    text-align: center;
}
-->
</style>

<table width="568" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#000000">
  <tr>
    <td valign="top"><table width="632" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td width="50%" valign="top" align="left">
            <strong><b>PRODUCATOR</b></strong><br />
            <font style="font-size: 9px; text-align: left;">Nr.
                ord. Reg. Com. / an:  <?= $producator['RegNumber'] ?><br />
                Atribut fiscal: - <br />
                CUI / CNP: <?= $producator['CUI'] ?><br />
                Sediul / Adresa: <?= $producator['CityName'] ?>, <?= $producator['Address'] ?> <br />
                Judetul: <?= $producator['JudetName'] ?> <br />
                Cont bancar: <?= $producator['BankAccount'] ?><br />
                Banca: <?= $producator['BankName'] ?>
            </font>
          </td>
            <td width="50%" valign="top" align="left">
            <strong><b>CUMPARATOR</b></strong><br />
            <font style="font-size: 9px; text-align: left;">Nr. ord. reg. com. / an: <?= $cumparator['RegNumber'] ?> <br />
            C.U.I / CI: <?= $cumparator['CUI'] ?> <br />
            Sediul / Adresa: <?= $cumparator['CityName'] ?>, <?= $cumparator['Address'] ?> <br />
            Judetul: <?= $cumparator['JudetName'] ?> <br />
            Contul: <?= $cumparator['BankAccount'] ?><br />
            Banca: <?= $cumparator['BankName'] ?>
            </font>
          </td>
        </tr>
      </table>
        <br />
        <br />
        FACTURA PROFORMA
        <div style="border: 1px solid #000000; margin-top: 6px;">
            Nr. Facturii fiscale: ADP<?= str_pad($orderID, 5, '0', STR_PAD_LEFT) ?><br />
            Data (ziua, luna, anul): <?= DateHelper::toDMY($order['Date']) ?>
        </div>
        <?php if ($producator['VATPayer']) { ?>
        <div class="style2">cota TVA= 20%</div>
        <?php } else { ?>
        <div class="style2">&nbsp;</div>
        <?php } ?>
      <table style="font-size: 10px;" width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000">
          <tr style="font-size: 9px;">
          <td width="5%" class="style1"><div align="center">Nr. crt. </div></td>
          <td width="47%" class="style1"><div align="center">Denumirea produselor <br />
              sau a serviciilor </div></td>
          <td width="5%" class="style1"><div align="center">U.M.</div></td>
          <td width="9%" class="style1"><div align="center">Cantitatea</div></td>
          <td width="12%" class="style1"><div align="center">Pretul unitar<br />
              -- lei --</div></td>
          <td width="11%" class="style1"><div align="center">Valoarea<br />
              -- lei -- </div></td>
          <?php if ($producator['VATPayer']) { ?>
          <td width="11%" class="style1"><div align="center">Valoarea TVA <br />
              -- lei -- </div>
          </td>
          <?php } ?>
        </tr>
        <tr>
          <td class="style1"><div align="center">0</div></td>
          <td class="style1"><div align="center">1</div></td>
          <td class="style1"><div align="center">2</div></td>
          <td class="style1"><div align="center">3</div></td>
          <td class="style1"><div align="center">4</div></td>
          <td class="style1"><div align="center">5 (3x4)</div></td>
          <?php if ($producator['VATPayer']) { ?>
          <td class="style1"><div align="center">6</div></td>
          <?php } ?>
        </tr>
        <?php
        $GrandTotalWithoutTVA = 0;
        $GrandTotalTVA = 0;
        $GrandTotal = 0;
        $i = 0;
        ?>
        <?php foreach ($cartItems['Products'] as $ocpID => $product) { ?>
            <?php $GrandTotalWithoutTVA += $product['TotalWithoutTVA']; ?>
            <?php $GrandTotalTVA += $product['TotalTVA']; ?>
            <?php $GrandTotal += $product['Subtotal']; ?>
            <?php $i++; ?>
            <tr>
                <td valign="top" class="style2"><?= $i ?></td>
                <td style="text-align: left;" class="style2"><?= $product['ProductName'] ?></td>
               
                <td valign="top" class="style2" align="center"><?= Unit::getName($product['UnitID']) ?></td>
                <td valign="top" class="style2" align="center"><?= (float) $product['ProductQuantity'] ?></td>
                <td valign="top" class="style2" align="center"><?= (float) $product['ProductPrice'] ?></td>
                <td valign="top" class="style2" align="center"><?= (float) $product['Subtotal'] ?></td>
                <?php if ($producator['VATPayer']) { ?>
                <td valign="top" class="style2" align="center"><?= (float) $product['TotalTVA'] ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
      </table>
      
      <div style="padding: 5px 0;" class="style1"><?= $producator['Name'] ?> <?= $producator['BankAccount'] ?>; termen de plata: 5 zile; penalitati de intarziere: 0,5% / zi</div>
      
      <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000">
        <tr>
            <td width="27%" rowspan="4" valign="top" style="font-size: 10px;" class="style1"><div align="center">Semnatura <br />
              si stampila<br />
              furnizorului</div>
            </td>
            <td  style="font-size: 9px; text-align: left; line-height: 16px;" width="38%" rowspan="4" valign="top" class="style1"><div style="text-align: center;"><b style="font-size: 10px;">Date privind expeditia</b></div>
            Numele delegatului ....................................................................................<br />
            B.I/C.I. seria ..... nr. .................... eliberat(a) ........... <br />
            Mijlocul de transport ....................... nr. ................. <br />
            Expedierea s-a efectuat in prezenta<br /> noastra la
            data de ............................. ora .............. <br />
            Semnaturile ............................................................. 
          </td>
          <td width="13%" rowspan="2" valign="top" style="font-size: 10px;">Total <br />
            din care:<br />
            accize
          </td>
          <td style="font-size: 10px;" width="11%" valign="top" align="center" class="style2"><?= $GrandTotal ?></td>
          <td style="font-size: 10px;" width="11%" valign="top" align="center" class="style2"><?= $producator['VATPayer'] ? $GrandTotalTVA : '-' ?></td>
        </tr>
        <tr>
          <td valign="top" class="style1">&nbsp;</td>
          <td valign="top" class="style1"><div align="center"><strong>X</strong></div></td>
        </tr>
        <tr style="font-size: 10px;">
          <td rowspan="2" valign="top" class="style1">Semnatura de primire </td>
          <td colspan="2" valign="top" class="style1">Total de plata 
            (col. 5 ) </td>
        </tr>
        <tr>
          <td colspan="2" valign="top"><div align="center" class="style2"><?= $GrandTotal ?></div></td>
        </tr>
      </table></td>
  </tr>
</table>