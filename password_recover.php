<?php
require_once 'core/core.php';
require_once 'services/User.php';
require_once 'libraries/Email.php';

$user_sercice = new User();

if (isset($_POST['Login'])) {
    $user = reset($user_sercice->getWhere(['UserName' => $_POST['UserName']], 1));

    if (empty($user['ID'])) {
        Validator::setError(Lang::t('EmailIsNotExist'));
        exit(json_encode([
            'result' => '',
            'errors' => Validator::showMessages()
        ]));
    } else {
        $code = uniqid();
        
        $user_sercice->update(['RecoveryCode' => $code], ['ID' => $user['ID']]);
        
        Email::send($user['UserName'], Lang::t('PasswordRecovery'), "
            <a href=" . Url::link('password_reset.php', ['code' => $code]) . ">" . Lang::t('LinkToResetPassword') . "</a>
        ");
        
        Validator::setSuccess(Lang::t('PleaseVerifyYourEmail'));
        exit(json_encode([
            'errors' => '',
            'result' => Validator::showMessages()
        ]));
    }
}

require_once 'template/head.php';
?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('PasswordRecovery') ?></h2>
        <div class="row">
            <form id="login-form" class="login-form col-md-6 form-horizontal" method="post">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= Lang::t('Email') ?></label>
                        <div class="col-md-9">
                            <input type="text" name="UserName" placeholder="<?= Lang::t('InsertYourEmailAddress') ?>" />
                        </div>
                    </div>
                </div>
                <input type="hidden" name="Login" value="1" />
                <div class="form-group text-right">
                    <input id="login-button" value="Login" class="btn btn-danger" /><br /><br />
                </div>
                <div class="form-group text-center">						
                    <p><a href="<?= Url::link('/termeni')?>"><?= Lang::t('TermsAndConditions') ?></a></p>			
                    <p><a href="<?= Url::link('/confidentialitate') ?>"><?= Lang::t('PrivacyPolicy') ?></a></p>
                </div>
            </form>
        </div>
    </div>
</section>	

<section class="home-block">
    <div class="container">
        <a href="" class="arrows-btn">
            <?= Lang::t('RegisterHere') ?><br />
            <?= Lang::t('AdoptAPeasant') ?>!
        </a>
    </div>
</section>

<script>

    var Login = {
        doLogin: function ()
        {
            $('.alert').remove();
            $('.has-error').removeClass('has-error');

            var valid = true;
            var form = $('#login-form');

            if (form.find('input[name=UserName]').val() == "")
            {
                form.find('input[name=UserName]').closest('label').addClass('has-error');
                valid = false;
            }

            if (valid)
            {
                $('#login-button').unbind('click');
                $.post('<?= Url::link('password_recover.php') ?>', form.serialize(), function (json) {
                    if (json.errors === '')
                    {
                        form.before(json.result);
                    }
                    else
                    {
                        form.before(json.errors);
                        $('#login-button').click(Login.doLogin);
                    }
                }, 'json');
            }

            return false;
        }

    };

    $(document).ready(function () {
        $('#login-button').click(Login.doLogin);
    });

</script>

<?php require_once 'template/footer.php'; ?>