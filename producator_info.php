<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserPhoto.php';
    
    $user_service = new User();
    $image_service = new Image();
    $user_info_service = new UserInfo();
    $user_photo_service = new UserPhoto();
    
    $user_info = $user_info_service->getByUserID((int)$_GET['id']);
    $user = $user_service->getByID($user_info['UserID']);
    
    if (empty($user_info['ID']) || empty($user['ID']))
    {
        Url::redirect('register.php');
    }
    
    if (Request::isPost())
    {
        $Description = Validator::validate('Description', Validator::ValidateEmpty, 'Description');
        
        if (!Validator::hasErrors())
        {
            $files = Upload::file('Photos[]', 'uploads/gallery', ['jpg', 'png']);
        
            
            if (count($files) > 0)
            {
                $insert = [];
                foreach ($files as $file)
                {
                    $Thumb = 'thumb_' . $file;
                    $Image = $file;
                    $dist = $_SERVER['DOCUMENT_ROOT']. '/uploads/gallery/';
                    
                    Upload::resize(250, $dist . $Thumb, $dist . $Image);
                    Upload::resize(1200, $dist . $Image, $dist . $Image);
                    
                    $imageID = $image_service->insert([
                        'Thumb' => $Thumb,
                        'Image' => $Image
                    ]);
                    
                    $insert[] = [
                        'UserID' => $user['ID'],
                        'ImageID' => $imageID
                    ];
                }
                
                $user_photo_service->insert_batch($insert);
            }
            
            $user_info_service->update(['Description' => $Description], ['ID' => $user_info['ID']]);
            
            $_SESSION['UserID'] = $user['ID'];
            
            Url::redirect('producator/index');
            
        }
    }
    
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('Description') ?></h2>
        <div class="row">
            <form class="login-form col-md-6 form-horizontal" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= Lang::t('AddDescription') ?></label>
                        <div class="col-md-9">
                            <textarea style="min-height: 150px;" class="summer-note form-control" name="Description"></textarea>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <label class="col-md-3 control-label"><?= Lang::t('AddPhotos') ?></label>
                        <div class="col-md-9">
                            <input class="form-control" type="file" multiple name="Photos[]" />
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <input type="submit" value="<?= Lang::t('Save') ?>" class="btn btn-danger" style="width: 240px; display: inline-block !important;" /><br /><br />
                   
                </div>
                
            </form>
        </div>
    </div>
</section>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>