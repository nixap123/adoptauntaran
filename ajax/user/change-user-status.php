<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';

if (empty($_SESSION['UserID'])) exit;

$userID = (int)$_POST['userID'];
$status = $_POST['status'] == 'Inactive' ? User::StatusInactive : User::StatusActive;

$user_service = new User();
$user_info_service = new UserInfo();

$user = $user_service->getByID($userID);
$user_info = $user_info_service->getByUserID($user['ID']);

if (empty($user['ID'])) exit;

if ($user['IsNew'])
{
    $message  = "<div style=\"text-align:center;\"><img src=\"" . Url::link('assets/images/logo.png') . "\"></div>";
    $message .= "<div>Drag(ă) " . $user_info['Name'] . ",</div>";
    $message .= "<div>îți mulțumim pentru interesul pe care l-ai arătat acestui proiect. Este o plăcere să te avem lângă noi. Platforma \"Adoptă un țăran\" este gândită să pună în legătură familiile de la oraș cu gospodăriile de la țară oferindu-le un mediu unde aceste părți să se întâlnească. Dacă primești acest mail, înseamnă că ți-am verificat gospodăria și aceasta corespunde exigențelor noastre. </div>";
    $message .= "<div>Îți urăm succes în demersurile pe care le ai, și promitem să te ținem la curent cu tot ceea ce înseamnă inițiative ale asociației noastre, dar și cu informații utile dezvoltării tale.</div>";
    $message .= "<br /><div>Cu mult drag,<br>asociația Creștem România Împreună</div>";
    
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=utf-8\r\n";

    mail($user['UserName'], 'Registrarea la adoptauntaran.ro', $message, $headers);
}

$user_service->update([
    'Status' => $status,
    'IsNew' => 0
], [
    'ID' => $userID
]);