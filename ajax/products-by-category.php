<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';

$product_service = new Product();

$CategoryID = (int)$_POST['CategoryID'];

$result = $product_service->getByCategory($CategoryID);

$html = '<option value="0">- selectati produs -</option>';
foreach ($result as $row)
{
    $html .= '<option data-unit-id="' . $row['UnitID'] . '" value="' . $row['ProductID'] . '">' . $row['Name'] . '</option>';
}

exit($html);