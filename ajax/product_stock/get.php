<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$product_stock_service = new ProductStock();

$psID = (int)$_POST['psID'];

if ($psID > 0)
{
    $psData = $product_stock_service->getForEdit($psID, $_SESSION['UserID']);
}

$CategoryID = isset($psData['CategoryID']) ? $psData['CategoryID'] : 0;
$ProductID = isset($psData['ProductID']) ? $psData['ProductID'] : 0;
$Quantity = isset($psData['Quantity']) ? $psData['Quantity'] : 0;
$UnitID = isset($psData['UnitID']) ? $psData['UnitID'] : 0;
$Price = isset($psData['Price']) ? $psData['Price'] : 0;
$ExpirationDate = isset($psData['ExpirationDate']) ? DateHelper::toDMY($psData['ExpirationDate']) : date('d-m-Y', strtotime('+1 week'));
$Regular = isset($psData['Regular']) ? $psData['Regular'] : 0;

?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('SelectCategory') ?></label>
            <?= Category::categorySelect($CategoryID) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('SelectProduct') ?></label>
            <?= Product::productSelect($CategoryID, $ProductID) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Quantity') ?></label>
            <input class="form-control" name="Quantity" value="<?= $Quantity ?>" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Unit') ?></label>
            <input type="text" class="form-control" readonly name="UnitID" value="<?= Unit::getName($UnitID) ?>" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Price') ?></label>
            <?= Html::priceInput('Price', $Price) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('ValidTo') ?></label>
            <input type="text" name="ExpirationDate" value="<?= DateHelper::toDMY($ExpirationDate) ?>" class="form-control datepicker" />
        </div>
    </div>
    <div class="col-md-3">
        <label class="hidden-sm hidden-xs">&nbsp;</label>
        <div class="checkbox">
            <label>
                <input <?= $Regular ? 'checked' : '' ?> name="Regular" type="checkbox"> <?= Lang::t('Permanent') ?>
            </label>
        </div>
    </div>
    <input type="hidden" name="psID" value="<?= $psID ?>" />
</div>
<script>
    $('input[name=Regular]').click(function () {
        if ($(this).is(':checked'))
        {
            $('input[name=ExpirationDate]').attr('disabled', 'disabled');
        } else
        {
            $('input[name=ExpirationDate]').removeAttr('disabled');
        }
    });
    
    $('select[name=CategoryID]').change(function(){
        $.post('<?= Url::link('ajax/products-by-category.php') ?>', {CategoryID: $(this).val()}, function(html){
            $('select[name=ProductID]').html(html);
        });
    });
    
    $('select[name=ProductID]').change(function(){
        $.post('<?= Url::link('ajax/unit-by-id.php') ?>', {ProductID:  $('select[name=ProductID]').val()}, function(html){
            $('input[name=UnitID]').val(html);
        });
    });
    
    $('.datepicker').datepicker({
        'format': 'dd.mm.yyyy'
    });
</script>