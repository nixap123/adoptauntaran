<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$product_stock_service = new ProductStock();

$psID = Request::post('psID');

$CategoryID = Validator::validate('CategoryID', Validator::ValidateEmpty, Lang::t('Category'));
$ProductID = Validator::validate('ProductID', Validator::ValidateEmpty, Lang::t('Product'));
$Quantity = Validator::validate('Quantity', Validator::ValidateNumber, Lang::t('Quantity'));
$Price = Validator::validate('Price', Validator::ValidateNumber, Lang::t('Price'));
$Regular = Request::post('Regular') == 'on' ? 1 : 0;

if ($Regular == 0)
{
    $ExpirationDate = Validator::validate('ExpirationDate', Validator::ValidateEmpty, Lang::t('ValabilPinaLa'));
}

if (!Validator::hasErrors())
{
    if ($psID > 0)
    {
        $productData = $product_stock_service->getByID($psID);
        
        $update = [
            'Price' => $Price
        ];
        
        if ($productData['OrderedQuantity'] > 0)
        {
            if ($ProductID !== $productData['ProductID'])
            {
                Validator::setError(Lang::t('YouDontChangeProduct'));
            }
        }
        else
        {
            $update['ProductID'] = $ProductID;
        }
        
        if ($Quantity < $productData['OrderedQuantity'])
        {
            Validator::setError(sprintf(Lang::t('Quantity:%dOrdered'), $productData['OrderedQuantity']));
        }
        else
        {
            $update['Quantity'] = $Quantity;
        }
        
        if ($Regular == 1)
        {
            $update['Regular'] = 1;
        }
        else
        {
            if (strtotime($ExpirationDate) < time())
            {
                Validator::setError(Lang::t('IncorrectDate'));
            }
            else
            {
                $update['ExpirationDate'] = date('c', strtotime($ExpirationDate));
            }
        }
        
        if (!Validator::hasErrors())
        {
            $update['Updated'] = date('c');
            $product_stock_service->update($update, ['ID' => $psID]);
            Validator::setSuccess(Lang::t('Saved')); 
        }
    }
    else
    {
        $psID = $product_stock_service->insert([
            'UserID' => $_SESSION['UserID'],
            'ProductID' => $ProductID,
            'Quantity' => $Quantity,
            'Price' => $Price,
            'OrderedQuantity' => 0,
            'Created' => date('c'),
            'Updated' => date('c'),
            'ExpirationDate' => date('c', strtotime($ExpirationDate)),
            'Regular' => $Regular,
            'Status' => ProductStock::StatusActive
        ]);
        Validator::setSuccess(Lang::t('ProductSaved'));
    }
}

exit(json_encode([
    'error' => Validator::hasErrors(),
    'messages' => Validator::showMessages(),
    'psID' => $psID
]));