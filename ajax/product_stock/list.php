<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$product_stock_service = new ProductStock();

$product_list = $product_stock_service->getActiveProductStocks($_SESSION['UserID']);

?>

<table id="ps-table" class="table dataTable">
    <thead>
        <tr>
            <th>ID</th>
            <th><?= Lang::t('Product') ?></th>
            <th><?= Lang::t('Category') ?></th>
            <th><?= Lang::t('Quantity') ?></th>
            <th><?= Lang::t('ValidTo') ?></th>
            <th><?= Lang::t('Price') ?></th>
            <th><?= Lang::t('Orders') ?></th>
            <th><?= Lang::t('Ordered') ?></th>
            <th><?= Lang::t('Status') ?></th>
            <th><?= Lang::t('Edit') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($product_list as $product) { ?>
        <tr>
            <td><?= $product['ID'] ?></td>
            <td>
                <?= $product['ProductName'] ?>
                <?php if ($product['Regular'] != 1 && ($product['Quantity'] - $product['OrderedQuantity']) / $product['Quantity'] < 0.1) { ?>
                <i title="Mic sold!" style="color: #d43f3a;" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                <?php } ?>
            </td>
            <td><?= $product['CategoryName'] ?></td>
            <td><?= (floatval($product['Quantity'])) ?> <?= Unit::getName($product['UnitID']) ?></td>
            <td><?= $product['Regular'] == 1 ? '(Regular)' : DateHelper::toDMY($product['ExpirationDate']) ?></td>
            <td><?= $product['Price'] ?></td>
            <td><?= $product['Orders'] ?></td>
            <td><?= $product['OrderedQuantity'] ?></td>
            <td><?= $product['Status'] ?></td>
            <td>
                <a data-toggle="modal" data-target=".edit-ps-modal" data-ps-id="<?= $product['psID'] ?>" class="edit-ps" style="cursor: pointer;" ><i data-toggle="tooltip" data-placement="right" title="<?= Lang::t('EditProduct') ?>" class="fa fa-pencil blue"></i></a>
                <a data-ps-id="<?= $product['psID'] ?>" class="delete-ps" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="<?= Lang::t('DeleteProduct') ?>"><i class="fa fa-trash red"></i></a>
                <?php if ($product['Status'] == ProductStock::StatusActive) { ?>
                <a href="#" class="btn btn-xs" data-toggle="tooltip" data-placement="right" title="<?= Lang::t('DeactivateProduct') ?>"><i psid="<?= $product['psID'] ?>" class="fa fa-pause-circle-o disable-product" aria-hidden="true"></i></a>
                <?php } else { ?>
                <a href="#" class="btn btn-xs" data-toggle="tooltip" data-placement="right" title="<?= Lang::t('ActivateProduct') ?>"><i psid="<?= $product['psID'] ?>" class="fa fa-play-circle-o enable-product" aria-hidden="true"></i></a>
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    
    $('#ps-table').dataTable();
    
    $(document).ready(function(){
        
        $('body').on('click', '.disable-product', function(e){
            e.preventDefault();
            var control = $(this);
            var psID = parseInt(control.attr('psid'));
            $.post('<?= Url::link('ajax/product_stock/change-ps-status.php') ?>', {psID: psID, status: 'Inactive'}, function(){
                control.removeClass('fa-pause-circle-o').removeClass('disable-product').addClass('fa-play-circle-o').addClass('enable-product').attr('title', 'Activate product');
            });
        });
        
        $('body').on('click', '.enable-product', function(e){
            e.preventDefault();
            var control = $(this);
            var psID = parseInt(control.attr('psid'));
            $.post('<?= Url::link('ajax/product_stock/change-ps-status.php') ?>', {psID: psID, status: 'Active'}, function(){
                control.removeClass('fa-play-circle-o').removeClass('enable-product').addClass('fa-pause-circle-o').addClass('disable-product').attr('title', 'Inactivate product');
            });
        });
        
    });

</script>