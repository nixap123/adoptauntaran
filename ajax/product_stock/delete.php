<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';

$product_service = new Product();
$product_stock_service = new ProductStock();

$psID = isset($_POST['psID']) ? (int)$_POST['psID'] : 0;

if ($psID > 0)
{
    $psData = $product_stock_service->getByID($psID);
    $product = $product_service->getByID($psData['ProductID']);
    
    if ($psData['OrderedQuantity'] > 0)
    {
        Validator::setError(sprintf(Lang::t('ProdusInCantitatea %d %s AFostCumparat'), $psData['OrderedQuantity'], Unit::getName($product['UnitID'])));
    }
    
    if (!Validator::hasErrors())
    {
        $product_stock_service->delete([
            'ID' => $psID,
            'UserID' => $_SESSION['UserID']
        ]);
        
        exit(json_encode([
            'error' => false,
            'messages' => ''
        ]));
    }
    else
    {
        exit(json_encode([
            'error' => true,
            'messages' => Validator::showMessages()
        ]));
    }
}