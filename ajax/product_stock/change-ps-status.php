<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

if (empty($_SESSION['UserID'])) exit;

$psID = (int)$_POST['psID'];
$status = $_POST['status'] == 'Inactive' ? ProductStock::StatusInactive : ProductStock::StatusActive;

$product_stock_service = new ProductStock();

$product_stock_service->update([
    'Status' => $status
], [
    'ID' => $psID,
    'UserID' => $_SESSION['UserID']
]);