<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';

$city_service = new City();

$result = $city_service->getByJudetID($_POST['JudetID']);

$html = '';
foreach ($result as $row)
{
    $html .= '<option value="' . $row['ID'] . '">' . $row['Name'] . '</option>';
}

exit($html);