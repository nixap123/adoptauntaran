<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Donate.php';

if (Request::isPost())
{
    $donate_service = new Donate();
    
    $Name = Validator::validate('Name', Validator::ValidateEmpty, Lang::t('Name'));
    $Address = Request::post('Address');
    $Phone = Request::post('Phone');
    $Email = Validator::validate('Email', Validator::ValidateEmail, Lang::t('Email'));
    $Amount = Validator::validate('Amount', Validator::ValidateNumber, Lang::t('DonateAmount'));
    $PaymentMode = Validator::validate('PaymentMode', Validator::ValidateEmpty, Lang::t('PaymentMethod'));
    
    if (!Validator::hasErrors())
    {
        $donateID = $donate_service->insert([
            'Name'          => $Name,
            'Address'       => $Address,
            'Phone'         => $Phone,
            'Email'         => $Email,
            'Amount'        => $Amount,
            'PaymentMode'   => $PaymentMode,
            'Status'        => Donate::StatusPending
        ]);
        
        if ($PaymentMode == 'paypal')
        {
            $response = file_get_contents('http://api.fixer.io/latest');
            
            if ($response)
            {
                $rates = json_decode($response);
                
                $rate = (float)$rates->rates->RON;
            }
            
            if (!$rate)
            {
                $rate = 4.5;
            }
            
            $Amount = round($Amount / $rate, 2);
        }
        
        $result = [
            'id' => $donateID,
            'amount' => $Amount
        ];
        
        exit(json_encode([
            'success' => true,
            'result' => $result,
            'errors' => ''
        ]));
    }
    else
    {
        exit(json_encode([
            'success' => true,
            'result' => [],
            'errors' => Validator::showMessages()
        ]));
    }
}