<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Subscribe.php';

$subscribe_service = new Subscribe();

if (Request::isPost())
{
    $Email = Validator::validate('email', Validator::ValidateEmail, 'Email');
    
    if (!Validator::hasErrors())
    {
        $exist = $subscribe_service->getWhere(['Email' => $Email], 1);
        
        if (count($exist) > 0)
        {
            Validator::setError(Lang::t('AllreadySubscribed'));
        }
        
        if (!Validator::hasErrors())
        {
            $subscribe_service->insert([
                'Email' => $Email,
                'Date'  => date('c')
            ]);
            
            Validator::setSuccess(Lang::t('SuccessSubscribe'));
            
            exit(json_encode([
                'success' => true,
                'messages' => Validator::showMessages(true)
            ]));
        }
        else
        {
            exit(json_encode([
                'success' => false,
                'messages' => Validator::showMessages(true)
            ]));
        }
    }
}