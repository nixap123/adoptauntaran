<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Donate.php';

$donate_service = new Donate();

$status = $_POST['status'] == Donate::StatusPaid ? Donate::StatusPaid : Donate::StatusPending;
$donateID = (int)$_POST['donateID'];

$donate_service->update([
    'Status' => $status
], [
    'ID' => $donateID
]);