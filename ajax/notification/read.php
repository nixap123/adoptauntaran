<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Notification.php';

$notification_service = new Notification();

$id = (int)$_POST['id'];

$notification_service->update([
    'Unread' => 0
], [
    'ID' => $id,
    'UserID' => $_SESSION['UserID']
]);