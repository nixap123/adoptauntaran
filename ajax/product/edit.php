<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductLang.php';
    
    $product_service = new Product();
    $product_lang_service = new ProductLang();
    
    $ProductID = isset($_POST['ProductID']) ? (int)$_POST['ProductID'] : 0;
    $CategoryID = (int)$_POST['CategoryID'];
    $UnitID = (int)$_POST['UnitID'];
    
    if ($ProductID == 0)
    {
        $ProductID = $product_service->insert([
            'CategoryID' => $CategoryID,
            'UnitID' => $UnitID
        ]);
    }
    else
    {
        $product_service->update([
            'CategoryID' => $CategoryID,
            'UnitID' => $UnitID
        ], [
            'ID' => $ProductID
        ]);
    }
    
    $product_lang_service->delete(['ProductID' => $ProductID]);
        
    $names = $_POST['Name'];
    foreach ($names as $langID => $name)
    {
        $product_lang_service->insert([
            'ProductID' => $ProductID,
            'LangID' => $langID,
            'Name' => $name
        ]);
    }
    
    Validator::setSuccess();
    
    echo json_encode([
        'productID' => $ProductID,
        'categoryID' => $CategoryID,
        'UnitID' => $UnitID,
        'message' => Validator::showMessages()
    ]);