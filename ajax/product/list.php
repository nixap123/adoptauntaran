<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';

$product_service = new Product();

$categoryID = (int)$_POST['CategoryID'];

$res = $product_service->getForAdmin($categoryID);

$return = "<table class=\"table table-striped dataTable\">"
            . "<thead><tr>"
                . "<th>" . Lang::t('Name') . "</th>"
                . "<th>" . Lang::t('Category') . "</th>"
                . "<th>" . Lang::t('Unit') . "</th>"
                . "<th></th>"
            . "</tr></thead><tbody>";

    foreach ($res as $row)
    {
        $return .= "<tr>"
                    . "<td>" . $row['Name'] . "</td>"
                    . "<td>" . $row['CategoryName'] . "</td>"
                    . "<td style='max-width: 100px;'>" . Unit::getName($row['UnitID']) . "</td>"
                    . "<td style='max-width: 100px;'>"
                        . "<a data-toggle=\"modal\" data-target=\".add-product-modal\" data-product-id=\"" . $row['ID'] . "\" class=\"add-product\" style=\"cursor:pointer;\"><i class=\"fa fa-pencil blue\"></i></a>&nbsp;"
                        . "<a data-product-id=\"" . $row['ID'] . "\" class=\"delete-product\" style=\"cursor:pointer;\"><i class=\"fa fa-trash red\"></i></a>"
                    . "</td>"
                . "</tr>";
    }

$return .= "</tbody></table>";

exit($return);