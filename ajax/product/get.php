<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';

$product_service = new Product();
$category_service = new Category();

$ProductID = (int)$_POST['ProductID'];

$res = $product_service->getForEdit($ProductID);

$arr = [];
foreach ($res as $row)
{
    $arr[$row['LangID']] = $row['Name'];
}

if ($ProductID > 0)
{
    $product = $product_service->getByID($ProductID);
}

foreach (Config::$LangList as $langID => $lang) { ?>

    <div class="form-group">
        <label class="control-label"><?= Lang::t('Name') ?> (<?= $lang['name'] ?>)</label>
        <input type="text" name="Name[<?= $langID ?>]" value="<?= isset($arr[$langID]) ? $arr[$langID] : '' ?>" class="form-control" />
    </div>

<?php } ?>

<div class="form-group">
    <label><?= Lang::t('Category') ?></label>
    <?= Category::categorySelect(isset($product['CategoryID']) ? $product['CategoryID'] : $_POST['CategoryID']) ?>
</div>

<div class="form-group">
    <label class="control-label"><?= Lang::t('Unit') ?></label>
    <?= Unit::unitSelect($product['UnitID']) ?>
</div>

<input type="hidden" name="ProductID" value="<?= $ProductID ?>" />