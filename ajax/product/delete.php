<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';

$product_service = new Product();

$ProductID = isset($_POST['ProductID']) ? (int)$_POST['ProductID'] : 0;

if ($ProductID > 0)
{
    $product_service->deleteByID($ProductID);
}