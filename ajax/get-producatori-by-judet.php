<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

$user_service = new User();

$judetID = (int)$_POST['JudetID'];
$offset = (int)$_POST['offset'];

$res = $user_service->getProducatoriByJudet($judetID, 6, $offset);
$usersByJudet = $res['result'];
$usersCount = $res['count'];

?>

<?php ob_start(); ?>
<?php foreach ($usersByJudet as $key => $producator) { ?>
    <?php if (count($producator['Categories']) > 0) { ?>
        <div class="profile-item col-md-4">
            <a href="<?= Url::link($key . '-' . Url::strToUrl($producator['User']['Name'])) ?>">
                <div class="profile-item-img">
                    <span class="red-square"></span>
                    <img src="<?= Url::link('assets/images/_taran.png') ?>" alt="">
                </div>
                <h3><?= $producator['User']['Name'] ?></h3>
                <p><strong><?= Lang::t('Offer') ?>:</strong> <?= implode(', ', $producator['Categories']) ?></p>
            </a>
        </div>
    <?php } ?>
<?php } ?>
<?php $html = ob_get_clean(); ?>

<?php
    echo json_encode([
        'html' => $html,
        'count' => $count
    ]);
?>