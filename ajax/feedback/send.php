<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Feedback.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

$feedback_service = new Feedback();
$user_service = new User();

$Message = strip_tags(Request::post('Message'));
$ReceiverID = Request::post('ReceiverID');
$Name = Request::post('Name');
$Email = Request::post('Email');
$Phone = Request::post('Phone');

$Receiver = $user_service->getByID($ReceiverID);

if (empty($Receiver['ID'])) exit;



$feedback_service->insert([
    'ReceiverID' => Request::post('ReceiverID'),
    'SenderID' => isset($_SESSION['UserID']) ? $_SESSION['UserID'] : 'null',
    'Name' => $Name,
    'Email' => $Email,
    'Phone' => $Phone,
    'Message' => $Message,
    'Date' => date('c')
]);

$message  = "<p>Nume: $Name</p>";
$message .= "<p>Email: $Email</p>";
$message .= "<p>Telefon: $Phone</p>";
$message .= "<div>$Message</div>";

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=utf-8\r\n";

mail($Receiver['UserName'], 'Feedback adoptauntaran.ro', $message, $headers);