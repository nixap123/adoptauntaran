<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Upload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';

$user_info_service = new UserInfo();

$res = Upload::image('uploads/profile', 256);

$return = [];
if (count($res) > 0)
{
    $image_service = new Image();
    
    foreach ($res as $file)
    {
        $imageID = $image_service->insert([
            'Thumb' => $file['thumb'],
            'Image' => $file['image']
        ]);
        
        $return[] = [
            'id' => $imageID,
            'thumb' => $file['thumb']
        ];
    }
}

$image = reset($return);
$user_info_service->update(['Photo' => $image['id']], ['UserID' => $_SESSION['UserID']]);

echo json_encode($image);