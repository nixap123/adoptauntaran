<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Upload.php';

$res = Upload::image('uploads/cos', 120);

exit(json_encode($res));