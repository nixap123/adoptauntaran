<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductLang.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();
$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();
$product_service = new Product();
$product_lang_service = new ProductLang();
$product_stock_service = new ProductStock();

$cartItemsQuantity = Request::post('quantity');

if (empty($cartItemsQuantity))
{
    Validator::setError('CartIsEmpty');
}

if (!Validator::hasErrors())
{
    $res = $order_service->getWhere([
        'UserID' => $_SESSION['UserID'],
        'Status' => Order::StatusInCart
    ], 1);
    $current_order = reset($res);

    if (empty($current_order['ID']))
    {
        $orderID = $order_service->insert([
            'UserID' => $_SESSION['UserID'],
            'Amount' => 0,
            'Date' => date('c'),
            'Status' => Order::StatusInCart
        ]);
    }
    else
    {
        $orderID = $current_order['ID'];
    }
    
    $orderAmount = 0;
    
    // check carts
    if (!empty($cartItemsQuantity['Carts']) && is_array($cartItemsQuantity['Carts']))
    {
        foreach ($cartItemsQuantity['Carts'] as $ocID => $ocQuantity)
        {
            $cart = $order_cart_service->getByID($ocID);
            if (!empty($cart['ID']))
            {
                if ($cart['OrderID'] != $orderID && $cart['Status'] == OrderCart::StatusActive)
                {
                    $newCart = $cart;
                    $newCart['OrderID'] = $orderID;
                    $newCart['Status'] = OrderCart::StatusInactive;
                    unset($newCart['ID']);
                    
                    $newCartID = $order_cart_service->insert($newCart);
                    
                    $cartItems = $order_cart_product_service->getWhere([
                        'OrderCartID' => $cart['ID']
                    ]);
                    $cartProdInsert = [];
                    foreach ($cartItems as $key => $ocpRow)
                    {
                        $cartProdInsert[$key] = $ocpRow;
                        $cartProdInsert[$key]['OrderCartID'] = $newCartID;
                        unset($cartProdInsert[$key]['ID']);
                    }
                    $order_cart_product_service->insert_batch($cartProdInsert);
                    
                    $orderAmount += $cart['Price'] * $ocQuantity;
                }
            }
        }
    }
    
    // check products
    if (!empty($cartItemsQuantity['Products']) && is_array($cartItemsQuantity['Products']))
    {
        foreach ($cartItemsQuantity['Products'] as $ocpID => $ocpQuantity)
        {
            $product = $order_cart_product_service->getByID($ocpID);
            if (empty($product['ID']))
            {
                Validator::showError(Lang::t('UnknownProduct'));
            }
            elseif ($ocpQuantity <= 0)
            {
                Validator::setError(Lang::t('InvalidQuantity'));
            }
            else
            {
                $productStock = $product_stock_service->getByID($product['ProductStockID']);
                
                if (empty($productStock['ID']))
                {
                    Validator::setError('UnknownProduct');
                }
                
                if (!Validator::hasErrors())
                {
                    $productStockQuantity = $productStock['Quantity'] - $productStock['OrderedQuantity'];
                
                    if ($productStock['Regular'] == 0 && $productStockQuantity - $ocpQuantity < 0)
                    {
                        $res = $product_lang_service->getWhere([
                            'ProductID' => $productStock['ProductID'],
                            'LangID' => Request::getLangID()
                        ], 1);
                        $productData = reset($res);
                        
                        Validator::setError(Lang::t('OutOfStock') . ' ' . abs($productStockQuantity - $ocpQuantity) . ' un. ' . $productData['Name']);
                    }
                    else
                    {
                        $orderAmount += $product['Price'] * $ocpQuantity;
                        $product_stock_service->query("UPDATE `ProductStock` SET `OrderedQuantity` = `OrderedQuantity` + $ocpQuantity WHERE `ID` = " . $productStock['ID']);
                    }
                }
            }
        }
    }
}

if (!Validator::hasErrors())
{
    $order_service->update([
        'Amount' => $orderAmount,
        'Date' => date('c'),
        'Status' => Order::StatusProccessing
    ], [
        'ID' => $orderID
    ]);
    
    exit(json_encode([
        'valid' => true,
        'messages' => ''
    ]));
}
else
{
    exit(json_encode([
        'valid' => false,
        'messages' => Validator::showMessages(true)
    ]));
}