<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();
$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();
$product_stock_service = new ProductStock();

$res = $order_service->getWhere([
    'UserID' => $_SESSION['UserID'],
    'Status' => Order::StatusInCart
], 1);
$current_order = reset($res);

if (empty($current_order['ID']))
{
    $orderID = $order_service->insert([
        'UserID' => $_SESSION['UserID'],
        'Amount' => 0,
        'Date' => date('c'),
        'Status' => Order::StatusInCart
    ]);
}
else
{
    $orderID = $current_order['ID'];
}

$ocpID = (int)$_POST['ID'];
$type = $_POST['type'];

$valid = true;

if ($type == OrderCart::TypeProduct)
{
    $res = $order_cart_product_service->getWhere(['ID' => $ocpID], 1);
    $ocpData = reset($res);
    
    if (!Validator::hasErrors())
    {
        $order_cart_product_service->deleteByID($ocpData['ID']);
        
        $amount = $ocpData['Price'] * $ocpData['Quantity'];
        $order_service->query("UPDATE `Order` SET `Amount` = `Amount` - $amount WHERE `ID` = $orderID");

        $order_service->query("UPDATE `OrderCart` SET `Price` = `Price` - $amount WHERE `ID` = " . $ocpData['OrderCartID']);
        
        $prods = $order_cart_product_service->getWhere([
            'OrderCartID' => $ocpData['OrderCartID']
        ]);
        
        if (empty($prods))
        {
            $order_cart_service->deleteByID($ocpData['OrderCartID']);
        }
        
        $TVA = $ocpData['Price'] * Config::$TVARate;
        
        $data = [];
        $data['PriceWithoutTVA'] = $ocpData['Price'] - $TVA;
        $data['TotalWithoutTVA'] = $data['PriceWithoutTVA'] * $ocpData['Quantity'];
        $data['TotalTVA'] = $TVA * $ocpData['Quantity'];
        $data['GrandTotalWithoutTVA'] = (float)($ocpData['Quantity'] * $data['PriceWithoutTVA'] * -1);
        $data['GrandTotalTVA'] = (float)($ocpData['Quantity'] * $TVA * -1);
        $data['CartTotal'] = (float)($ocpData['Quantity'] * $ocpData['Price'] * -1);
                
        Validator::setSuccess('Deleted');
    }
    
    echo json_encode([
        'valid' => $valid,
        'messages' => Validator::showMessages(true),
        'data' => $data
    ]);
}
else
{
    $cart = $order_cart_service->getByID($ocpID);
    
    if (empty($cart['ID']))
    {
        Validator::setError('UnknownCart');
    }
    
    if (!Validator::hasErrors())
    {
        $order_cart_service->deleteByID($cart['ID']);
        $order_cart_product_service->delete([
            'OrderCartID' => $cart['ID']
        ]);
        
        $amount = $cart['Price'] * $cart['Quantity'];
        $order_service->query("UPDATE `Order` SET `Amount` = `Amount` - $amount WHERE `ID` = $orderID");
        
        $TVA = $cart['Price'] * Config::$TVARate;
        
        $data = [];
        $data['PriceWithoutTVA'] = $cart['Price'] - $TVA;
        $data['TotalWithoutTVA'] = $data['PriceWithoutTVA'] * $cart['Quantity'];
        $data['TotalTVA'] = $TVA * $cart['Quantity'];
        $data['GrandTotalWithoutTVA'] = (float)($cart['Quantity'] * $data['PriceWithoutTVA'] * -1);
        $data['GrandTotalTVA'] = (float)($cart['Quantity'] * $TVA * -1);
        $data['CartTotal'] = (float)($cart['Quantity'] * $cart['Price'] * -1);
                
        Validator::setSuccess('Deleted');
        
        echo json_encode([
            'valid' => $valid,
            'messages' => Validator::showMessages(true),
            'data' => $data
        ]);
    }
}