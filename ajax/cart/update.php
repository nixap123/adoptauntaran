<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();
$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();
$product_stock_service = new ProductStock();

$res = $order_service->getWhere([
    'UserID' => $_SESSION['UserID'],
    'Status' => Order::StatusInCart
], 1);
$current_order = reset($res);

if (empty($current_order['ID']))
{
    $orderID = $order_service->insert([
        'UserID' => $_SESSION['UserID'],
        'Amount' => 0,
        'Date' => date('c'),
        'Status' => Order::StatusInCart
    ]);
}
else
{
    $orderID = $current_order['ID'];
}

$ocpID = (int)$_POST['ocpID'];
$quantity = (float)$_POST['quantity'];
$type = $_POST['type'];

$valid = true;

if ($type == OrderCart::TypeProduct)
{
    if (!($quantity > 0))
    {
        $valid = false;
        Validator::setError('InvalidQuantity');
    }
    
    if (!Validator::hasErrors())
    {
        $res = $order_cart_product_service->getWhere(['ID' => $ocpID], 1);
        $ocpData = reset($res);
        $productStock = $product_stock_service->getByID(empty($ocpData['ProductStockID']) ? 0 : $ocpData['ProductStockID']);

        if (empty($productStock['ID']))
        {
            $valid = false;
            Validator::setError(Lang::t('UnknownProduct'));
        }
    }
    
    if (!Validator::hasErrors())
    {
        $order_cart_product_service->update([
            'Qantity' => $quantity
        ], [
            'ID' => $ocpData['ID']
        ]);
        
        $amount = $ocpData['Price'] * ($quantity - $ocpData['Quantity']);
        $order_service->query("UPDATE `Order` SET `Amount` = `Amount` + $amount WHERE `ID` = $orderID");

        $order_service->query("UPDATE `OrderCart` SET `Price` = `Price` + $amount WHERE `ID` = " . $ocpData['OrderCartID']);
        
        $order_cart_product_service->update([
            'Quantity' => $quantity
        ], [
            'ID' => $ocpID
        ]);
        
        $TVA = $ocpData['Price'] * Config::$TVARate;
        
        $data = [];
        $data['PriceWithoutTVA'] = $ocpData['Price'] - $TVA;
        $data['TotalWithoutTVA'] = $data['PriceWithoutTVA'] * $quantity;
        $data['TotalTVA'] = $TVA * $quantity;
        $data['GrandTotalWithoutTVA'] = (float)($quantity - $ocpData['Quantity']) * $data['PriceWithoutTVA'];
        $data['GrandTotalTVA'] = (float)($quantity - $ocpData['Quantity']) * $TVA;
        $data['CartTotal'] = (float)($quantity - $ocpData['Quantity']) * $ocpData['Price'];
                
        Validator::setSuccess('Updated');
    }
    
    echo json_encode([
        'valid' => $valid,
        'messages' => Validator::showMessages(true),
        'data' => $data
    ]);
}