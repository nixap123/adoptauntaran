<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();
$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();
$product_stock_service = new ProductStock();

$res = $order_service->getWhere([
    'UserID' => $_SESSION['UserID'],
    'Status' => Order::StatusInCart
], 1);
$current_order = reset($res);

if (empty($current_order['ID']))
{
    $orderID = $order_service->insert([
        'UserID' => $_SESSION['UserID'],
        'Amount' => 0,
        'Date' => date('c'),
        'Status' => Order::StatusInCart
    ]);
}
else
{
    $orderID = $current_order['ID'];
}

$ID = (int)$_POST['ID'];
$quantity = (float)$_POST['quantity'];
$type = $_POST['type'];

$valid = true;

if ($type == OrderCart::TypeCart)
{
    $res = $order_cart_service->getWhere([
        'OrderID' => $orderID,
        'CartWeekID' => $ID,
        'Type' => OrderCart::TypeCart
    ], 1);
    $allreadyAddedCart = reset($res);
    
    
    if (!empty($allreadyAddedCart['ID']))
    {
        Validator::setError(Lang::t('CartAllreadyAdded'));
    }
    
    if (!Validator::hasErrors())
    {
        $cartWeek = $cart_week_service->getByID($ID);
        $cartProducts = $cart_week_product_service->getByCartID($cartWeek['ID']);

        $OrderCartID = $order_cart_service->insert([
            'OrderID' => $orderID,
            'CartWeekID' => $cartWeek['ID'],
            'Price' => $cartWeek['Price'],
            'Quantity' => 1,
            'Type' => OrderCart::TypeCart,
            'Status' => 'Active'
        ]);
        
        $amount = $cartWeek['Price'];
        $order_service->query("UPDATE `Order` SET `Amount` = `Amount` + $amount WHERE `ID` = $orderID");

        $cartProdInsert = [];
        foreach ($cartProducts as $product)
        {
            $cartProdInsert[] = [
                'OrderCartID' => $OrderCartID,
                'ProductStockID' => $product['psID'],
                'Price' => $product['Price'],
                'Quantity' => $product['Quantity']
            ];
        }
        $order_cart_product_service->insert_batch($cartProdInsert);
        
        Validator::setSuccess(Lang::t('AddedToCart') );
    }
    else
    {
        $valid = false;
    }
    
    echo json_encode([
        'valid' => $valid,
        'messages' => Validator::showMessages(true)
    ]);
}
else
{
    $productStock = $product_stock_service->getByID($ID);
        
    if (empty($productStock['ID']))
    {
        Validator::setError('UnknownProduct');
    }
    
    if (!Validator::hasErrors())
    {
        $res = $order_cart_service->getWhere([
            'OrderID' => $orderID,
            'CartWeekID' => 0,
            'Type' => OrderCart::TypeProduct
        ], 1);
        $allreadyAddedCart = reset($res);

        if (isset($allreadyAddedCart['ID']))
        {
            $OrderCartID = $allreadyAddedCart['ID'];
        }
        else
        {
            $OrderCartID = $order_cart_service->insert([
                'OrderID' => $orderID,
                'CartWeekID' => 0,
                'Price' => 0,
                'Quantity' => 1,
                'Type' => OrderCart::TypeProduct,
                'Status' => 'Active'
            ]);
        }

        $amount = $productStock['Price'] * $quantity;
        $order_service->query("UPDATE `Order` SET `Amount` = `Amount` + $amount WHERE `ID` = $orderID");

        $order_service->query("UPDATE `OrderCart` SET `Price` = `Price` + $amount WHERE `ID` = $OrderCartID");

        $res = $order_cart_product_service->getWhere([
            'OrderCartID' => $OrderCartID,
            'ProductStockID' => $ID
        ], 1);
        $allreadyAddedProduct = reset($res);
        
        if (isset($allreadyAddedProduct['ID']) && $allreadyAddedProduct['Price'] == $productStock['Price'])
        {
            $order_cart_product_service->query("UPDATE `OrderCartProduct` SET `Quantity` = `Quantity` + $quantity WHERE `ID` = " . $allreadyAddedProduct['ID']);
        }
        else
        {
            $order_cart_product_service->insert([
                'OrderCartID' => $OrderCartID,
                'ProductStockID' => $productStock['ID'],
                'Price' => $productStock['Price'],
                'Quantity' => $quantity
            ]);
        }
                
        Validator::setSuccess(Lang::t('AddedToCart'));
    }
    
    echo json_encode([
        'valid' => $valid,
        'messages' => Validator::showMessages(true)
    ]);
}