<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();

$res = $order_service->getInCartOrder($_SESSION['UserID']);

$count = [];
$total = [];
foreach ($res as $row)
{
    if ($row['Type'] == OrderCart::TypeCart)
    {
        $count['cart'][$row['CartWeekID']] += 1;
        $total[$row['CartWeekID']] = $row['CartPrice'] * $row['CartQuantity'];
    }
    else
    {
        $count['product'] += 1;
        $total[$row['ocpID'] . 'p'] = $row['ProductPrice'] * $row['ProductQuantity'];
    }
}

exit(json_encode([
    'count' => count($count['cart']) + $count['product'],
    'total' => array_sum($total)
]));