<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();
$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();
$product_stock_service = new ProductStock();

$res = $order_service->getWhere([
    'UserID' => $_SESSION['UserID'],
    'Status' => Order::StatusInCart
], 1);
$current_order = reset($res);

if (empty($current_order['ID']))
{
    $orderID = $order_service->insert([
        'UserID' => $_SESSION['UserID'],
        'Amount' => 0,
        'Date' => date('c'),
        'Status' => Order::StatusInCart
    ]);
}
else
{
    $orderID = $current_order['ID'];
}

$ocID = (int)$_POST['ID'];

$cart = $order_cart_service->getByID($ocID);
    
if (empty($cart['ID']))
{
    Validator::setError('UnknownCart');
}

if (!Validator::hasErrors())
{
    $amount = $cart['Price'] * $cart['Quantity'];
    
    $order = $order_service->getByID($cart['OrderID']);
    
    if (isset($order['ID']) && $order['Amount'] != 0)
    {
        $order_service->query("UPDATE `Order` SET `Amount` = `Amount` - $amount WHERE `ID` = " . $order['ID']);
    }

    $TVA = $cart['Price'] * Config::$TVARate;

    $data = [];
    $data['PriceWithoutTVA'] = $cart['Price'] - $TVA;
    $data['TotalWithoutTVA'] = $data['PriceWithoutTVA'] * $cart['Quantity'];
    $data['TotalTVA'] = $TVA * $cart['Quantity'];
    $data['GrandTotalWithoutTVA'] = (float)($cart['Quantity'] * $data['PriceWithoutTVA'] * -1);
    $data['GrandTotalTVA'] = (float)($cart['Quantity'] * $TVA * -1);
    $data['CartTotal'] = (float)($cart['Quantity'] * $cart['Price'] * -1);
    
    $order_cart_service->update([
        'Status' => OrderCart::StatusInactive,
    ], [
        'ID' => $ocID
    ]);

    Validator::setSuccess('Deleted');

    echo json_encode([
        'valid' => true,
        'messages' => Validator::showMessages(true),
        'data' => $data
    ]);
}
else
{
    echo json_encode([
        'valid' => false,
        'messages' => Validator::showMessages(true),
        'data' => $data
    ]);
}