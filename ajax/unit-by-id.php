<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';

$product_service = new Product();

$ProductID = (int)$_POST['ProductID'];

$product = $product_service->getByID($ProductID);

exit(Unit::getName($product['UnitID']));