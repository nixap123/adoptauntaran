<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CategoryLang.php';
    
    $category_service = new Category();
    $category_lang_service = new CategoryLang();
    
    $CategoryID = isset($_POST['CategoryID']) ? (int)$_POST['CategoryID'] : 0;
    
    if ($CategoryID == 0)
    {
        $CategoryID = $category_service->insert([
            'Position' => 0
        ]);
    }
    
    $category_lang_service->delete(['CategoryID' => $CategoryID]);
        
    $names = $_POST['Name'];
    foreach ($names as $langID => $name)
    {
        $category_lang_service->insert([
            'CategoryID' => $CategoryID,
            'LangID' => $langID,
            'Name' => $name
        ]);
    }
    
    Validator::setSuccess();
    
    echo json_encode([
        'categoryID' => $CategoryID,
        'message' => Validator::showMessages()
    ]);