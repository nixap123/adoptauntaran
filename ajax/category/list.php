<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
    
    $category_service = new Category();
    
    echo $category_service->getTree();