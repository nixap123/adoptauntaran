<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
    
    $category_service = new Category();
    
    $CategoryID = (int)$_POST['CategoryID'];
    
    $res = $category_service->getForEdit($CategoryID);
    
    $arr = [];
    foreach ($res as $row)
    {
        $arr[$row['LangID']] = $row['Name'];
    }
    
    foreach (Config::$LangList as $langID => $lang) { ?>

    <div class="form-group">
        <label class="control-label"><?= Lang::t('Label') ?> (<?= $lang['name'] ?>)</label>
        <input type="text" name="Name[<?= $langID ?>]" value="<?= isset($arr[$langID]) ? $arr[$langID] : '' ?>" class="form-control" />
    </div>

    <?php } ?>

<input type="hidden" name="CategoryID" value="<?= $CategoryID ?>" />