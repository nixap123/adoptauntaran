<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CategoryLang.php';

$category_service = new Category();
$category_lang_service = new CategoryLang();

$CategoryID = (int)$_POST['CategoryID'];

$category_service->deleteByID($CategoryID);
$category_lang_service->delete(['CategoryID' => $CategoryID]);