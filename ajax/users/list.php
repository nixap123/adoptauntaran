<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserRating.php';

$user_service = new User();

$type = Request::post('type') == User::TypeGal ? User::TypeGal : User::TypeProducator;
$limit = (int)Request::post('limit');
$offset = (int)Request::post('offset');

switch (Request::post('sort'))
{
    case 'rating':
        $sort = 'Rating desc';
        break;
    case 'alfabetic':
        $sort = 'ui.Name asc';
        break;
    case 'implicit':
        $sort = 'u.ID desc';
        break;
    default:
        $sort = 'u.ID desc';
        break;
}

$res = $user_service->getNearestsUserIDs($_SESSION['UserID'], (array)$type);

if (empty($res)) $res = [0];

$ids = array_keys($res);

$res = $user_service->getByCondition($ids, $type, $limit, $offset, $sort);

foreach ($res as $row)
{
    if ($row['Type'] == User::TypeGal)
    {
        $res3 = $user_service->query("select distinct u.ID, ui.Name from User as u left join UserInfo as ui on ui.UserID = u.ID where u.`ParentID` = " . $row['ID'] . " and `Status` = 'Active' limit 3");
        $result3 = [];
        foreach ($res3 as $row1)
        {
            $result3[] = '<a style="float: none;display: inline; padding: 3px 0; background: transparent; color: inherit !important;" href="' . Url::link($row1['ID'] . '-' . Url::strToUrl($row1['Name'])) . '">' . $row1['Name'] . '</a>';
            if (count($result3) > 3) break;
        }
        $result3count = count($res3);
    }
    else
    {
        $res3 = $user_service->query("select distinct pl.Name from ProductStock as ps left join ProductLang as pl on pl.ProductID = ps.ProductID and pl.LangID = " . Request::getLangID() . " where ps.`UserID` = " . $row['ID'] . " and (ps.ExpirationDate > now() or ps.Regular = 1)");
        $result3 = [];
        foreach ($res3 as $row1)
        {
            $result3[] = $row1['Name'];
            if (count($result3) > 3) break;
        }
        $result3count = count($res3);
    }
?>
<div style="display: none;" class="producatori-item">
    <div class="producatori-item-img">
        <img src="<?= Url::link('uploads/profile/' . $row['Thumb']) ?>">
    </div>
    <strong><?= $row['Name'] ?></strong>
    <span><?= implode(', ', $result3) ?><?= $result3count > 3 ? ', ...' : '' ?></span>
    <?php if ($row['Type'] == User::TypeGal) { ?>
    <?php $res2 = $user_service->query("select count(*) as count from User where `ParentID` = " . $row['ID'] . " and `Status` = 'Active'"); $res2 = reset($res2); ?>
    <strong><?= $res2['count'] ?> <?= Lang::t('Manufacturers') ?></strong>
    <?php } else { ?>
    <strong><?= $row['ProductCount'] ?> <?= Lang::t('Products') ?></strong>
    <?php } ?>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-6">
            <?= UserRating::ratingWidget($row['ID']) ?>
        </div>
        <div class="col-md-6">
            <a href="<?= Url::link($row['ID'] . '-' . Url::strToUrl($row['Name'])) ?>"><?= Lang::t('More') ?></a>
        </div>								
    </div>
</div>
<?php
}