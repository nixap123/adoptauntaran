<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Upload.php';

$res = Upload::image('uploads/cos', 120);

$return = [];
if (count($res) > 0)
{
    $image_service = new Image();
    
    foreach ($res as $file)
    {
        $imageID = $image_service->insert([
            'Thumb' => $file['thumb'],
            'Image' => $file['image']
        ]);
        
        $return[] = [
            'id' => $imageID,
            'thumb' => $file['thumb'],
        ];
    }
}

echo json_encode($return);