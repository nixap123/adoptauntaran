<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$cart_week_service = new CartWeek();

$cart_list = $cart_week_service->getListByUserID($_SESSION['UserID']);

?>

<table id="cos-table" class="table dataTable">
    <thead>
        <tr>
            <th><?= Lang::t('CartDate') ?></th>
            <th><?= Lang::t('CartDescription') ?></th>
            <th class="text-center"><?= Lang::t('Price') ?></th>
            <th class="text-center"><?= Lang::t('Quantity') ?></th>
            <th><?= Lang::t('Edit') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($cart_list as $cart) { ?>
        <?php $cartData = reset($cart) ?>
        <tr>
            <td><?= DateHelper::toDMY($cartData['CartCreatedDate']) . ' - ' . DateHelper::toDMY($cartData['CartDate']) ?></td>
            <td>
                <?php $products = []; ?>
                <?php foreach ($cart as $product) { ?>
                    <?php $products[] = $product['ProductName'] . ' ' . (float)$product['ProductQuantity'] . ' ' . Unit::getName($product['UnitID']) ?>
                <?php } ?>
                <?= implode(',  ', $products) ?>
            </td>
            <td class="text-center"><?= (float)$cartData['CartPrice'] ?></td>
            <td class="text-center"><?= (floatval($cartData['CartQuantity'])) ?></td>
            <td>
                <a data-toggle="modal" data-target=".edit-cos-modal" data-cos-id="<?= $cartData['CartWeekID'] ?>" class="edit-cos" style="cursor: pointer;"><i class="fa fa-pencil blue"></i></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>

    $('#cos-table').dataTable();

</script>