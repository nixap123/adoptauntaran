<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$product_stock_service = new ProductStock();

$CategoryID = (int)$_POST['CategoryID'];

$result = $product_stock_service->getUserProductsByCategory($CategoryID, $_SESSION['UserID']);

$html = '<option value="0">- ' . Lang::t('SelectProduct') . ' -</option>';
foreach ($result as $row)
{
    $html .= '<option sold="' . $row['Sold'] . '" value="' . $row['ID'] . '">' . $row['Name'] . ' (' . (float)$row['Sold'] . Unit::getName($row['UnitID']) . ' / ' . $row['Price'] . 'lei)</option>';
}

exit($html);