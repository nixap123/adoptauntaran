<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';

$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();

$cosID = (int)$_POST['cosID'];

$cosProducts = [];
if ($cosID > 0)
{
    $cosData = $cart_week_service->getForEdit($cosID, $_SESSION['UserID']);
    $cos = reset($cosData);
    
    $cosProducts = $cart_week_product_service->getByCartID($cosID);
}

$CartPrice = isset($cos['CartPrice']) ? $cos['CartPrice'] : 0;
$CartQuantity = isset($cos['CartQuantity']) ? $cos['CartQuantity'] : 0;
$CartDate = isset($cos['CartDate']) ? DateHelper::toDMY($cos['CartDate']) : date('d.m.Y', time() + 604800);
$Regular = isset($cos['Regular']) ? $cos['Regular'] : 0;
$Photo = isset($cos['Photo']) ? $cos['Photo'] : '';
$StartDate = isset($cos['StartDate']) ? DateHelper::toDMY($cos['StartDate']) : date('d.m.Y', time());

?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Cartprice') ?></label>
            <input class="form-control" name="CartPrice" value="<?= $CartPrice ?>" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('CartQuantity') ?></label>
            <input class="form-control" name="CartQuantity" value="<?= $CartQuantity ?>" />
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('ValidFrom') ?></label>
            <?php if ($cosID > 0) { ?>
            <input class="form-control" readonly name="StartDate" value="<?= $StartDate ?>" />
            <?php } else { ?>
            <input class="form-control datepicker" name="StartDate" value="<?= $StartDate ?>" />
            <?php } ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('ValidTo') ?></label>
            <?php if ($cosID > 0) { ?>
            <input class="form-control" readonly name="CartDate" value="<?= $CartDate ?>" />
            <?php } else { ?>
            <input class="form-control datepicker" name="CartDate" value="<?= $CartDate ?>" />
            <?php } ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <input class="checkbox" <?= $Regular ? 'checked' : '' ?> name="Regular" value="1" type="checkbox"> <?= Lang::t('Permanent') ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Image') ?></label>
            <input type="file" class="form-control ajax-image" name="Photo" />
        </div>
        <div id="image-wrap">
            <?php if ($cos['Image']) { ?>
            <img class="thumbnail" src="<?= Url::link('uploads/cos/' . $cos['Image']) ?>" />
            <?php } ?>
        </div>
    </div>
    <input type="hidden" name="cosID" value="<?= $cosID ?>" />
</div>
<hr />
<div id="add-product-to-cos-form" class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Category') ?></label>
            <?= Category::categorySelect() ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Product') ?></label>
            <select id="added-product" class="form-control"></select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label"><?= Lang::t('Quantity') ?></label>
            <input type="number" step="0.5" min="0.5" class="form-control" id="added-product-quntity" />
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label hidden-sm hidden-xs">&nbsp;</label>
            <button type="button" class="btn btn-success btn-block" id="add-product-to-cos"><i class="fa fa-plus"></i> <?= Lang::t('Add') ?></button>
        </div>
    </div>
</div>
<div id="cos-product-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= Lang::t('Product') ?></th>
                <th><?= Lang::t('Category') ?></th>
                <th class="text-center"><?= Lang::t('Quantity') ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cosProducts as $psID => $row) { ?>
            <tr>
                <td>
                    <input type="hidden" name="psID[<?= $psID ?>]" value="<?= $psID ?>" />
                    <?= $row['ProductName'] ?>
                </td>
                <td>
                    <?= $row['CategoryName'] ?>
                </td>
                <td class="text-center">
                    <input type="hidden" name="psQuantity[<?= $psID ?>]" value=" <?= $row['Quantity'] ?>" />
                     <?= $row['Quantity'] ?>
                </td>
                <td>
                    <a onclick="$(this).closest('tr').remove()" style="cursor:pointer;">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $('input[name=Regular]').click(function () {
        if ($(this).is(':checked'))
        {
            $('input[name=ExpirationDate]').attr('disabled', 'disabled');
        } else
        {
            $('input[name=ExpirationDate]').removeAttr('disabled');
        }
    });
    
    $('select[name=CategoryID]').change(function(){
        $.post('<?= Url::link('ajax/cos/products-by-category.php') ?>', {CategoryID: $(this).val()}, function(html){
            $('#added-product').html(html);
        });
    });
    
    $('select#added-product').change(function(){
        var sold = $(this).find('option:selected').attr('sold');
        $('#added-product-quntity').attr('max', sold);
    });
    
    $('.datepicker').datepicker({
        'format': 'dd.mm.yyyy'
    });
</script>