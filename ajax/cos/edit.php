<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeekProduct.php';

$product_stock_service = new ProductStock();
$cart_week_service = new CartWeek();
$cart_week_product_service = new CartWeekProduct();

$cosID = (int)Request::post('cosID');

$CartPrice = Validator::validate('CartPrice', Validator::ValidateNumber, Lang::t('CartPrice'));
$CartQuantity = Validator::validate('CartQuantity', Validator::ValidateNumber, Lang::t('CartQuantity'));
$ImageID = Validator::validate('ImageID', Validator::ValidateEmpty, Lang::t('Image'));
$Regular = Request::post('Regular') == 'on' ? 1 : 0;

if ($Regular == 0)
{
    $CartDate = Validator::validate('CartDate', Validator::ValidateEmpty, Lang::t('CartDate'));
}

$cartProductsIDs = Validator::validate('psID', Validator::ValidateEmpty, Lang::t('AddProducts'));
$cartProductsQuantity = (array)Request::post('psQuantity');

// calc new product quantity difference in cart
$stockDifference = [];
if (!Validator::hasErrors())
{
    
    $productStocks = $product_stock_service->getActiveProductStocks($_SESSION['UserID']);

    // get new stocks needed to reservation
    foreach ($cartProductsQuantity as $psID => $psQuantity)
    {
        $stockDifference[$psID] = $psQuantity * $CartQuantity;
    }

    if ($cosID > 0)
    {
        $cosData = $cart_week_service->getByID($cosID);
        $cosProductsList = $cart_week_service->getForEdit($cosID, $_SESSION['UserID']);

        foreach ($cosProductsList as $psID => $row)
        {
            if (isset($stockDifference[$psID]))
            {
                $stockDifference[$psID] -= $row['ProductQuantity'] * $cosData['Quantity'];
            }
            else
            {
                $stockDifference[$psID] = $row['ProductQuantity'] * -1 * $cosData['Quantity'];
            }
        }
    }
    
}

// check product avalaibility in stocks
foreach ($stockDifference as $psID => $newQuantity)
{
    $diff = $productStocks[$psID]['Quantity'] - ($productStocks[$psID]['OrderedQuantity'] + $newQuantity);
    if ($diff < 0)
    {
        Validator::setError('' . $productStocks[$psID]['ProductName'] . ' - ' . abs($diff) . ' ' . Unit::getName($productStocks[$psID]['UnitID']));
    }
}


// insert or update cart and update product stocks
if (!Validator::hasErrors())
{
    // insert or update cart
    if (!($cosID > 0))
    {
        $cosID = $cart_week_service->insert([
            'UserID' => $_SESSION['UserID'],
            'Price' => $CartPrice,
            'Quantity' => $CartQuantity,
            'ExpirationDate' => date('c', strtotime($CartDate)),
            'Regular' => $Regular,
            'Photo' => $ImageID,
            'Status' => CartWeek::StatusActive
        ]);
    }
    else
    {
        $cart_week_service->update([
            'Price' => $CartPrice,
            'Quantity' => $CartQuantity,
            'ExpirationDate' => date('c', strtotime($CartDate)),
            'Regular' => $Regular,
            'Photo' => $ImageID,
            'Status' => CartWeek::StatusActive
        ], ['ID' => $cosID]);
    }
    
    // insert cart products
    $cart_week_product_service->delete(['CartWeekID' => $cosID]);
    foreach ($cartProductsQuantity as $psID => $psQuantity)
    {
        $cart_week_product_service->insert([
            'CartWeekID' => $cosID,
            'ProductStockID' => $psID,
            'Quantity' => $psQuantity,
            'Price' => $productStocks[$psID]['Price']
        ]);
    }
    
    // update product stocks
    foreach ($stockDifference as $psID => $newQuantity)
    {
        //$product_stock_service->update(['OrderedQuantity' => 'OrderedQuantity + ' . $newQuantity], ['ID' => $psID]);
        $product_stock_service->query("UPDATE `ProductStock` SET `OrderedQuantity` = `OrderedQuantity` + " . $newQuantity . " WHERE ID = " . $psID . " AND UserID = " . $_SESSION['UserID']);
    }
    
    Validator::setSuccess(Lang::t('ProductSaved'));
}

exit(json_encode([
    'error' => Validator::hasErrors(),
    'messages' => Validator::showMessages(),
    'cosID' => $cosID
]));