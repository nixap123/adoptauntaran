<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();

$order = $order_service->getByID((int)$_POST['orderID']);

$order_carts = $order_cart_service->getWhere([
    'OrderID' => $order['ID']
]);

$orderCartIDs = [];
foreach ($order_carts as $cart)
{
    $orderCartIDs[] = $cart['ID'];
}

$res = $order_cart_product_service->query("select ocp.ID as ocpID from `OrderCartProduct` as ocp "
                                        . "right join ProductStock as ps on ps.ID = ocp.ProductStockID and ps.UserID = " . $_SESSION['UserID'] . " "
                                        . "where ocp.OrderCartID in (" . implode(', ', $orderCartIDs) . ")");

foreach ($res as $row)
{
    $order_cart_product_service->update([
        'Delivered' => Order::DeliveryStatusLivrat
    ], [
        'ID' => $row['ocpID']
    ]);
}

$res = $order_cart_product_service->query("select ocp.`Delivered` as Delivered from `OrderCartProduct` as ocp "
                                        . "where ocp.OrderCartID in (" . implode(', ', $orderCartIDs) . ")");

$delivered_arr = [];
foreach ($res as $row)
{
    $delivered_arr[] = $row['Delivered'];
}

if (!in_array(Order::DeliveryStatusNeridicat, $delivered_arr))
{
    $order_service->update([
        'Delivered' => Order::DeliveryStatusLivrat,
        'Status' => Order::StatusPaid
    ], [
        'ID' => $order['ID']
    ]);
}