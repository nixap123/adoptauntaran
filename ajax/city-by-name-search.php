<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';

$city_service = new City();

if (isset($_GET['locationID']) && isset($_GET['distance']))
{
    $Location = $city_service->getByID($_GET['locationID']);
    
    $res = $city_service->getNearestCities($Location['Lat'], $Location['Long'], (int)$_GET['distance'], $_GET['q']);
}
else
{
   $res = $city_service->query("SELECT c.ID, c.Name, j.Name as JudetName FROM `City` as c left join `Judet` as j on j.ID = c.JudetID where c.Name like '%" . $city_service->escape($_GET['q']) . "%'");
}


$result = [];
foreach ($res as $row)
{
    $result[] = [
        'id' => $row['ID'],
        'text' => $row['Name'] . ' (' . $row['JudetName'] .')'
    ];
}

exit(json_encode($result));