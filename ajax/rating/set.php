<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserRating.php';

$user_rating_service = new UserRating();

$userID = (int)$_POST['userID'];
$vote = (int)$_POST['vote'];
$success = true;

$allreadyVoted = $user_rating_service->getWhere([
    'UserID' => $userID,
    'SenderID' => $_SESSION['UserID']
]);

if (empty($_SESSION['UserID']))
{
    Validator::setError(Lang::t('PleaseLogin'));
}

if (!Validator::hasErrors())
{
    $vote = $vote > 5 ? 5 : $vote;
    $vote = $vote < 1 ? 1 : $vote;
    
    if (count($allreadyVoted) > 0)
    {
        $user_rating_service->update([
            'Rating' => $vote
        ], [
            'UserID' => $userID,
            'SenderID' => $_SESSION['UserID']
        ]);
    }
    else
    {
        $user_rating_service->insert([
            'UserID' => $userID,
            'SenderID' => $_SESSION['UserID'],
            'Rating' => $vote
        ]);
    }
}
else
{
    $success = false;
}

exit(json_encode([
    'success' => $success,
    'messages' => Validator::showMessages(true)
]));