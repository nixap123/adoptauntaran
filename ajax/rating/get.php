<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserRating.php';

$userID = (int)$_POST['userID'];

echo UserRating::ratingWidget($userID);