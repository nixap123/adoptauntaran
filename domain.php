<?php

if ($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR'] || $_SERVER['SERVER_ADDR'] == '127.0.0.1') exit('Access denided!');

// domain config
define('ROOT_DOMAIN', 'adoptauntaran.ro');
define('DOMAIN_DIR', 'public_html/dev');
define('USER_PASSWORD', uniqid('gal_'));

// cookie file for CURL
define('COOKIE_FILE', $_SERVER['DOCUMENT_ROOT'] . '/api/cookies.txt');

// CPanel access
define('CPANEL_URL', 'https://srv.enflux.ro:2083'); // CPanel url without trailing slash
define('CPANEL_USER', 'adoptauntaran'); // cpanel root user name
define('CPANEL_PASSWORD', 'Par0la132##'); // cpanel root user password

function getToken()
{
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_USERPWD, CPANEL_USER . ":" . CPANEL_PASSWORD);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"); 
    curl_setopt($ch, CURLOPT_URL, CPANEL_URL);
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    
    return $info;
}

// get access token for CPanel
do {
    $info = getToken();
}
while ($info['http_code'] != '200');

$url = str_replace('https://', '', $info['url']);
$arr = array_filter(explode('/', $url));
$token = $arr[1];

// add domain
$sNewDomain = $_GET['domain'];
$sNewDomainUser = $_GET['user'];
$sNewDomainPass = 'f3Wh1.p+t_+p';
$sNewDomainFolder = 'public_html/dev/';

$sNewDomain = urlencode($sNewDomain);
$sNewDomainUser = urlencode($sNewDomainUser);
$sNewDomainPass = urlencode($sNewDomainPass);
$sNewDomainFolder = urlencode($sNewDomainFolder);

$post = "domain=$sNewDomain&user=$sNewDomainUser&root_domain=" . ROOT_DOMAIN . "&dir=" . urlencode(DOMAIN_DIR) . "&pass=" . urlencode(USER_PASSWORD) . "&pass2=" . urlencode(USER_PASSWORD) . "&go=Add+Domain";

$ch = curl_init(); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"); 
curl_setopt($ch, CURLOPT_URL, CPANEL_URL . "/$token/frontend/x3/addon/doadddomain.html");
curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE);
curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

$data = curl_exec($ch);
$info = curl_getinfo($ch);
curl_close($ch);

exit(json_encode($info));