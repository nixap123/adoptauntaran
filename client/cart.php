<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCartProduct.php';

$order_service = new Order();
$order_cart_service = new OrderCart();
$order_cart_product_service = new OrderCartProduct();

$cartItems = $order_service->getCartContents($_SESSION['UserID']);


?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Cart') ?></h2>
        <div class="row">
            <div class="col-md-12 cart-inn">
                <form id="cart-form" method="post">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th><?= Lang::t('Quantity') ?></th>
                                <th><?= Lang::t('UnitPrice') ?></th>
                                <th><?= Lang::t('TotalPrice') ?></th>
                                <th>TVA</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $GrandTotalWithoutTVA = 0; $GrandTotalTVA = 0; $GrandTotal = 0; ?>
                            <?php foreach ($cartItems['Carts'] as $cartID => $cart) { ?>
                            <?php $GrandTotalWithoutTVA += $cart['cart']['TotalWithoutTVA']; ?>
                            <?php $GrandTotalTVA += $cart['cart']['TotalTVA']; ?>
                            <?php $GrandTotal += $cart['cart']['Subtotal']; ?>
                            <tr ocid="<?= $cart['cart']['ocID'] ?>">
                                <td>
                                    <?= Lang::t('WeekCart') ?>: <small><?= implode(', ', $cart['products']) ?></small><br>
                                    <a href="#" onclick="if (confirm('<?= Lang::t('Delete') ?>')) Cart.removeForever(<?= $cart['cart']['ocID'] ?>); return false;"><?= Lang::t('RemoveForever') ?></a>
                                </td>
                                <td><?= (float)$cart['cart']['CartPrice'] ?> lei</td>
                                <td><input type="hidden" name="quantity[Carts][<?= $cart['cart']['ocID'] ?>]" value="1" /></td>
                                <td><span class="PriceWithoutTVA"><?= (float)$cart['cart']['PriceWithoutTVA'] ?></span> lei</td>
                                <td><span class="TotalWithoutTVA"><?= (float)$cart['cart']['TotalWithoutTVA'] ?></span> lei</td>
                                <td><span class="TotalTVA"><?= (float)$cart['cart']['TotalTVA'] ?></span> lei</td>
                                <td><a onclick="return Cart.remove(<?= $cart['cart']['ocID'] ?>, Cart.typeCart)" href="#"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <?php } ?>
                            <?php foreach ($cartItems['Products'] as $ocpID => $product) { ?>
                            <?php $GrandTotalWithoutTVA += $product['TotalWithoutTVA']; ?>
                            <?php $GrandTotalTVA += $product['TotalTVA']; ?>
                            <?php $GrandTotal += $product['Subtotal']; ?>
                            <tr ocpid="<?= $product['ocpID'] ?>">
                                <td><?= $product['ProductName'] ?> de la <?= $product['UserName'] ?></td>
                                <td><?= (float)$product['ProductPrice'] ?> lei / <?= Unit::getName($product['UnitID']) ?></td>
                                <td><input type="number" name="quantity[Products][<?= $product['ocpID'] ?>]" onchange="Cart.update(<?= $product['ocpID'] ?>, parseFloat($(this).val()))" value="<?= (float)$product['ProductQuantity'] ?>" /></td>
                                <td><span class="PriceWithoutTVA"><?= (float)$product['PriceWithoutTVA'] ?></span> lei</td>
                                <td><span class="TotalWithoutTVA"><?= (float)$product['TotalWithoutTVA'] ?></span> lei</td>
                                <td><span class="TotalTVA"><?= (float)$product['TotalTVA'] ?></span> lei</td>
                                <td><a onclick="return Cart.remove(<?= $product['ocpID'] ?>, Cart.typeProduct)" href="#"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong><span id="GrandTotalWithoutTVA"><?= $GrandTotalWithoutTVA ?></span> lei</strong></td>
                                <td><strong><span id="GrandTotalTVA"><?= $GrandTotalTVA ?></span> lei</strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="2" class="text-center"><strong><?= Lang::t('Total') ?>: <span id="CartTotal"><?= $GrandTotalWithoutTVA + $GrandTotalTVA ?></span> lei</strong></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>				
                    
                    <?php if ($GrandTotal > 0) { ?>
                    <button id="order-button" onclick="return Cart.doOrder()" class="arrows-btn">
                        <?= Lang::t('Order') ?><br />
                        <?= Lang::t('Now') ?>!
                    </button>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</section>

<div id="success-order-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= Lang::t('Congratulations') ?>
            </div>
            <div class="modal-body">
                <!--Felicitări! Ați efectuat o comandă nouă către producători. <br/> Aceștia vor lua legătura cu dumneavoastră în cel mai scurt timp! -->
                <?= Lang::t('CongratulationsAdded') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-modal" data-dismiss="modal"><?= Lang::t('Close') ?></button>
            </div>
        </div>
    </div>
</div>


<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/footer.php'; ?>