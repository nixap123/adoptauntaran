<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Bank.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Judet.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';

    $user_service = new User();
    $user_info_service = new UserInfo();

    $userID = $_SESSION['UserID'];

    $user = $user_service->getByID($userID);
    $user_info = $user_info_service->getByUserID($user['ID']);
    
    if (Request::isPost())
    {
        $UserName = Validator::validate('Email',  Validator::ValidateEmail, 'Email');
        $Password = Validator::validate('Password', Validator::ValidateEmpty, 'Parola');
        $Judet = Validator::validate('Judet', Validator::ValidateEmpty, 'Județ');
        $Location = Validator::validate('Location', Validator::ValidateEmpty, 'Localitate');
        $ClientType = Validator::validate('ClientType', Validator::ValidateEmpty, 'Client Type');
        
        $Name = Validator::validate('Name', Validator::ValidateEmpty, 'Nume Companiei / Nume, Prenume');
        $Address = Validator::validate('Address', Validator::ValidateEmpty, 'Adresa');
        $Phone = Request::post('Phone');
        $ContactPerson = Request::post('ContactPerson');
        $Coverage = Request::post('Coverage');
        $Description = Request::post('Description');
        $Cnp = Request::post('Cnp');
        $Cui = Request::post('Cui');
        $BankAccount = Request::post('BankAccount');
        $BankID = Request::post('BankID');
        $RegNumber = Request::post('RegNumber');
        
        if (!Validator::hasErrors())
        {
            $ex_user = $user_service->getWhere(['UserName' => $UserName, 'Status' => User::StatusActive], 1);

            if (isset($ex_user[0]['ID']) && !($userID > 0))
            {
                Validator::setError('Email is already registered');
            }
            
            if (!Validator::hasErrors())
            {
                $userData = [
                    'ParentID' => 0,
                    'UserName' => $UserName,
                    'Password' => $Password,
                    'ClientType' => $ClientType,
                    'Status' => User::StatusActive
                ];
                
                $user_service->update($userData, ['ID' => $userID]);

                $userInfoData = [
                    'Name' => $Name,
                    'Phone' => $Phone,
                    'Address' => $Address,
                    'JudetID' => $Judet,
                    'LocationID' => $Location,
                    'CNP' => $Cnp,
                    'CUI' => $Cui,
                    'RegNumber' => $RegNumber,
                    'BankAccount' => $BankAccount,
                    'BankID' => $BankID,
                    'Description' => $Description,
                    'Coverage' => $Coverage > 0 ? $Coverage : 50,
                    'ContactPerson' => $ContactPerson
                ];

                $user_info_service->update($userInfoData, ['UserID' => $userID]);

                Validator::setSuccess();
            }
        }
    }

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('EditSettings') ?></h2>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <?= Validator::showMessages() ?>
                        <form method="post" class="col-md-12 form-horizontal">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Person') ?></label>
                                        <div class="col-md-7">
                                            <div class="radio">
                                                <label onclick="Register.setFormPF()">
                                                    <input name="ClientType" <?= Request::dataPost($user, 'ClientType', 'ClientType') == User::ClientTypePF ? 'checked' : '' ?> value="PF" type="radio" class="ace">
                                                    <span class="lbl"> <?= Lang::t('Individual') ?></span>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label onclick="Register.setFormPJ()">
                                                    <input name="ClientType" <?= Request::dataPost($user, 'ClientType', 'ClientType') == User::ClientTypePJ ? 'checked' : '' ?> value="PJ" type="radio" class="ace">
                                                    <span class="lbl"> <?= Lang::t('Legal') ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('CompanyNameOrName') ?></label>
                                        <div class="col-md-7">
                                            <input type="text" name="Name" value="<?= Request::dataPost($user_info, 'Name', 'Name') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Email') ?></label>
                                        <div class="col-md-7">
                                            <input type="email" name="Email" value="<?= Request::dataPost($user, 'UserName', 'Email') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Phone') ?></label>
                                        <div class="col-md-7">
                                            <input type="text" name="Phone" value="<?= Request::dataPost($user_info, 'Phone', 'Phone') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div id="reg_number" class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('CompanyRegistrationNumber') ?> *</label>
                                        <div class="col-md-7">
                                            <input type="text" name="RegNumber" value="<?= Request::dataPost($user_info, 'RegNumber', 'RegNumber') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div id="cui" class="form-group">
                                        <label class="col-md-5 control-label">CUI / CIF</label>
                                        <div class="col-md-7">
                                            <input type="text" name="Cui" value="<?= Request::dataPost($user_info, 'CUI', 'Cui') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div id="cnp" class="form-group">
                                        <label class="col-md-5 control-label">CNP</label>
                                        <div class="col-md-7">
                                            <input type="text" name="Cnp" value="<?= Request::dataPost($user_info, 'CNP', 'Cnp') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div id="cont_bancar" class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('BankAccount') ?> *</label>
                                        <div class="col-md-7">
                                            <input type="text" name="BankAccount" value="<?= Request::dataPost($user_info, 'BankAccount', 'BankAccount') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div id="banka" class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Bank') ?></label>
                                        <div class="col-md-7">
                                            <?= Bank::bankSelect(Request::dataPost($user_info, 'BankID', 'BankID')) ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Description') ?></label>
                                        <div class="col-md-7">
                                            <textarea class="form-control" name="Description"><?= Request::dataPost($user_info, 'Description', 'Description') ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">* <?= Lang::t('OptionalFields') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Address') ?></label>
                                        <div class="col-md-7">
                                            <input type="text" name="Address" value="<?= Request::dataPost($user_info, 'Address', 'Address') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('County') ?></label>
                                        <div class="col-md-7">
                                            <?= Judet::judetSelect(Request::dataPost($user_info, 'JudetID', 'Judet')) ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Location') ?></label>
                                        <div class="col-md-7">
                                            <?= City::citySelect(Request::dataPost($user_info, 'JudetID', 'Judet'), Request::dataPost($user_info, 'LocationID', 'Location')) ?>
                                        </div>
                                    </div>

                                    <div id="pers_contact" class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('ContactPerson') ?></label>
                                        <div class="col-md-7">
                                            <input name="ContactPerson" type="text" value="<?= Request::dataPost($user_info, 'ContactPerson', 'ContactPerson') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Password') ?></label>
                                        <div class="col-md-7">
                                            <input name="Password" type="text" value="<?= Request::dataPost($user, 'Password', 'Password') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <!--div class="form-group">
                                        <label class="col-md-5 control-label">Rescriere parola</label>
                                        <div class="col-md-7">
                                            <input name="PasswordConfirmation" type="password" class="form-control" placeholder="">
                                        </div>
                                    </div-->

                                    <div style="display: none;" class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Radius') ?></label>
                                        <div class="col-md-6">
                                            <input name="Coverage" type="number" value="<?= Request::dataPost($user_info, 'Coverage', 'Coverage') ?>" class="form-control col-md-4" placeholder="">
                                        </div>
                                        <span class="help-inline col-xs-12 col-md-1">
                                            <span class="middle">km</span>
                                        </span>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('RegisteredOn') ?></label>
                                        <div class="col-md-7">
                                            <input name="" type="text" readonly value="<?= DateHelper::toDMY($user['RegDate']) ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    
                                    <br />
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"></label>
                                        <div class="col-md-7 text-right">
                                            <button class="btn btn-success" type="submit"><?= Lang::t('Save') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>



                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </div>
</section><!-- /.main-content -->

<script>
    
    PageJS = {
        
        setGalForm: function()
        {
            $('select[name=ParentID]').closest('.form-group').hide();
        },
        
        setProducatorForm: function()
        {
            $('select[name=ParentID]').closest('.form-group').show();
        }
        
    };
    
    $(document).ready(function(){
        
        $('select[name=Judet]').change(function(){
            $.post('<?= Url::link('ajax/city-by-judet.php') ?>', {JudetID: $(this).val()}, function(html){
                $('select[name=Location]').html(html);
            });
        });
        
        $('input[name=AccountType]').change(function(){
            if ($(this).val() == '<?= User::TypeGal ?>')
            {
                PageJS.setGalForm();
            }
            else
            {
                PageJS.setProducatorForm();
            }
        });
        
        <?php if (Request::dataPost($user, 'Type', 'AccountType') == User::TypeGal) { ?>
        PageJS.setGalForm();
        <?php } else { ?>
        PageJS.setProducatorForm();
        <?php } ?>
            
        <?php if (isset($_GET['galID'])) { ?>
         $('select[name=ParentID]').val(parseInt('<?= $_GET['galID'] ?>'));
        <?php } ?>
        
    });

</script>

<script>

    var Register = {
        setFormPF: function ()
        {
            $('#cnp').show().find('input').attr('required', 'required');
            $('#cui, #reg_number, #banka, #cont_bancar, #pers_contact').hide().find('input').removeAttr('required');
            $('#name-label').text('Nume / prenume');
            $('#firm-name-control').hide();
        },
        setFormPJ: function ()
        {
            $('#cnp').hide().find('input').removeAttr('required');
            $('#cui, #reg_number, #banka, #cont_bancar, #pers_contact').show();
            $('#cui input').attr('required', 'required');
            $('#name-label').text('Nume / prenume delegat');
            $('#firm-name-control').show();
        }
    };

<?= Request::post('ClientType') == 'PJ' ? '$("input[name=ClientType]).click()' : 'Register.setFormPF();' ?>

</script>
<style>
</style>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/footer.php'; ?>