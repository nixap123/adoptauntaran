<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php'; 
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Notification.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';

$user_service = new User();
$user_info_service = new UserInfo();
$image_service = new Image();
$notification_service = new Notification();
$order_service = new Order();

$USER = $user_service->getByID($_SESSION['UserID']);

$USER_INFO = $user_info_service->getByUserID($USER['ID']);

$user_logo = $image_service->getByID($USER_INFO['Photo']);

$unread_notifications_count = $notification_service->getCountUnreaded($_SESSION['UserID']);

$orders_total = $order_service->getClientOrdersSum($_SESSION['UserID']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Adopta un taran</title>
        <meta charset="UTF-8" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/styles.css') ?>" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap-theme.min.css') ?>">

        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?= Url::link('assets/js/fancybox/jquery.fancybox.css') ?>">

        <script src="<?= Url::link('assets/js/jquery.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/main.js') ?>"></script>
        <script src="<?= Url::link('assets/js/product/cart.js') ?>"></script>
        <script src="<?= Url::link('assets/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= Url::link('assets/js/jquery.noty.packaged.min.js') ?>"></script>

        <link rel="stylesheet" type="text/css" href="<?= Url::link('assets/css/imgareaselect-default.css') ?>" />
        <script src="<?= Url::link('assets/js/fancybox/jquery.fancybox.js') ?>"></script>
        
        <script>
        function notification(text, type, timeout) {
            var n = noty({
                text        : text,
                type        : type,
                dismissQueue: true,
                timeout     : timeout == undefined ? 3000 : timeout,
                closeWith   : ['click'],
                layout      : 'topRight',
                theme       : 'defaultTheme',
                maxVisible  : 10
            });
        }
        </script>
        
    </head>
    <body class="client-panel">

        <div class="top-bar ">
            <div class="container">
                <div class="row">
                    <div class="text-left donate-bar col-md-6">
                        <i class="fa fa-credit-card"></i> <a href="<?= Url::link('/doneaza') ?>"><?= Lang::t('DonateForThisProject') ?></a>
                    </div>
                    <div class="text-right user-bar col-md-6">
                        <a href="<?= Url::switchLang('ro') ?>"><img src="/assets/images/ro.png" /></a>
                        <a href="<?= Url::switchLang('en') ?>"><img src="/assets/images/en.png" /></a>&nbsp;&nbsp;&nbsp;
                        <?php if ($USER['Type'] == User::TypeProducator) { ?>
                        <a class="notification-menu" href="<?= Url::link('producator/index') ?>"> <?= Lang::t('ManufacturerPanel') ?></a> | 
                        <?php } ?>
                        <a class="notification-menu" href="<?= Url::link('client/notification.php') ?>"><span class="unread-notifications"><?= $unread_notifications_count ?></span> <?= Lang::t('Notification') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('client/setari/index') ?>"> <?= Lang::t('Settings') ?></a> | 
                        <a href="<?= Url::link('logout.php') ?>"><?= Lang::t('Logout') ?></a>
                    </div>
                </div>
            </div>
        </div>

        <header class="header acc">
            <div class="container">
                <div class="row">
                    <div class="logo-bar col-md-2">
                        <a href="<?= Url::link('client/index') ?>" title=""><img src="<?= Url::link('assets/images/logo.png') ?>" alt=""></a>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('client/cart') ?>">
                                        <i class="fa fa-shopping-cart"></i>
                                        <strong><count id="cart-price"></count><sup>lei</sup></strong>
                                        <span><?= Lang::t('ProductsInCart') ?>: <count id="cart-count"></count></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('client/comenzi') ?>">
                                        <i class="fa fa-shopping-basket"></i>
                                        <strong><?= Order::clientOrderCount($_SESSION['UserID']) ?> (<?= Order::clientCartCount($_SESSION['UserID']) ?>)</strong>
                                        <span><?= Lang::t('Orders') ?> (recursive)</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="acc-cart">
                                    <a>
                                        <i class="fa fa-dot-circle-o"></i>
                                        <strong><?= $orders_total ?><sup>lei</sup></strong>
                                        <span><?= Lang::t('TotalOrders') ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="user-bar col-md-3">
                                <div class="acc-inn">
                                    <a id="acc-photo" class="acc-photo">
                                        <?php if (!empty($user_logo['ID'])) { ?>
                                        <img src="<?= Url::link('uploads/profile/' . $user_logo['Thumb']) ?>" />
                                        <?php } else { ?>
                                        <img src="<?= Url::link('assets/images/no_profile_img.jpg') ?>" />
                                        <?php } ?>
                                    </a>
                                    <input class="hidden" id="inputfile-profile" type="file" name="" />
                                    <div class="clearfix"></div>
                                    <div class="dropdown">
                                        <p id="open-profile" data-toggle="dropdown" role="button">
                                            <span><?= Lang::t('Welcome') ?>, <?= $USER_INFO['Name'] ?>!</span>
                                            <i class="fa fa-caret-down"></i>
                                        </p>
                                        <ul class="pull-right dropdown-menu" aria-labelledby="open-profile">
                                            <li><a href="<?= Url::link('client/setari/index.php') ?>"><i class="fa fa-cogs"></i> <?= Lang::t('Settings') ?></a></li>
                                            <li><a href="<?= Url::link('logout.php') ?>"><i class="fa fa-sign-out"></i> <?= Lang::t('Logout') ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="nat-patt"></div>
        </header>

