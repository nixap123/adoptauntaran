<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserRating.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/News.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$user_service = new User();
$cart_week_service = new CartWeek();
$product_stock_service = new ProductStock();
$news_service = new News();

$nearest_users = $user_service->getNearestsUserIDs($_SESSION['UserID']);

$ids = array_keys($nearest_users);

$carts = $cart_week_service->getCurrentWeekCartsByUserIDs($ids);
$randomCart = reset($carts);

if ($randomCart)
{
    $randomCartData = reset($randomCart);
}

$randomProducators = $user_service->getRandomProducators();

$sRandomProducators = [];
$i = 1;
foreach ($randomProducators as $row)
{
    if ($i > 2) break;
    
    if (!empty($row['Categories']))
    {
        $sRandomProducators[] = $row;
        $i++;
    }
}

$produseleZonei = $product_stock_service->getProduseleZonei($_SESSION['UserID']);

$zoneNews = $news_service->getLastNews($ids, 5);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/head.php'; ?>



<section class="home-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <?php if ($randomCartData) { ?>
                <!-- Cos Saptamanal -->	
                <h2 class="text-left center-title margin-10"><?= Lang::t('ThisWeek') ?></h2>
                <div class="week-cart-block">
                    <div class="row">
                        <div class="col-md-2 week-cart-icon">
                            <a class="lightbox" href="<?= Url::link('uploads/cos/' . $randomCartData['Image']) ?>">
                                <i class="fa fa-shopping-cart"></i>
                                <span style="font-size: 12px;"><?= Lang::t('ViewPhoto') ?></span>
                            </a>
                            
                        </div>
                        <div class="col-md-5">
                            <strong><?= Lang::t('WeekCartContain') ?>:</strong>
                            <ul>
                                <?php foreach ($randomCart as $product) { ?>
                                <li><?= (float)$product['ProductQuantity'] ?> <?= Unit::getName($product['UnitID']) ?> <?= $product['ProductName'] ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-md-5 week-cart-price">
                            <span><?= (float)$randomCartData['Price'] ?> <sup>lei</sup></span>
                            <a onclick="return Cart.add(<?= $randomCartData['CartWeekID'] ?>, 1, Cart.typeCart)" data-cos-id="<?= $randomCartData['CartWeekID'] ?>" href=""><i class="fa fa-plus-circle"></i> <?= Lang::t('AddToCart') ?></a>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <!-- Lista Produse -->					
                <h2 class="text-left center-title margin-10"><?= Lang::t('ZoneProduct') ?></h2>
                <div class="profile-products">
                    <ul>
                        <?php foreach ($produseleZonei as $categoryID => $category) {  ?>
                        <li><a data-toggle="collapse" href="#cat-<?= $categoryID ?>"><i class="fa fa-plus-circle"></i> <?= $category['Name'] ?></a>
                            <ul class="collapse" id="cat-<?= $categoryID ?>">
                                <?php $res = reset($category['Products']); ?>
                                <?php //echo '<pre>'; print_r($category['Products']); echo '</pre>'; ?>
                                <?php if (count($res['Producatori']) == 1) { ?>
                                <?php $product = reset($res['Producatori']); ?>
                                <li class="product-item">
                                    <strong><?= $product['ProductName'] ?></strong>
                                    <span><?= $product['Price'] ?> lei / <?= Unit::getName($product['UnitID']) ?></span>
                                    <input type="number" name="product-quantity" value="1">
                                    <a href="#" onclick="return Cart.add(<?= $product['psID'] ?>, $(this).closest('.product-item').find('input[name=product-quantity]').val(), Cart.typeProduct)"><i class="fa fa-plus-circle"></i></a>
                                </li>
                                <?php } else { ?>
                                <?php foreach ($category['Products'] as $psID => $product) { ?>
                                <li class="more-prods">
                                    <a data-toggle="collapse" href="#cat-<?= $categoryID ?>-<?= $psID ?>"><i class="fa fa-plus-circle"></i></a>
                                    <strong><?= $product['Name'] ?></strong>
                                    <span></span>
                                    <span><i><?= count($product['Producatori']) ?> oferte</i></span>
                                    <ul class="collapse" id="cat-<?= $categoryID ?>-<?= $psID ?>">
                                        <?php foreach ($product['Producatori'] as $psID => $ps) { ?>
                                        <li class="product-item">
                                            <strong><?= $ps['ProducatorName'] ?></strong>
                                            <span><?= $ps['Price'] ?> lei / <?= Unit::getName($ps['UnitID']) ?></span>
                                            <input type="number" name="product-quantity" value="1">
                                            <a href="#" onclick="return Cart.add(<?= $psID ?>, $(this).closest('.product-item').find('input[name=product-quantity]').val(), Cart.typeProduct)"><i class="fa fa-plus-circle"></i></a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </div>		
            </div>

            <div class="col-md-6">

                <!-- Producatori -->	
                <div class="producatori">
                    <h2 class="text-left center-title margin-10"><?= Lang::t('Manufacturers') ?></h2>

                    <div class="producatori-list">
                        <?php foreach ($sRandomProducators as $producator) { ?>
                        <div class="producatori-item">
                            <div class="producatori-item-img">
                                <img src="<?= Url::link('uploads/profile/' . $producator['User']['Thumb']) ?>" alt="">
                            </div>
                            <strong><?= $producator['User']['Name'] ?> <small><?= ProductStock::getProductStockCount($producator['User']['ID']) ?> <?= Lang::t('Products') ?></small></strong>
                            <span><?= implode(', ', $producator['Categories']) ?></span>		
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= UserRating::ratingWidget($producator['User']['ID']) ?>
                                </div>
                                <div class="col-md-6">
                                    <a href="<?= Url::link($producator['User']['ID'] . '-' . Url::strToUrl( $producator['User']['Name'])) ?>"><?= Lang::t('More') ?></a>
                                </div>

                            </div>
                        </div>
                        <?php } ?>

                        <a href="<?= Url::link('client/producatori') ?>" class="arrows-btn">
                            <?= Lang::t('ViewAll') ?><br />
                            <?= Lang::t('Manufacturers') ?><br />
                            <?= Lang::t('FromZone') ?>
                        </a>
                    </div>


                </div>					


                <!-- News -->
                <h2 class="text-left center-title margin-10"><?= Lang::t('NewsFromZone') ?></h2>
                <div class="profile-news">
                    <?php if (!empty($zoneNews)) { ?>
                    <?php foreach ($zoneNews as $news) { ?>
                    <article>
                        <a href="<?= Url::link('blog/' . $news['ID'] . '-' . Url::strToUrl($news['Title'])) ?>"><?= $news['Title'] ?></a>
                        <time>Adăugat de <?= $user_info['Name'] ?> pe <?= DateHelper::toDMY($news['Date']) ?></time>
                    </article>
                    <?php } ?>
                    <?php } ?>
                    <a href="">vezi mai mult</a>
                </div>					
            </div>

        </div>



    </div>
</section>	



<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/footer.php'; ?>