<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$order_service = new Order();

$orderList = $order_service->getClientOrderList($_SESSION['UserID']);

$user_info_service = new UserInfo();

$user_info = $user_info_service->getByUserID($_SESSION['UserID']);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('YourOrders') ?></h2>
        <div class="row">
            <div class="col-md-12 cart-inn">
                <h3 class="cart-title">1. <?= Lang::t('RecurrentOrders') ?></h3>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th><?= Lang::t('Quantity') ?></th>
                            <th><?= Lang::t('UnitPrice') ?></th>
                            <th><?= Lang::t('TotalPrice') ?></th>
                            <?php if ($user_info['VATPayer']) { ?>
                            <th>TVA</th>
                            <?php } ?>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orderList['orderCarts'] as $cart) { ?>
                        <tr ocid="<?= $cart['cart']['ID'] ?>">
                            <td><?= Lang::t('WeekCartFrom') ?> <?= $cart['cart']['ProducatorName'] ?><small><br>&nbsp;&nbsp;&nbsp;&nbsp;- <?= implode('<br>&nbsp;&nbsp;&nbsp;&nbsp;- ', $cart['products']) ?></small></td>
                            <td><?= $cart['cart']['CartPrice'] ?> lei</td>
                            <td><input type="number" name="" value="1" disabled /></td>
                            <td><?= $cart['cart']['CartPrice'] ?> lei</td>
                            <?php if ($user_info['VATPayer']) { ?>
                            <td><?= $cart['cart']['CartPrice'] - ($cart['cart']['CartPrice'] * Config::$TVARate) ?> lei</td>
                            <?php } ?>
                            <td><?= $cart['cart']['CartPrice'] * Config::$TVARate ?> lei</td>
                            <td><a href="#" onclick="if (confirm('<?= Lang::t('Delete') ?>')) Cart.removeForever(<?= $cart['cart']['ID'] ?>); return false;"><i class="fa fa-trash-o"></i></a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>				

                <div class="clearfix"></div>


                <h3 class="cart-title">2. <?= Lang::t('FinishedOrders') ?></h3>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID <?= Lang::t('Order') ?></th>
                            <th><?= Lang::t('OrderDate') ?></th>
                            <th><?= Lang::t('Status') ?></th>
                            <th><?= Lang::t('TotalPrice') ?></th>
                            <?php if ($user_info['VATPayer']) { ?>
                            <th>TVA</th>
                            <?php } ?>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orderList['orders'] as $order) { ?>
                        <tr>
                            <td><?= $order['ID'] ?></td>
                            <td><?= DateHelper::toDMY($order['Date']) ?></td>
                            <td><?= $order['Delivered'] == 1 ? Lang::t('Delivered') : Lang::t('NotDelivered') ?></td>
                            <td><?= $order['Amount'] ?> lei</td>
                            <?php if ($user_info['VATPayer']) { ?>
                            <td><?= $order['Amount'] * Config::$TVARate ?> lei</td>
                            <?php } ?>
                            <td><a target="_blank" href="<?= Url::link('reports/report.php', ['report' => 'client-order', 'data' => ['orderID' => $order['ID']]]) ?>"><i class="fa fa-file-pdf-o"></i></a></td>
                            <td><a href=""><i class="fa fa-star"></i></a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>				

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</section>	


<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/footer.php'; ?>