<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h1><?= Lang::t('Manufacturers') ?> <?= Lang::t('FromZone') ?></h1>
        <div class="row">
            <div data-user-type="Gal" class="col-md-6">
                <h2 class="text-left center-title margin-10">
                    <?= Lang::t('Galls') ?>
                    <select class="sort-field pull-right">
                        <option value="">- <?= Lang::t('SortBy') ?> -</option>
                        <option value="rating"><?= Lang::t('Rating') ?></option>
                        <option value="alfabetic"><?= Lang::t('A-Z') ?></option>
                        <option value="implicit"><?= Lang::t('Default') ?></option>
                    </select>
                </h2>
                <div class="producatori">
                    <div class="producatori-list">
                    
                    </div>
                    <div class="text-center">
                        <button class="red-btn more-users"><?= Lang::t('More') ?></button>
                    </div>
                </div>
            </div>
            <div data-user-type="Producator" class="col-md-6">
                <h2 class="text-left center-title margin-10">
                    <?= Lang::t('Manufacturers') ?> 
                    <select class="sort-field pull-right">
                        <option value="">- <?= Lang::t('SortBy') ?> -</option>
                        <option value="rating"><?= Lang::t('Rating') ?></option>
                        <option value="alfabetic"><?= Lang::t('A-Z') ?></option>
                        <option value="implicit"><?= Lang::t('Default') ?></option>
                    </select>
                </h2>
                <div class="producatori-list">
                    
                </div>
                <div class="text-center">
                    <button class="red-btn more-users"><?= Lang::t('More') ?></button>
                </div>
            </div>
        </div>
    </div>
</section>	

<style>
    .producatori-list .profile-rating i 
    {
        font-size: 18px;
        margin: 0 5px 0 0;
    }
    .producatori-list {
        min-height: 400px;
    }
</style>

<script>

    var PageJS = {
        
        limit: 3,
        
        init: function(){
            $('div[data-user-type] .sort-field').change(function (){
                PageJS.changeSort(this);
            });
            $('div[data-user-type] .more-users').click(function(){
                var type = $(this).closest('div[data-user-type]').attr('data-user-type');
                var sort = $(this).closest('div[data-user-type]').find('.sort-field').val();
                PageJS.getUsers(type, sort);
            });
            PageJS.getUsers('Gal', '');
            PageJS.getUsers('Producator', '');
        },
        
        changeSort: function(control)
        {
            var sort = $(control).val();
            var type = $(control).closest('div[data-user-type]').attr('data-user-type');
            $(control).closest('div[data-user-type]').find('.producatori-list').empty();
            PageJS.getUsers(type, sort);
        },
        
        getUsers: function(type, sort)
        {
            $.post('<?= Url::link('ajax/users/list.php') ?>', {type: type, limit: PageJS.limit, offset: $('div[data-user-type=' + type + '] .producatori-item').length, sort: sort}, function(html){
                $('div[data-user-type=' + type + '] .producatori-list').append(html);
                $('div[data-user-type=' + type + '] .producatori-list .producatori-item:hidden').fadeIn(200);
            });
        }
        
    };
    
    PageJS.init();

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/client/template/footer.php'; ?>