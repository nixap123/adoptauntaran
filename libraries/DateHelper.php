<?php

class DateHelper
{
    
    public static function toDMY($date)
    {
        return date('d.m.Y', strtotime($date));
    }
    
    public static function toDMYHI($date)
    {
        return date('d.m.Y H:i', strtotime($date));
    }
    
}