<?php

class Request
{
    
    public static $Language = 1;
    
    
    public static function parse()
    {
        $uri = trim($_SERVER['REQUEST_URI'], '/');
        
        $segments = explode('/', $uri);
        
        if (empty($segments[0]))
        {
            $_SESSION['Language'] = 1;
            static::$Language = 1;
        }
        else
        {
            if ($segments[0] == 'en')
            {
                $_SESSION['Language'] = 2;
                static::$Language = 2;
            }
            else
            {
                $_SESSION['Language'] = 1;
                static::$Language = 1;
            }
        }
    }

    public static function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && count($_POST) > 0)
        {
            return true;
        }
        
        return false;
    }

    public static function post($name = false)
    {
        if ($name)
        {
            return isset($_POST[$name]) ? $_POST[$name] : '';
        }
        
        return $_POST;
    }
    
    public static function dataPost($data, $data_key, $post_key)
    {
        if (static::isPost())
        {
            return static::post($post_key);
        }
        else
        {
            return isset($data[$data_key]) ? $data[$data_key] : '';
        }
    }
    
    public function isGet()
    {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }
    
    public static function getLangID()
    {
        return isset($_SESSION['Language']) ? $_SESSION['Language'] : 1;
    }
    
}