<?php

class Email
{
    
    public static function send($to, $subject, $message)
    {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        
        mail($to, $subject, $message, $headers);
    }
    
}