<?php

class Lang
{
    
    public static function t($name, $data = [])
    {
        $langID = Request::getLangID();
        $LANG = require $_SERVER['DOCUMENT_ROOT'] . '/languages/' . ($langID == 1 ? 'ro' : 'en') . '.php';
        
        if (isset($LANG[$name]))
        {
            $ret = $LANG[$name];
        }
        else
        {
            $ret = preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $name);
        }
        
        $_SESSION['t'][$name] = $ret;
        
        if (count($data) > 0)
        {
            foreach ($data as $key => $val)
            {
                $ret = str_replace('{' . $key . '}', $val, $ret);
            }
        }
        
        return $ret;
    }
    
}