<?php

class Url
{
    
    public static $IsOriginalDomain = true;
    
    public static $UrlDomain = false;
    
    
    public static function detectDomain()
    {
        if (stripos(Config::$BaseUrl, $_SERVER['HTTP_HOST']) == false)
        {
            static::$IsOriginalDomain = false;
            static::$UrlDomain = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        }
        else
        {
            static::$IsOriginalDomain = true;
            static::$UrlDomain = Config::$BaseUrl;
        }
    }

    private static function base_url()
    {
        return rtrim(static::$UrlDomain, '/') . (Request::$Language == 2 ? '/en' : '') . '/';
    }

    public static function link($url = '', $getParams = [])
    {
        $query_string = '';
        
        if (count($getParams) > 0)
        {
            $arr = [];
            foreach ($getParams as $key => $val)
            {
                if (is_array($val))
                {
                    foreach ($val as $k => $v)
                    {
                        $arr[] = $key . '[' . $k . ']=' . $v;
                    }
                }
                else
                {
                    $arr[] = $key . '=' . $val;
                }
            }

            $query_string = '?' . implode('&', $arr);
        }
        
        return static::base_url() . ltrim(str_replace('.php', '', $url), '/') . $query_string;
    }
    
    public static function redirect($url = '', $getParams = [])
    {
        header('Location: ' . static::link($url, $getParams));
        exit;
    }
    
    public static function strToUrl($str, $replace=array(), $delimiter='-')
    {
        if( !empty($replace) )
        {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
    
    public static function switchLang($langSlug)
    {
        $uri = $_SERVER['REQUEST_URI'];
        $clean_uri = str_replace(['/en/', '/ro/'], '/', $uri);
        
        if ($langSlug == 'en')
        {
            return '/en' . $clean_uri;
        }
        else
        {
            return $clean_uri;
        }
    }
    
}