<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Donate.php';

$donate_service = new Donate();

if (isset($_GET['all']))
{
    $status = [Donate::StatusPaid, Donate::StatusPending];
}
else
{
    $status = Donate::StatusPaid;
}

$donatori = $donate_service->getWhere([
    'Status' => $status
]);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Donations') ?></h2>
        <div>
            <div>
                <a href="?all" class="red-btn"><?= Lang::t('All') ?></a>
                <a href="?" class="red-btn"><?= Lang::t('Paid') ?></a>
            </div>
            <hr />
            <table class="table table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th><?= Lang::t('Name') ?></th>
                        <th class="hidden-xs hidden-sm"><?= Lang::t('Address') ?></th>
                        <th><?= Lang::t('Phone') ?></th>
                        <th class="hidden-xs hidden-sm"><?= Lang::t('Email') ?></th>
                        <th><?= Lang::t('Amount') ?></th>
                        <th class="hidden-xs hidden-sm"><?= Lang::t('PaymentMethod') ?></th>
                        <th><?= Lang::t('Status') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($donatori as $donator) { ?>
                    <tr donator-id="<?= $donator['ID'] ?>">
                        <td><?= $donator['Name'] ?></td>
                        <td class="hidden-xs hidden-sm"><?= $donator['Address'] ?></td>
                        <td><?= $donator['Phone'] ?></td>
                        <td class="hidden-xs hidden-sm"><?= $donator['Email'] ?></td>
                        <td><?= $donator['Amount'] ?></td>
                        <td class="hidden-xs hidden-sm"><?= $donator['PaymentMethod'] ?></td>
                        <td><?= Donate::statusSelect($donator['Status']) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>	
<script>

    $(document).ready(function(){
        
        $('tr[donator-id] select[name=DonateStatus]').change(function(){
            var staus = $(this).val();
            var donateID = $(this).closest('tr').attr('donator-id');
            $.post('<?= Url::link('ajax/donate/status.php') ?>', {status: staus, donateID: donateID}, function(){
                notification('<?= Lang::t('StatusChanged') ?>', 'success');
            });
        });
        
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>