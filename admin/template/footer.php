


<section class="before-foot">

    <div class="clearfix"></div>

    <p>&nbsp;</p>

    <div class="nat-patt"></div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>

    <div class="container">
        <div class="row">
            <div class="newsletter-block col-md-6">
                <h2 class="center-title">Newsletter</h2>

                <p>
                    <!--Pentru a vă abona la newsletter, <br />
                    vă rugăm introduceți adresa de e-mail: -->
                    <?= Lang::t('FooterNewsletter') ?>
                </p>

                <form onsubmit="return subscribe(this);">
                    <input required class="col-md-7" type="email" name="Email" placeholder="nume@domen.ro" />
                    <button type="submit" class="col-md-4"><?= Lang::t('Add') ?></button>
                </form>

            </div>

            <div class="bottom-menu col-md-6">
                <h2 class="center-title text-left"><?= Lang::t('AboutProject') ?></h2>
                <ul>
                    <li><a href="<?= Url::link('about') ?>"><?= Lang::t('ProjectDescription') ?></a></li>
                    <li><a href="<?= Url::link('initiatori') ?>"><?= Lang::t('AboutInitiators') ?></a></li>
                    <li><a href="<?= Url::link('contact') ?>"><?= Lang::t('Contact') ?></a></li>	
                </ul>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <span>&copy; 2015 - 2016 <?= Lang::t('FooterCopyright') ?></span>
            </div>
            <div class="footer-socials col-md-6 col-sm-6 col-xs-12">
                <span>
                    <?= Lang::t('SocialNetwork') ?>
                    <a href="" target="_blank"><i class="fa fa-facebook-square"></i></a>
                    <a href="" target="_blank"><i class="fa fa-twitter-square"></i></a>
                    <a href="" target="_blank"><i class="fa fa-google-plus-square"></i></a>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href=""><?= Lang::t('PrivacyPolicy') ?></a>
            </div>
            <div class="col-md-6 text-right col-sm-6 col-xs-12">
                <small><?= Lang::t('CreatedBy') ?></small> <a href="" target="_blank"><img src="<?= Url::link('assets/images/zl.jpg') ?>"></a>
            </div>
        </div>
    </div>
</footer>
<script>

    function subscribe(form)
    {
        var email = $(form).find('input[name=Email]').val();
        
        $.post('<?= Url::link('ajax/subscribe.php') ?>', {email: email}, function(json){
            if (json.success)
            {
                notification(json.messages, 'success', 5000);
                $(form).trigger('reset');
            }
            else
            {
                notification(json.messages, 'warning', 5000);
            }
        }, 'json');
        
        return false;
    }

</script>

<script>
	$('.datepicker').datepicker({
		'format': 'dd.mm.yyyy'
	});
	
	$('.dataTable').dataTable({
		"aLengthMenu": [ [10, 20, 50, -1], [10, 20, 50, "Toate"] ],
		"iDisplayLength": 10,
                "bSort": false
	});
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('#acc-photo').click(function(){
            $('#inputfile-profile').click();
        });
        
        $('#inputfile-profile').change(function(event){
                        
            var files = event.target.files;
                
            var that = $(this);

            var data = new FormData();

            $.each(files, function(key, value)
            {
                data.append(key, value);
            });

            $.ajax({
                url: '<?= Url::link('ajax/user-logo-upload.php') ?>',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    $('.acc-photo img').attr('src', '<?= Url::link('uploads/profile') ?>' + '/' + data.thumb);
                    $('#inputfile-profile').val('');
                }
            });
        });
        
        $('a.lightbox').fancybox();
        
    });
</script>
</body>
</html>