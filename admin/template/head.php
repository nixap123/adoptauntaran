<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php'; 
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Notification.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Product.php';

$user_service = new User();
$user_info_service = new UserInfo();
$image_service = new Image();
$product_service = new Product();
$notification_service = new Notification();

$USER = $user_service->getByID($_SESSION['UserID']);

$USER_INFO = $user_info_service->getByUserID($USER['ID']);

$user_logo = $image_service->getByID($USER_INFO['Photo']);

$unread_notifications_count = $notification_service->getCountUnreaded($_SESSION['UserID']);

$res = $user_service->query("select ID, `Type` from `User`");

$gal_count = 0;
$producatori_count = 0;
$client_count = 0;
foreach ($res as $row)
{
    switch ($row['Type'])
    {
        case User::TypeGal:
            $gal_count++;
            break;
        case User::TypeProducator:
            $producatori_count++;
            break;
        case User::TypeClient:
            $client_count++;
            break;
    }
}

$res = $product_service->query("select count(*) as count from `Product`");
$res = reset($res);
$prod_count = $res['count'];

?>


<!DOCTYPE html>
<html>
    <head>
        <title>ADMIN: Adopta un taran</title>
        <meta charset="UTF-8" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/styles.css') ?>" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap-theme.min.css') ?>">

        <link rel="stylesheet" href="<?= Url::link('assets/css/select2.min.css') ?>">
        
        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?= Url::link('assets/js/fancybox/jquery.fancybox.css') ?>">

        <script src="<?= Url::link('assets/js/jquery-2.1.4.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/jquery.dataTables.min.js') ?>"></script>

        <script src="<?= Url::link('assets/js/bootstrap.min.js') ?>"></script>
        
        <script src="<?= Url::link('assets/js/main.js') ?>"></script>

        <link rel="stylesheet" type="text/css" href="<?= Url::link('assets/css/imgareaselect-default.css') ?>" />
        <script type="text/javascript" src="<?= Url::link('assets/js/jquery.imgareaselect.pack.js') ?>"></script>

        <!--[if lte IE 8]>
                <script src="<?= Url::link('assets/js/html5shiv.min.js') ?>"></script>
                <script src="<?= Url::link('assets/js/respond.min.js') ?>"></script>
        <![endif]-->

        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap-datepicker3.min.css') ?>" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/dataTables.min.css') ?>" />

        <script src="<?= Url::link('assets/js/jquery.nestable.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/bootstrap-datepicker.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/select2.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script type="text/javascript" src="<?= Url::link('assets/js/jquery.noty.packaged.min.js') ?>"></script>
        
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
        <script>
            $(document).ready(function() {
                $('.summer-note').summernote({
                 height: 300,        
             });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('.summer-note-mini').summernote({
                 height: 150      
             });
            });
        </script>
        <script>
        function notification(text, type, timeout) {
            var n = noty({
                text        : text,
                type        : type,
                dismissQueue: true,
                timeout     : timeout == undefined ? 3000 : timeout,
                closeWith   : ['click'],
                layout      : 'topRight',
                theme       : 'defaultTheme',
                maxVisible  : 10
            });
        }
        </script>
    </head>
    <body class="admin-panel">

        <script>
            $(document).ready(function() {
                $(".open-menu").click(function() {
                    $(".user-bar").toggle("slow");
                });
                
                var udali = document.getElementById("userbar").innerHTML;
                var rezz = udali.replace(/\|/g, "");
                document.getElementById("userbar").innerHTML = rezz;
            });
        </script>
        <div class="top-bar ">
            <div class="container">
                <div class="row">
                    <div class="text-left donate-bar col-md-3">
                        ADMIN: Adoptă un țăran!
                        <button class="hidden-lg hidden-md pull-right open-menu btn btn-xs btn-danger">Menu</button>
                    </div>
                    
                    <div id="userbar" class="text-right user-bar col-md-9">
                        <a href="<?= Url::switchLang('ro') ?>"><img src="/assets/images/ro.png" /></a>
                        <a href="<?= Url::switchLang('en') ?>"><img src="/assets/images/en.png" /></a>&nbsp;&nbsp;&nbsp;
                        <a class="notification-menu" href="<?= Url::link('admin/subscribe') ?>"><?= Lang::t('Subscribers') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('admin/donatori') ?>"><?= Lang::t('Donations') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('admin/feedback') ?>"><?= Lang::t('Feedback') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('admin/notification') ?>"><span class="unread-notifications"><?= $unread_notifications_count ?></span> <?= Lang::t('Notifications') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('admin/setari/index') ?>"><?= Lang::t('Settings') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('admin/news/index') ?>"><?= Lang::t('News') ?></a> | 
                        <a class="notification-menu" href="<?= Url::link('admin/pages/index') ?>"><?= Lang::t('Pages') ?></a> | 
                        <a href="<?= Url::link('logout.php') ?>"><?= Lang::t('Logout') ?></a>
                    </div>
                </div>
            </div>
        </div>

        <header class="header acc">
            <div class="container">
                <div class="row">
                    <div class="logo-bar col-md-2">
                        <a href="<?= Url::link('admin/index') ?>" title=""><img src="<?= Url::link('assets/images/logo.png') ?>" alt=""></a>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('admin/produse/index') ?>">
                                        <i class="hidden-xs fa fa-shopping-basket"></i>
                                        <strong> <?= $prod_count ?></strong>
                                        <span><?= Lang::t('Products') ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('admin/producatori/index') ?>">
                                        <i class="hidden-xs fa fa-users"></i>
                                        <strong><?= $gal_count ?> / <?= $producatori_count ?></strong>
                                        <span><?= Lang::t('GalManufacturers') ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('admin/users/index') ?>">
                                        <i class="hidden-xs fa fa-users"></i>
                                        <strong><?= $client_count ?></strong>
                                        <span><?= Lang::t('Clients') ?> (total)</span>
                                    </a>
                                </div>
                            </div>
                            <div class="user-bar col-md-3 hidden-sm hidden-xs">
                                <div class="acc-inn">
                                    <a id="acc-photo" class="acc-photo">
                                        <?php if (!empty($user_logo['ID'])) { ?>
                                        <img src="<?= Url::link('uploads/profile/' . $user_logo['Thumb']) ?>" />
                                        <?php } else { ?>
                                        <img src="<?= Url::link('assets/images/no_profile_img.jpg') ?>" />
                                        <?php } ?>
                                    </a>
                                    <input class="hidden" id="inputfile-profile" type="file" name="" />
                                    <div class="clearfix"></div>
                                    <div class="dropdown">
                                        <p id="open-profile" data-toggle="dropdown" role="button">
                                            <span>Bine ați venit, <?= $USER_INFO['Name'] ?>!</span>
                                            <i class="fa fa-caret-down"></i>
                                        </p>
                                        <ul class="pull-right dropdown-menu" aria-labelledby="open-profile">
                                            <li><a href="<?= Url::link('admin/setari/index.php') ?>"><i class="fa fa-cogs"></i> <?= Lang::t('Settings') ?></a></li>
                                            <li><a href="<?= Url::link('logout.php') ?>"><i class="fa fa-sign-out"></i> <?= Lang::t('Logout') ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="nat-patt"></div>
        </header>
