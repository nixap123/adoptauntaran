<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';

$category_service = new Category();

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('ProductsCategories') ?></h2>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="panel panel-default">
                                <div class="panel-heading"><?= Lang::t('EditCategory') ?></div>
                                <div class="panel-body">
                                    <div>
                                        <a data-toggle="modal" data-target=".bs-example-modal-sm" data-category-id="0" class="red-btn edit-category"><i class="fa fa-plus"></i> <?= Lang::t('AddCategory') ?></a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <hr />
                                    <div class="dd dd-draghandle">
                                        <?= $category_service->getTree() ?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div id="product-table">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
    </div>
</section><!-- /.main-content -->

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Adauga categoria
            </div>
            <div class="modal-body">
                <form id="category-form">
                    <?php foreach (Config::$LangList as $langID => $lang) { ?>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Label') ?> (<?= $lang['name'] ?>)</label>
                        <input type="text" name="Name[<?= $langID ?>]" class="form-control" />
                    </div>
                    <?php } ?>
                    <input type="hidden" name="CategoryID" value="0" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Lang::t('Close') ?></button>
                <button id="save-category" type="button" class="btn btn-primary"><?= Lang::t('Save') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade add-product-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= Lang::t('AddProduct') ?>
            </div>
            <div class="modal-body">
                <form id="product-form">
                    <?php foreach (Config::$LangList as $langID => $lang) { ?>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Label') ?> (<?= $lang['name'] ?>)</label>
                        <input type="text" name="Name[<?= $langID ?>]" class="form-control" />
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Category') ?></label>
                        <?= Category::categorySelect() ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Unit') ?></label>
                        <?= Unit::unitSelect() ?>
                    </div>
                    <input type="hidden" name="ProductID" value="0" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Lang::t('Close') ?></button>
                <button id="save-product" type="button" class="btn btn-primary"><?= Lang::t('Save') ?></button>
            </div>
        </div>
    </div>
</div>

<script>

    var PageJS = {
        
        showCategoryList: function(){
            $.post('<?= Url::link('ajax/category/list.php') ?>', {}, function(html){
                $('.dd').html(html);
                $('.dd').nestable();
			
                $('.dd-handle a').on('mousedown', function(e){
                    e.stopPropagation();
                });

                $('[data-rel="tooltip"]').tooltip();

                });
        },
        
        categorySave: function()
        {
            $('#category-form .has-error').removeClass('has-error');
            var valid = true;
            $('#category-form input[name^=Name]').each(function(){
                if ($.trim($(this).val()) == "")
                {
                    $(this).closest('.form-group').addClass('has-error');
                    valid = false;
                }
            });
            
            if (valid)
            {
                $.post('<?= Url::link('ajax/category/edit.php') ?>', $('#category-form').serialize(), function(json)
                {
                    $('#category-form').prepend(json.message);
                    $('#category-form input[name=CategoryID]').val(json.categoryID);
                    setTimeout(function(){
                        $('#category-form .alert').fadeIn(1000, function(){
                            $(this).remove();
                        });
                    }, 2000);
                    PageJS.showCategoryList();
                }, 'json');
            }
        },
            
        productSave: function()
        {
            $('#product-form .has-error').removeClass('has-error');
            var valid = true;
            $('#product-form input[name^=Name]').each(function(){
                if ($.trim($(this).val()) == "")
                {
                    $(this).closest('.form-group').addClass('has-error');
                    valid = false;
                }
            });
            
            if (valid)
            {
                $.post('<?= Url::link('ajax/product/edit.php') ?>', $('#product-form').serialize(), function(json)
                {
                    $('#product-form').prepend(json.message);
                    $('#product-form select[name=CategoryID]').val(json.categoryID);
                    $('#product-form input[name=ProductID]').val(json.productID);
                    setTimeout(function(){
                        $('#product-form .alert').fadeIn(1000, function(){
                            $(this).remove();
                        });
                    }, 2000);
                    PageJS.showProductList(0);
                }, 'json');
            }
        },
            
        showProductList: function(categoryID)
        {
            $.post('<?= Url::link('ajax/product/list.php') ?>', {"CategoryID": categoryID}, function(html){
                $('#product-table').html(html).find('.dataTable').dataTable({
                    "aLengthMenu": [ [10, 20, 50, -1], [10, 20, 50, "Toate"] ],
                    "iDisplayLength": 10
                });
            });
        }
        
    };

	$(document).ready(function(){
        
            PageJS.showCategoryList();
            PageJS.showProductList(0);
            
            $('body').on('click', '.edit-category', function(){
                var categoryID = $(this).attr('data-category-id');

                if (categoryID == 0)
                {
                    $('#category-form').trigger('reset');
                }

                $.post('<?= Url::link('ajax/category/get.php') ?>', {CategoryID: categoryID}, function(html){
                    $('#category-form').html(html);
                });
            });
            
            $('body').on('click', '.add-product', function(){
                var productID = $(this).attr('data-product-id');
                var categoryID = $(this).attr('data-category-id');
                
                $.post('<?= Url::link('ajax/product/get.php') ?>', {CategoryID: categoryID, ProductID: productID}, function(html){
                   $('#product-form').html(html);
               });
            });
            
            $('body').on('click', '.edit-category', function(){
                var categoryID = $(this).attr('data-category-id');
                $('#category-form input[name=CategoryID]').val(categoryID);
            });
            
            $('body').on('click', '.delete-category', function(){
                var categoryID = $(this).attr('data-category-id');
                
                if (confirm('Confirm?'))
                {
                    $.post('<?= Url::link('ajax/category/delete.php') ?>', {"CategoryID": categoryID}, function(){
                        PageJS.showCategoryList();
                    });
                }
            });
            
            $('body').on('click', '.delete-product', function(){
                var productID = $(this).attr('data-product-id');
                
                if (confirm('Confirm?'))
                {
                    $.post('<?= Url::link('ajax/product/delete.php') ?>', {"ProductID": productID}, function(){
                        PageJS.showProductList();
                    });
                }
            });
            
            $('#save-category').click(function(){
                PageJS.categorySave();
            });
            
            $('#save-product').click(function(){
                PageJS.productSave();
            });
            
            

            $('#serialize').click(function(){
                serialized = $('ol.sortable').nestedSortable('serialize');
                $('#serializeOutput').text(serialized+'\n\n');
            });

            $('#toHierarchy').click(function(e){
                hiered = $('ol').nestedSortable('toHierarchy', {startDepthCount: 0});
                hiered = dump(hiered);
                (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
                $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
            });

            $('#toArray').click(function(e){
                arraied = $('ol').nestedSortable('toArray', {startDepthCount: 0});
                arraied = dump(arraied);
                (typeof($('#toArrayOutput')[0].textContent) != 'undefined') ?
                $('#toArrayOutput')[0].textContent = arraied : $('#toArrayOutput')[0].innerText = arraied;
            });

	});

	function dump(arr,level)
        {
            var dumped_text = "";
            if(!level) level = 0;

            //The padding given at the beginning of the line.
            var level_padding = "";
            for(var j=0;j<level+1;j++) level_padding += "    ";

            if(typeof(arr) == 'object') { //Array/Hashes/Objects
                    for(var item in arr) {
                            var value = arr[item];

                            if(typeof(value) == 'object') { //If it is an array,
                                    dumped_text += level_padding + "'" + item + "' ...\n";
                                    dumped_text += dump(value,level+1);
                            } else {
                                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                            }
                    }
            } else { //Strings/Chars/Numbers etc.
                    dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
            }
            return dumped_text;
	}



</script>


<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>