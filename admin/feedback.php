<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Feedback.php';

$feedback_service = new Feedback();

$feedbacks = $feedback_service->getUserFeedback();

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Feedback') ?></h2>
        <div>
            <table class="table table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th><?= Lang::t('Receiver') ?></th>
                        <th><?= Lang::t('Sender') ?></th>
                        <th><?= Lang::t('Phone') ?></th>
                        <th><?= Lang::t('Email') ?></th>
                        <th><?= Lang::t('Message') ?></th>
                        <th><?= Lang::t('Date') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($feedbacks as $feedback) { ?>
                    <tr>
                        <td><?= $feedback['Receiver'] ?></td>
                        <td><?= $feedback['Sender'] ?></td>
                        <td><?= $feedback['Phone'] ?></td>
                        <td><?= $feedback['Email'] ?></td>
                        <td class="big-popover"><a class="btn btn-link btn-md" data-toggle="popover" title="<?= Lang::t('Message') ?>" data-placement="top" data-content="<?= htmlentities($feedback['Message']) ?>"><?= Lang::t('ViewMessage') ?></a></td>
                        <td><?= DateHelper::toDMYHI($feedback['Date']) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>	
<script>

    $(document).ready(function(){
        
        $('[data-toggle="popover"]').popover();
        
        $(document).on('click', function (e) {
            $('[data-toggle="popover"],[data-original-title]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                    (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false
                }
            });
        });
        
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>