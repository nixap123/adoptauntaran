<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

$user_sercice = new User();

$producatori = $user_sercice->getAllClients();

if (isset($_GET['deleteID']))
{
    $user_sercice->deleteUserByID((int)$_GET['deleteID']);
    Url::redirect('admin/users/index.php');
}

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Clients') ?></h2>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <table class="table table-striped table-responsive dataTable">
                        <thead>
                            <tr>
                                <th><?= Lang::t('Name') ?></th>
                                <th class="hidden-xs hidden-sm"><?= Lang::t('Type') ?></th>
                                <th class="hidden-xs hidden-sm"><?= Lang::t('County') ?></th>
                                <th class="hidden"><?= Lang::t('Location') ?></th>
                                <th class="hidden-xs hidden-sm"><?= Lang::t('Phone') ?></th>
                                <th class="hidden-xs hidden-sm"><?= Lang::t('Products') ?></th>
                                <th class="hidden-xs hidden-sm"><?= Lang::t('Rating') ?></th>
                                <th class="hidden-xs hidden-sm"><?= Lang::t('Comments') ?></th>
                                <th><?= Lang::t('Edit') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($producatori as $producator) { ?>
                            <tr>
                                <td><a href="" target="_blank"><?= $producator['Name'] ?></a></td>
                                <td class="hidden-xs hidden-sm"><?= $producator['Type'] ?></td>
                                <td class="hidden-xs hidden-sm"><?= $producator['JudetName'] ?></td>
                                <td class="hidden"><?= $producator['CityName'] ?></td>
                                <td class="hidden-xs hidden-sm"><?= $producator['Phone'] ?></td>
                                <td class="hidden-xs hidden-sm"><?= $producator['Products'] ?></td>
                                <td class="hidden-xs hidden-sm"><?= $producator['Rating'] ?></td>
                                <td class="hidden-xs hidden-sm"><?= $producator['Comments'] ?></td>
                                <td>
                                    <a href="<?= Url::link('admin/producatori/edit.php', ['id' => $producator['ID']]) ?>" class="btn btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a onclick="return confirm('Confirm action?')" href="<?= Url::link('admin/producatori/index.php', ['deleteID' => $producator['ID']]) ?>" class="btn btn-xs"><i class="fa fa-trash"></i></a>
                                    <?php if ($producator['Status'] == User::StatusActive) { ?>
                                    <a href="#" class="btn btn-xs"><i uid="<?= $producator['ID'] ?>" title="<?= Lang::t('Deactivate') ?>" class="fa fa-pause-circle-o disable-user" aria-hidden="true"></i></a>
                                    <?php } else { ?>
                                    <a href="#" class="btn btn-xs"><i uid="<?= $producator['ID'] ?>" title="<?= Lang::t('Activate') ?>" class="fa fa-play-circle-o enable-user" aria-hidden="true"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>	
                        </tbody>
                    </table>



                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </div>
</section><!-- /.main-content -->

<script>
    
    $(document).ready(function(){
        
        $('body').on('click', '.disable-user', function(e){
            e.preventDefault();
            var control = $(this);
            var userID = parseInt(control.attr('uid'));
            $.post('<?= Url::link('ajax/user/change-user-status.php') ?>', {userID: userID, status: 'Inactive'}, function(){
                control.removeClass('fa-pause-circle-o').removeClass('disable-user').addClass('fa-play-circle-o').addClass('enable-user').attr('title', 'Activate user account');
            });
        });
        
        $('body').on('click', '.enable-user', function(e){
            e.preventDefault();
            var control = $(this);
            var userID = parseInt(control.attr('uid'));
            $.post('<?= Url::link('ajax/user/change-user-status.php') ?>', {userID: userID, status: 'Active'}, function(){
                control.removeClass('fa-play-circle-o').removeClass('enable-user').addClass('fa-pause-circle-o').addClass('disable-user').attr('title', 'Inactivate user account');
            });
        });
        
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>