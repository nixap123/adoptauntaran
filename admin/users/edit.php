<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Bank.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Judet.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';

    $user_service = new User();
    $user_info_service = new UserInfo();

    $userID = (int)$_GET['id'];

    $user = $user_service->getByID($userID);
    $user_info = $user_info_service->getByUserID($user['ID']);
    
    if ($userID > 0 && empty($user['ID']))
    {
        if (isset($_GET['galID']))
        {
            Url::redirect('admin/producatori/edit.php', ['galID' => $_GET['galID']]);
        }
        else
        {
            Url::redirect('admin/producatori/edit.php');
        }
    }
    
    if (isset($_GET['galID']))
    {
        $user['ParentID'] = (int)$_GET['galID'];
    }
    
    if (Request::isPost())
    {
        $UserName = Validator::validate('Email',  Validator::ValidateEmail, 'Email');
        $Password = Validator::validate('Password', Validator::ValidateEmpty, 'Password');
        $ClientType = Validator::validate('ClientType', Validator::ValidateEmpty, 'TypeClient');
        $AccountType = Validator::validate('AccountType', Validator::ValidateEmpty, 'InteresedIn');
        $ParentID = $AccountType == User::TypeProducator ? Request::post('ParentID') : 0;
        $Judet = Validator::validate('Judet', Validator::ValidateEmpty, Lang::t('County'));
        $Location = Validator::validate('Location', Validator::ValidateEmpty, Lang::t('Location'));
        $RegDate = Request::post('RegDate');
        
        $Name = Validator::validate('Name', Validator::ValidateEmpty, 'CompanyNameOrName');
        $Address = Validator::validate('Address', Validator::ValidateEmpty, 'Address');
        $Phone = Request::post('Phone');
        $ContactPerson = Request::post('ContactPerson');
        $Coverage = Request::post('Coverage');
        $Description = Request::post('Description');
        $Cnp = Request::post('Cnp');
        $Cui = Request::post('Cui');
        $BankAccount = Request::post('BankAccount');
        $BankID = Request::post('BankID');
        $RegNumber = Request::post('RegNumber');
        
        if (!Validator::hasErrors())
        {
            $ex_user = $user_service->getWhere(['UserName' => $UserName, 'Status' => User::StatusActive], 1);

            if (isset($ex_user[0]['ID']) && !($userID > 0))
            {
                Validator::setError(Lang::t('EmailIsAllreadyRegistered'));
            }
            
            if (!Validator::hasErrors())
            {
                $userData = [
                    'ParentID' => $ParentID,
                    'UserName' => $UserName,
                    'Password' => $Password,
                    'Type' => $AccountType,
                    'ClientType' => $ClientType,
                    'Status' => User::StatusActive,
                    'RegDate' => date('c', strtotime($RegDate))
                ];

                if ($userID > 0)
                {
                    $user_service->update($userData, ['ID' => $userID]);
                }
                else
                {
                    $insertID = $user_service->insert($userData);
                }

                $userInfoData = [
                    'UserID' => $userID > 0 ? $userID : $insertID,
                    'Name' => $Name,
                    'Phone' => $Phone,
                    'Address' => $Address,
                    'JudetID' => $Judet,
                    'LocationID' => $Location,
                    'CNP' => $Cnp,
                    'CUI' => $Cui,
                    'RegNumber' => $RegNumber,
                    'BankAccount' => $BankAccount,
                    'BankID' => $BankID,
                    'Description' => $Description,
                    'Coverage' => $Coverage > 0 ? $Coverage : 50,
                    'ContactPerson' => $ContactPerson
                ];

                if ($userID > 0)
                {
                    $user_info_service->update($userInfoData, ['UserID' => $userID]);
                }
                else
                {
                    $user_info_service->insert($userInfoData);
                }

                if ($userID > 0)
                {
                    Validator::setSuccess();
                }
                else
                {
                    Url::redirect('admin/producatori/edit.php', ['id' => $insertID]);
                }
            }
        }
    }

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= $userID > 0 ? 'Editeaza' : 'Adauga' ?> <?= Lang::t('GalManufacturer') ?></h2>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <?= Validator::showMessages() ?>
                        <form method="post" class="col-md-12 form-horizontal">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('TypeClient') ?></label>
                                        <div class="col-md-7">
                                            <div class="radio">
                                                <label>
                                                    <input name="AccountType" <?= Request::dataPost($user, 'Type', 'AccountType') == User::TypeProducator ? 'checked' : '' ?> value="Producator" type="radio" class="ace">
                                                    <span class="lbl"> <?= Lang::t('Manufacturer') ?></span>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input name="AccountType" <?= Request::dataPost($user, 'Type', 'AccountType') == User::TypeGal ? 'checked' : '' ?> value="Gal" type="radio" class="ace">
                                                    <span class="lbl"> <?= Lang::t('Gal') ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Person') ?></label>
                                        <div class="col-md-7">
                                            <div class="radio">
                                                <label>
                                                    <input name="ClientType" <?= Request::dataPost($user, 'ClientType', 'ClientType') == User::ClientTypePF ? 'checked' : '' ?> value="PF" type="radio" class="ace">
                                                    <span class="lbl"> <?= Lang::t('Individual') ?></span>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input name="ClientType" <?= Request::dataPost($user, 'ClientType', 'ClientType') == User::ClientTypePJ ? 'checked' : '' ?> value="PJ" type="radio" class="ace">
                                                    <span class="lbl"> <?= Lang::t('Legal') ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Gal') ?></label>
                                        <div class="col-md-7">
                                            <?= User::galSelect(Request::dataPost($user_info, 'ParentID', 'ParentID')) ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('CompanyNameOrName') ?></label>
                                        <div class="col-md-7">
                                            <input type="text" name="Name" value="<?= Request::dataPost($user_info, 'Name', 'Name') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Email') ?></label>
                                        <div class="col-md-7">
                                            <input type="email" name="Email" value="<?= Request::dataPost($user, 'UserName', 'Email') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Phone') ?></label>
                                        <div class="col-md-7">
                                            <input type="text" name="Phone" value="<?= Request::dataPost($user_info, 'Phone', 'Phone') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('CompanyRegistrationNumber') ?> *</label>
                                        <div class="col-md-7">
                                            <input type="text" name="RegNumber" value="<?= Request::dataPost($user_info, 'RegNumber', 'RegNumber') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">CUI / CIF</label>
                                        <div class="col-md-7">
                                            <input type="text" name="Cui" value="<?= Request::dataPost($user_info, 'CUI', 'Cui') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">CNP</label>
                                        <div class="col-md-7">
                                            <input type="text" name="Cnp" value="<?= Request::dataPost($user_info, 'CNP', 'Cnp') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('BankAccount') ?> *</label>
                                        <div class="col-md-7">
                                            <input type="text" name="BankAccount" value="<?= Request::dataPost($user_info, 'BankAccount', 'BankAccount') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Bank') ?></label>
                                        <div class="col-md-7">
                                            <?= Bank::bankSelect(Request::dataPost($user_info, 'BankID', 'BankID')) ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Description') ?></label>
                                        <div class="col-md-7">
                                            <textarea class="form-control" name="Description"><?= Request::dataPost($user_info, 'Description', 'Description') ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">* <?= Lang::t('OptionalFields') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Address') ?></label>
                                        <div class="col-md-7">
                                            <input type="text" name="Address" value="<?= Request::dataPost($user_info, 'Address', 'Address') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('County') ?></label>
                                        <div class="col-md-7">
                                            <?= Judet::judetSelect(Request::dataPost($user_info, 'JudetID', 'Judet')) ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Location') ?></label>
                                        <div class="col-md-7">
                                            <?= City::citySelect(Request::dataPost($user_info, 'JudetID', 'Judet'), Request::dataPost($user_info, 'LocationID', 'Location')) ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('ContactPerson') ?></label>
                                        <div class="col-md-7">
                                            <input name="ContactPerson" type="text" value="<?= Request::dataPost($user_info, 'ContactPerson', 'ContactPerson') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Password') ?></label>
                                        <div class="col-md-7">
                                            <input name="Password" type="text" value="<?= Request::dataPost($user, 'Password', 'Password') ?>" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <!--div class="form-group">
                                        <label class="col-md-5 control-label">Rescriere parola</label>
                                        <div class="col-md-7">
                                            <input name="PasswordConfirmation" type="password" class="form-control" placeholder="">
                                        </div>
                                    </div-->

                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('Radius') ?></label>
                                        <div class="col-md-6">
                                            <input name="Coverage" type="number" value="<?= Request::dataPost($user_info, 'Coverage', 'Coverage') ?>" class="form-control col-md-4" placeholder="">
                                        </div>
                                        <span class="help-inline col-xs-12 col-md-1">
                                            <span class="middle">km</span>
                                        </span>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('UserStatus') ?></label>
                                        <div class="col-md-7">
                                            <?= User::statusSelect(Request::dataPost($user, 'Status', 'Status')) ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"><?= Lang::t('RegisteredOn') ?></label>
                                        <div class="col-md-7">
                                            <?= Html::datePicker('RegDate', Request::dataPost($user, 'RegDate', 'RegDate')) ?>
                                        </div>
                                    </div>
                                    
                                    <br />
                                    <div class="form-group">
                                        <label class="col-md-5 control-label"></label>
                                        <div class="col-md-7 text-right">
                                            <button class="btn btn-success" type="submit"><?= Lang::t('Save') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>



                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </div>
</section><!-- /.main-content -->

<script>
    
    PageJS = {
        
        setGalForm: function()
        {
            $('select[name=ParentID]').closest('.form-group').hide();
        },
        
        setProducatorForm: function()
        {
            $('select[name=ParentID]').closest('.form-group').show();
        }
        
    };
    
    $(document).ready(function(){
        
        $('select[name=Judet]').change(function(){
            $.post('<?= Url::link('ajax/city-by-judet.php') ?>', {JudetID: $(this).val()}, function(html){
                $('select[name=Location]').html(html);
            });
        });
        
        $('input[name=AccountType]').change(function(){
            if ($(this).val() == '<?= User::TypeGal ?>')
            {
                PageJS.setGalForm();
            }
            else
            {
                PageJS.setProducatorForm();
            }
        });
        
        <?php if (Request::dataPost($user, 'Type', 'AccountType') == User::TypeGal) { ?>
        PageJS.setGalForm();
        <?php } else { ?>
        PageJS.setProducatorForm();
        <?php } ?>
            
        <?php if (isset($_GET['galID'])) { ?>
         $('select[name=ParentID]').val(parseInt('<?= $_GET['galID'] ?>'));
        <?php } ?>
        
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>