<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Page.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$page_service = new Page();

$page_list = $page_service->getListForEdit($_SESSION['UserID']);

if (isset($_GET['delID']))
{
    $page_service->delete([
        'UserID' => $_SESSION['UserID'],
        'ID' => (int)$_GET['delID']
    ]);
    
    Url::redirect('admin/pages/index.php');
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php';
?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Pages') ?></h2>

        <div>
            <a href="<?= Url::link('admin/pages/add.php') ?>"><button class="btn btn-success"><i class="fa fa-plus"></i> <?= Lang::t('AddPage') ?></button></a>
        </div>
        <br />
        <div>
            <table class="table table-condensed dataTable">
                <thead>
                    <tr>
                        <th><?= Lang::t('Label') ?></th>
                        <th><?= Lang::t('Date') ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($page_list as $row) { ?>
                <tr>
                    <td><?= $row['Title'] ?></td>
                    <td><?= DateHelper::toDMY($row['Date']) ?></td>
                    <td>
                        <a href="<?= Url::link('admin/pages/add', ['id' => $row['ID']]) ?>"><i class="fa fa-pencil blue"></i></a>&nbsp;
                        <a href="<?= Url::link('admin/pages/index', ['delID' => $row['ID']]) ?>"><i class="fa fa-trash red"></i></a>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>