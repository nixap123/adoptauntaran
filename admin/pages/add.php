<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Page.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/PageLang.php';

$page_service = new Page();
$page_lang_service = new PageLang();

$pageID = (int)$_GET['id'];

if ($pageID > 0)
{
    $page = $page_service->getWhere(['ID' => $pageID], 1);
    $pageData = reset($page);
    
    if (empty($pageData['ID']))
    {
        Url::redirect('admin/pages/add.php', ['id' => 0]);
    }
    
    $Date = DateHelper::toDMY($pageData['Date']);
    $Url = $pageData['Url'];
    
    $pageLang = $page_lang_service->getByPageID($pageID);
    
    foreach ($pageLang as $langID => $row)
    {
        $titles[$langID] = $row['Title'];
        $texts[$langID] = $row['Text'];
    }
}


if (Request::isPost())
{
    $titles = Request::post('title');
    $texts = Request::post('text');
    
    $title_ro = $titles[1];
    $text_ro = $texts[1];
    
    $date = Validator::validate('Date', Validator::ValidateEmpty, 'PublishedDate');
    $Url = Request::post('Url');
    
    if (empty($title_ro))
    {
        Validator::setError(Lang::t('TitleIsRequired'));
    }
    
    if (empty($text_ro))
    {
        Validator::setError(Lang::t('TextIsRequired'));
    }
    
    if (!Validator::hasErrors())
    {
        $pageData = [];
        $pageData['Date'] = date('c', strtotime($date));
        
        if (empty($Url))
        {
            $pageData['Url'] = Url::strToUrl($title_ro);
        }
        else
        {
            $pageData['Url'] = $Url;
        }
        
        if ($pageID > 0)
        {
            $page_service->update($pageData, ['ID' => $pageID]);
        }
        else
        {
            $pageID = $page_service->insert([
                'UserID' => $_SESSION['UserID'],
                'Date' => $pageData['Date'],
                'Url' => $pageData['Url']
            ]);
        }
        
        $page_lang_service->delete(['PageID' => $pageID]);
        
        foreach ($titles as $langID => $name)
        {
            $page_lang_service->insert([
                'PageID' => $pageID,
                'LangID' => $langID,
                'Title' => isset($titles[$langID]) ? $titles[$langID] : '',
                'Text' => isset($texts[$langID]) ? $texts[$langID] : ''
            ]);
        }
        
        $_SESSION['saved'] = true;
        Url::redirect('admin/pages/add.php', ['id' => $pageID]);
    }
    
    if (!empty($_SESSION['saved']))
    {
        unset($_SESSION['saved']);
        Validator::setSuccess();
    }
}


require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php';
?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('AddPage') ?></h2>
        <div>
            <?= Validator::showMessages() ?>
        </div>
        <form method="post">
            <div class="form-group hidden">
                <label class="control-label"><?= Lang::t('PublishedDate') ?></label>
                <input type="text" name="Date" value="<?= DateHelper::toDMY(date('c')) ?>" class="form-control datepicker" />
            </div>
            <div class="form-group">
                <label class="control-label"><?= Lang::t('SeoUrl') ?></label>
                <input type="text" name="Url" value="<?= $pageData['Url'] ?>" class="form-control" />
            </div>
            
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?php foreach (Config::$LangList as $langID => $lang) { ?>
                <li role="presentation" class="<?= reset(Config::$LangList) == $lang ? 'active' : '' ?>"><a href="#tab-<?= $langID ?>" aria-controls="tab-<?= $langID ?>" role="tab" data-toggle="tab"><?= $lang['name'] ?></a></li>
                <?php } ?>
            </ul>
            <br />
            <!-- Tab panes -->
            <div class="tab-content">
                <?php foreach (Config::$LangList as $langID => $lang) { ?>
                <div role="tabpanel" class="tab-pane <?= reset(Config::$LangList) == $lang ? 'active' : '' ?>" id="tab-<?= $langID ?>">
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Title') ?></label>
                        <input type="text" name="title[<?= $langID ?>]" value="<?= isset($titles[$langID]) ? $titles[$langID] : '' ?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Text') ?></label>
                        <textarea rows="20" class="summer-note form-control" name="text[<?= $langID ?>]"><?= isset($texts[$langID]) ? $texts[$langID] : '' ?></textarea>
                    </div>
                </div>
                <?php } ?>
            </div>
            <br />
            <div>
                <button type="submit" class="btn btn-primary"><?= Lang::t('Save') ?></button>
            </div>
        </form>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>