<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';

Url::redirect('admin/notification');
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

$user_sercice = new User();

$last_users = $user_sercice->getLastRegistrations();
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left">Dashboard</h2>

        <div class="row">
            <div class="">
                <!-- PAGE CONTENT BEGINS -->

                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                    <div class="tabbable">
                        <ul class="nav nav-tabs padding-12 " id="myTab4">
                            <li class="active">
                                <a data-toggle="tab" href="#home4" aria-expanded="true">Recent</a>
                            </li>

                            <li class="">
                                <a data-toggle="tab" href="#profile4" aria-expanded="false">Ultimele comentarii</a>
                            </li>

                            <li class="">
                                <a data-toggle="tab" href="#dropdown14" aria-expanded="false">Abonati Newsletter</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="home4" class="tab-pane active">

                                <div class="row">
                                    <div class="col-md-12 notification-inn">
                                        <div class="row">

                                            <div class="notification-item col-md-4">
                                                <a class="bg-success">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Ați adăugat un produs în coș!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                                <a class="bg-success">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Ați adăugat un produs în coș!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                                <a class="bg-success">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Ați adăugat un produs în coș!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                            </div>
                                            <div class="notification-item col-md-4">
                                                <a class="bg-info">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Coș săptamânal - solicitat!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                                <a class="bg-info">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Coș săptamânal - solicitat!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                            </div>
                                            <div class="notification-item col-md-4">
                                                <a class="bg-danger">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Produsul (zmeură) - nu a fost livrat!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                                <a class="bg-danger">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Produsul (zmeură) - nu a fost livrat!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                                <a class="bg-danger">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Produsul (zmeură) - nu a fost livrat!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                                <a class="bg-danger">
                                                    <i class="fa fa-shopping-cart"></i> 
                                                    <strong>Produsul (zmeură) - nu a fost livrat!</strong> 
                                                    <span>11.09.2016</span>
                                                </a>
                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>

                            <div id="profile4" class="tab-pane">
                                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
                            </div>

                            <div id="dropdown14" class="tab-pane">
                                <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div>    
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>