<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Subscribe.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$subscribe_service = new Subscribe();

$subscribe_list = $subscribe_service->getList();

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Subscribers') ?> <span class="notif-count unread-notifications"><?= count($subscribe_list) ?></span></h2>
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th><?= Lang::t('Email') ?></th>
                            <th><?= Lang::t('Date') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($subscribe_list as $item) { ?>
                        <tr>
                            <td><?= $item['Email'] ?></td>
                            <td><?= DateHelper::toDMYHI($item['Date']) ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>