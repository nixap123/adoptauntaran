<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Setting.php';

$setting_service = new Setting();

$settings = $setting_service->getSettings();

if (Request::isPost())
{
    Setting::saveSettings(Request::post());
    Validator::setSuccess();
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/head.php';
?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Settings') ?></h2>

		<div class="row">
			<div class="col-lg-6 col-md-6">
				<form method="post">
                                    <div>
						<?= Validator::showMessages() ?>
						<?= Setting::getSettingsForm(Setting::adminSettingsList()) ?>
					</div>
					<br />
					<div>
						<button type="submit" class="btn btn-primary"><?= Lang::t('Save') ?></button>
					</div>
				</form>
			</div>
		</div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/template/footer.php'; ?>