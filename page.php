<?php
require_once 'core/core.php';
require_once 'services/Page.php';
require_once 'services/PageLang.php';

$page_service = new Page();

$page = $page_service->getByUrl($_GET['url']);

if (empty($page['Url'])) {
    Url::redirect();
}


$TITLE = $page['Title'];

require_once 'template/head.php';
?>



<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?php echo $page['Title']; ?></h2>
<?php echo $page['Text']; ?>
    </div>
</section>


<?php
require_once 'template/footer.php';
?>


   