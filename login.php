<?php
require_once 'core/core.php';
require_once 'services/User.php';
require_once 'libraries/GCaptcha.php';

$user_sercice = new User();

$useCaptcha = !empty($_SESSION['login_errors']) && $_SESSION['login_errors'] >= 2 ? true : false;

if (isset($_POST['Login'])) {
    
    if ($useCaptcha)
    {
        $secret = "6LfTZAcUAAAAAPmXcBsRxg90PgXYkHIpiGJasjVn";
        
        $reCaptcha = new ReCaptcha($secret);
        
        $response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
        
        if (!$response->success)
        {
            Validator::setError('Invalid Captcha');
        }
    }
    
    if (!Validator::hasErrors())
    {
        $user = $user_sercice->doLogin($_POST['UserName'], $_POST['Password']);

        if (empty($user['ID']))
        {
            Validator::setError('Email sau Parola sunt incorecte!');

            if (isset($_SESSION['login_errors']))
            {
                $_SESSION['login_errors'] += 1;
            }
            else
            {
                $_SESSION['login_errors'] = 1;
            }
        }

        if (!Validator::hasErrors())
        {
            if ($user['Status'] == User::StatusInactive)
            {
                Validator::setError('Your account is inactive!');
            }
            else
            {
                $_SESSION['UserID'] = $user['ID'];
            }
        }


    }
    
    exit(json_encode([
        'errors' => Validator::showMessages(),
        'result' => Validator::hasErrors() ? '' : '<script>window.location.href="' . $user_sercice->accountLink($user) . '";</script>',
        'useCaptcha' => $useCaptcha
    ]));
}

require_once 'template/head.php';
?>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('Login') ?></h2>
		
        <div class="row">
            <form id="login-form" class="login-form col-md-6 col-sm-10 col-xs-10 form-horizontal" method="post">
			
                <div id="login-error"></div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('Email') ?></label>
                    <div class="col-md-9">
                        <input type="text" name="UserName" placeholder="<?= Lang::t('InsertEmailAddress') ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('Password') ?></label>
                    <div class="col-md-9">
                        <input type="password" name="Password" placeholder="*********" />
                    </div>
                </div>
                <div>
                    <div id="captcha-wrap" class="pull-right <?php if (!$useCaptcha) { ?>hidden<?php } ?>">
                        <div class="g-recaptcha" data-sitekey="6LfTZAcUAAAAAL50681sFVbj8NJuYhHnlNApvNhQ"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <input type="hidden" name="Login" value="1" />
                <div class="form-group text-right" style="margin-right: 0px;">
                    <input type="submit" id="login-button" value="Login" class="go-login red-btn" /><br /><br />
                    <p><a href="<?= Url::link('password_recover') ?>"><?= Lang::t('PasswordRecovery') ?></a></p>
                </div>
                <div class="form-group text-center">						
                    <p><a href="<?= Url::link('termeni') ?>"><?= Lang::t('TermsAndConditions') ?></a></p>			
                    <p><a href="<?= Url::link('confidentialitate') ?>"><?= Lang::t('PrivacyPolicy') ?></a></p>
                </div>
            </form>
        </div>
    </div>
</section>	

<section class="home-block">
    <div class="container">
        <a href="<?= Url::link('register.php') ?>" class="arrows-btn">
            <?= Lang::t('Register here') ?><br />
            <?= Lang::t('AdoptAPeasant') ?>
        </a>
    </div>
</section>

<script>

    var Login = {
        doLogin: function ()
        {
            $('.alert').remove();
            $('.has-error').removeClass('has-error');

            var valid = true;
            var form = $('#login-form');

            if (form.find('input[name=UserName]').val() == "")
            {
                form.find('input[name=UserName]').closest('label').addClass('has-error');
                valid = false;
            }

            if (form.find('input[name=Password]').val() == "")
            {
                form.find('input[name=Password]').closest('label').addClass('has-error');
                valid = false;
            }

            if (valid)
            {
                $.post('<?= Url::link('login.php') ?>', form.serialize(), function (json) {
                    if (json.errors === '')
                    {
                        document.write(json.result);
                    }
                    else
                    {
                        $('#login-error').html(json.errors);
                        if (json.useCaptcha == true)
                        {
                            $('#captcha-wrap').removeClass('hidden');
                            grecaptcha.reset();
                        }
                    }
                }, 'json');
            }

            return false;
        }

    };

    $(document).ready(function () {
        $('#login-button').click(Login.doLogin);
    });

</script>

<?php require_once 'template/footer.php'; ?>