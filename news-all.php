<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/News.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$user_service = new User();
$news_service = new News();

$userID = (int)$_GET['id'];

$user = $user_service->getByID($userID);

if (empty($user['ID'])) Url::redirect();

$news = $news_service->getByUserID($user['ID']);

require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php';     
?>


<section class="home-block">
    <div class="container">
        <h2 class="text-left center-title margin-10"><?= Lang::t('News') ?></h2>
        <br />
        <div class="profile-news">
            <?php foreach ($news as $item) { ?>
            <article>
                <a href="<?= Url::link('blog/' . $item['ID'] . '-' . Url::strToUrl($item['Title'])) ?>"><?= $item['Title'] ?></a>
                <time>Adăugat: <?= DateHelper::toDMY($item['Date']) ?></time>
            </article>
            <?php } ?>
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>