<?php

define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR);
define('BASE_URL', $_SERVER['SERVER_ADDR'] == '127.0.0.1' ? 'http://adoptauntaran.loc/' : 'http://adoptauntaran.ml/');

class Config
{

    public static $BaseUrl = BASE_URL;
    
    public static $DbHost = 'adoptauntaran.ml';
    public static $DbName = 'flycode_adopta';
    public static $DbUser = 'flycode_adopta';
    public static $DbPassword = '010101';
    
    public static $TVARate = 0.2;
    
    // Site languages
    public static $LangList = [
        1 => ['name' => 'Romanian'],
        2 => ['name' => 'English'],
    ];
    
    // Product units
    public static $UnitList = [
        1 => [
            1 => 'kg',
            2 => 'kg'
        ],
        2 => [
            1 => 'buc',
            2 => 'item'
        ]
    ];
    
    // pyments
    public static $PaypalEmail = 'work.colesnic89@gmail.com';
    
}