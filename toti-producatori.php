<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';

$user_service = new User();

$res = $user_service->getProducatoriByJudet(0);
$usersByJudet = $res['result'];
$usersCount = $res['count'];

require_once $_SERVER['DOCUMENT_ROOT'] .'/template/head.php';

?>

<section class="home-block">
    <div class="container">
        <h2 class="text-left center-title"><?= Lang::t('AllManufacturers') ?></h2>
        <div class="page-content">
            <div class="row">
                <?php foreach ($usersByJudet as $userID => $producator) { ?>
                <div class="profile-item col-md-4">
                    <a href="<?= Url::link($userID . '-' . Url::strToUrl($producator['User']['Name'])) ?>">
                        <div class="profile-item-img">
                            <span class="red-square"></span>
                            <img src="<?= Url::link('assets/images/_taran.png') ?>" alt="">
                        </div>
                        <h3><?= $producator['User']['Name'] ?></h3>
                        <p><strong><?= Lang::t('Offer') ?>:</strong> <?= implode(', ', $producator['Categories']) ?></p>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>