var Cart = {
    
    typeCart: 'CartWeek',
    typeProduct: 'Product',
    
    add: function(ID, quantity, type)
    {
        $.post('/ajax/cart/add.php', {ID: ID, quantity: quantity, type: type}, function(json){
            if (json.valid == false)
            {
                notification(json.messages, 'warning', 6000);
            }
            else
            {
                Cart.updateStatus();
                notification(json.messages, 'success', 6000);
            }
        }, 'json');
        return false;
    },

    remove: function (ID, type)
    {
        $.post('/ajax/cart/delete.php', {ID: ID, type: type}, function(json){
            if (json.valid == false)
            {
                notification(json.messages, 'warning');
            }
            else
            {
                if (type == Cart.typeCart)
                {
                    $('tr[ocid=' + ID + ']').remove();
                }
                else
                {
                    $('tr[ocpid=' + ID + ']').remove();
                }
                
                Cart.updateStatus();
                Cart.updateTotal(json, ID);
                notification(json.messages, 'success');
            }
        }, 'json');
        return false;
    },
    
    removeForever: function(ID)
    {
        $.post('/ajax/cart/delete-forever.php', {ID: ID}, function(json){
            if (json.valid == false)
            {
                notification(json.messages, 'warning');
            }
            else
            {
                $('tr[ocid=' + ID + ']').remove();
                
                Cart.updateStatus();
                Cart.updateTotal(json, ID);
                notification(json.messages, 'success');
            }
        }, 'json');
        return false;
    },

    update: function (ocpID, quantity)
    {
        $.post('/ajax/cart/update.php', {ocpID: ocpID, quantity: quantity, type: Cart.typeProduct}, function(json){
            if (json.valid == false)
            {
                notification(json.messages, 'warning');
            }
            else
            {
                Cart.updateStatus();
                Cart.updateTotal(json, ocpID);
                notification(json.messages, 'success', 6000);
            }
        }, 'json');
        return false;
    },
    
    updateTotal: function(json, ocpID)
    {
        var cartRow = $('tr[ocpid=' + ocpID + ']');
        cartRow.find('.PriceWithoutTVA').html(json.data.PriceWithoutTVA);
        cartRow.find('.TotalWithoutTVA').html(json.data.TotalWithoutTVA);
        cartRow.find('.TotalTVA').html(json.data.TotalTVA);

        var GrandTotalWithoutTVA = parseFloat($('#GrandTotalWithoutTVA').text()) + parseFloat(json.data.GrandTotalWithoutTVA);
        $('#GrandTotalWithoutTVA').html(Math.round(GrandTotalWithoutTVA * 100) / 100);

        var GrandTotalTVA = parseFloat($('#GrandTotalTVA').text()) + parseFloat(json.data.GrandTotalTVA);
        $('#GrandTotalTVA').html(Math.round(GrandTotalTVA * 100) / 100);

        var CartTotal = parseFloat($('#CartTotal').text()) + parseFloat(json.data.CartTotal);
        $('#CartTotal').html(Math.round(CartTotal * 100) / 100);
    },
    
    updateStatus: function()
    {
        $.post('/ajax/cart/status.php', {}, function(json){
            $('#cart-count').html(json.count);
            $('#cart-price').html(json.total);
            
            if (json.total == 0)
            {
                $('#order-button').hide();
            }
        }, 'json');
    },
    
    doOrder: function()
    {
        var data = $('#cart-form').serialize();
        
        $.post('/ajax/cart/order.php', data, function(json){
            if (json.valid == false)
            {
                notification(json.messages, 'warning', 10000);
            }
            else
            {
                $('#success-order-modal').modal('show');
                $('#success-order-modal').on('hidden.bs.modal', function () {
                    window.location.reload();
                });
            }
        }, 'json');
        return false;
    }

};

$(document).ready(function (){
    Cart.updateStatus();
});