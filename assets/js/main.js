var Error = {};

var Rating = {
    
    set: function(vote, userID, elem)
    {
        $.post('/ajax/rating/set.php', {vote: vote, userID: userID}, function(json){
            if (json.success)
            {
                Rating.get(userID, elem);
                notification('Voted', 'success');
            }
            else
            {
                notification('Allready voted', 'warning');
            }
        }, 'json');
    },
    
    get: function(userID, elem)
    {
        $.post('/ajax/rating/get.php', {userID: userID}, function(html){
            $(elem).closest('.rating-widget').html(html);
        });
    }
    
};


$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 300) {
        $('.menu').fadeIn(500);
    } else {
        $('.menu').fadeOut(500);
    }

});


