<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/News.php';

$news_service = new News();

$news = $news_service->getByID($_GET['id']);

require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php';     
?>


<section class="home-block">
    <div class="container">
        <h2 class="text-left center-title"><?= $news['Title'] ?></h2>
        <div class="news-content">
            <?= $news['Text'] ?>
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>