<?php
require_once 'core/core.php';
require_once 'services/Page.php';
require_once 'services/PageLang.php';


$TITLE = 'Contact';

require_once 'template/head.php';
?>



<section class="home-block">
    <div class="container">
        <h2 class="center-title">Contact</h2>
        <div class="text-center">
            <h4>INIȚIATORI</h4>
            Acest proiect aparține tuturor românilor. Totuși, în calitate de inițiatori, noi suntem:<br />
            Asociația Creștem România Împreună, cu sediul în Brașov,<br />
            Parcul Industrial Metrom, str. Carpaților nr. 60. <br />
            Ne găsești la telefon: 0040 734 596 089, fax: 0040 268 417 164<br />
            si e-mail: <a href="mailto:contact@cri.org.ro">contact@cri.org.ro</a>
            <br /><br />
            <h4>TRIMITE UN MESAJ</h4>
            Pentru a ne trimite un mesaj, te rugam sa completezi formularul de mai jos.<br /><br />
        </div>
        <div class="row">
            <form id="login-form" class="login-form col-md-12 col-sm-12 col-xs-10 form-horizontal" method="post">

                <?= Validator::showMessages() ?>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nume</label>
                            <div class="col-md-9">
                                <input type="text" name="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <input type="email" name="" />
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Prenume</label>
                            <div class="col-md-9">
                                <input type="text" name="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Telefon</label>
                            <div class="col-md-9">
                                <input type="text" name="" />
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">Mesaj</label>
                        <div class="col-md-10 row">
                            <textarea rows="5"></textarea>
                        </div>
                    </div>
                    
                </div>
                
            </form>
            
        </div>
        <a href="#" class="arrows-btn">
            Trimite<br />
             Mesaj!
        </a>
    </div>
</section>


<?php
require_once 'template/footer.php';
?>


