


<section class="before-foot">

    <div class="clearfix"></div>

    <p>&nbsp;</p>

    <div class="nat-patt"></div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>

    <div class="container">
        <div class="row">
            <div class="newsletter-block col-md-6">
                <h2 class="center-title">Newsletter</h2>

                <p>
                    <!--Pentru a vă abona la newsletter, <br />
                    vă rugăm introduceți adresa de e-mail: -->
                    <?= Lang::t('FooterNewsletter') ?>
                </p>

                 <form onsubmit="return subscribe(this);">
                    <input required class="col-md-7" type="email" name="Email" placeholder="nume@domen.ro" />
                    <button type="submit" class="col-md-4"><?= Lang::t('Add') ?></button>
                </form>

            </div>

            <div class="bottom-menu col-md-6">
                <h2 class="center-title text-left"><?= Lang::t('AboutProject') ?></h2>
                <ul>
                    <li><a href="<?= Url::link('about') ?>"><?= Lang::t('ProjectDescription') ?></a></li>
                    <li><a href="<?= Url::link('initiatori') ?>"><?= Lang::t('AboutInitiators') ?></a></li>
                    <li><a href="<?= Url::link('contact') ?>"><?= Lang::t('Contact') ?></a></li>	
                </ul>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <span>&copy; 2015 - 2016 <?= Lang::t('FooterCopyright') ?></span>
            </div>
            <div class="footer-socials col-md-6">
                <span>
                    <?= Lang::t('SocialNetwork') ?>
                    <a href="" target="_blank"><i class="fa fa-facebook-square"></i></a>
                    <a href="" target="_blank"><i class="fa fa-twitter-square"></i></a>
                    <a href="" target="_blank"><i class="fa fa-google-plus-square"></i></a>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href=""><?= Lang::t('PrivacyPolicy') ?></a>
            </div>
            <div class="col-md-6 text-right">
                <small><?= Lang::t('CreatedBy') ?></small> <a href="" target="_blank"><img src="<?= Url::link('assets/images/zl.jpg') ?>"></a>
            </div>
        </div>
    </div>
</footer>
<script>

    function subscribe(form)
    {
        var email = $(form).find('input[name=Email]').val();
        
        $.post('<?= Url::link('ajax/subscribe.php') ?>', {email: email}, function(json){
            if (json.success)
            {
                notification(json.messages, 'success', 5000);
                $(form).trigger('reset');
            }
            else
            {
                notification(json.messages, 'warning', 5000);
            }
        }, 'json');
        
        return false;
    }

</script>
</body>
</html>