</div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- basic scripts -->


<script type="text/javascript">
    if ('ontouchstart' in document.documentElement)
        document.write("<script src='<?= Url::link('assets/js/jquery.mobile.custom.min.js') ?>'>" + "<" + "/script>");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {
        $(document).on('click', '.toolbar a[data-target]', function (e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $(target).addClass('visible');//show target
        });
    });
</script>
</body>
</html>