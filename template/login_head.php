<!DOCTYPE html>
<html lang="ro">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Dashboard - Adopta un țăran</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= Url::link('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />
        
        <!--[if !IE]> -->
        <script src="<?= Url::link('assets/js/jquery-2.1.4.min.js') ?>"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="<?= Url::link('assets/js/jquery-1.11.3.min.js') ?>"></script>
        <![endif]-->

        <!-- text fonts <?= Url::link('') ?> -->
        <link rel="stylesheet" href="<?= Url::link('assets/css/fonts.googleapis.com.css') ?>" />

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= Url::link('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="<?= Url::link('assets/css/ace-part2.min.css') ?>" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= Url::link('assets/css/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= Url::link('assets/css/ace-rtl.min.css') ?>" />

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="<?= Url::link('assets/css/ace-ie.min.css') ?>" />
        <![endif]-->

        <!-- ace settings handler -->
        <script src="<?= Url::link('assets/js/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
        <!--[if lte IE 8]>
        <script src="<?= Url::link('assets/js/html5shiv.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/respond.min.js') ?>"></script>
        <![endif]-->
    </head>
    <body class="login-layout light-login">
        <div class="main-container">
            <div class="main-content">