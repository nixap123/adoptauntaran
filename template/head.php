<?php
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

    $user_service = new User();

    if (isset($_SESSION['UserID'])) {
        $USER = $user_service->getByID($_SESSION['UserID']);
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?= isset($TITLE) ? $TITLE : 'Adopta un taran' ?></title>
        <meta charset="UTF-8" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/styles.css') ?>" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap-theme.min.css') ?>">
        <link rel="stylesheet" href="<?= Url::link('assets/css/select2.min.css') ?>">

        <link rel="stylesheet" href="<?= Url::link('assets/js/fancybox/jquery.fancybox.css') ?>">

        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">

        <script src="<?= Url::link('assets/js/jquery.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/bootstrap.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/select2.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= Url::link('assets/js/main.js') ?>"></script>

        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
        <script>
            $(document).ready(function () {
                $('.summer-note').summernote({
                    height: 300,
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('.summer-note-mini').summernote({
                    height: 150,
                });
            });
        </script>
        <script src="<?= Url::link('assets/js/product/cart.js') ?>"></script>
        <script type="text/javascript" src="<?= Url::link('assets/js/jquery.noty.packaged.min.js') ?>"></script>
        <script>
            function notification(text, type, timeout) {
                var n = noty({
                    text: text,
                    type: type,
                    dismissQueue: true,
                    timeout: timeout == undefined ? 3000 : timeout,
                    closeWith: ['click'],
                    layout: 'topRight',
                    theme: 'defaultTheme',
                    maxVisible: 10
                });
            }
        </script>
    </head>
    <body>

        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="user-bar col-md-6 col-sm-6 col-xs-12">
                        <iframe class="hidden-sm hidden-xs" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fadoptauntaran.ro&width=117&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId=293042120875424" width="200" height="46" style="float:left; border:none;overflow:hidden; margin-right:15px;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        <?php if (empty($_SESSION['UserID'])) { ?>
                        <a href="<?= Url::link('login.php') ?>"><?= Lang::t('Login') ?></a> <?= Lang::t('Or') ?> <a href="<?= Url::link('register.php') ?>"><?= Lang::t('Register') ?></a>
                        <?php } else { ?>
                            <a href="<?= $user_service->accountLink($USER) ?>">Account</a> | <a href="<?= Url::link('logout.php') ?>">Logout</a>
                        <?php } ?>
                    </div>
                    <div class="donate-bar col-md-6 col-sm-6 col-xs-12">
                        <i class="fa fa-credit-card"></i> <a href="<?= Url::link('doneaza.php') ?>"><?= Lang::t('DonateForThisProject') ?></a>                        
                    </div>
                </div>
                        <div class="lang-min">
                            <?= Lang::t('SelectLanguage') ?>:
                            <a href="<?= Url::switchLang('ro') ?>"><img src="/assets/images/ro.png" /></a>
                            <a href="<?= Url::switchLang('en') ?>"><img src="/assets/images/en.png" /></a>
                        </div>
            </div>
        </div>

        <header class="header">
            <div class="container">
                <div class="logo-bar">
                    <a href="<?= Url::link() ?>" title=""><img src="<?= Url::link('assets/images/logo.png') ?>" alt=""></a>
                </div>
                <div class="top-text">
                    <h2>
                        <?= Lang::t('TopDesc') ?>
                    </h2>
                </div>
            </div>

            <div class="nat-patt"></div>
        </header>

        <div class="menu hidden-sm hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <a href="<?= Url::link() ?>" class="logo-min"><img src="/assets/images/logo_min.png" /></a>
                    </div>
                    <div class="col-md-9">
                        <div class="lang-min">
                            <?= Lang::t('SelectLanguage') ?>:
                            <a href="<?= Url::switchLang('ro') ?>"><img src="/assets/images/ro.png" /></a>
                            <a href="<?= Url::switchLang('en') ?>"><img src="/assets/images/en.png" /></a>
                        </div>
                        <ul>
                            <li><a href="<?= Url::link() ?>"><?= Lang::t('Home') ?></a></li>
                            
                            <?php if (empty($_SESSION['UserID'])) { ?>
                            <li><a href="<?= Url::link('login.php') ?>"><?= Lang::t('Login') ?></a> <?= Lang::t('Or') ?> <a href="<?= Url::link('register.php') ?>"><?= Lang::t('Register') ?></a></li>
                            <?php } else { ?>
                                <li><a href="<?= $user_service->accountLink($USER) ?>">Account</a> | <a href="<?= Url::link('logout.php') ?>">Logout</a></li>
                            <?php } ?>
                            
                            <li><a href="<?= Url::link('doneaza.php') ?>"><?= Lang::t('Donate') ?></a></li>
                            <li><a href="<?= Url::link('despre-proect.php') ?>"><?= Lang::t('AboutProject') ?></a></li>
                            <li><a href="<?= Url::link('despre-initiatori.php') ?>"><?= Lang::t('AboutInitiators') ?></a></li>
                            <li><a href="<?= Url::link('contacte.php') ?>"><?= Lang::t('Contact') ?></a></li>
                        </ul>
                    </div>
                </div>
                <p>&nbsp;</p>
            </div>
        </div>