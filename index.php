<?php
require_once 'core/core.php';
require_once 'services/User.php';
require_once 'services/Judet.php';
require_once 'services/News.php';

$user_service = new User();
$news_service = new News();

$randomUsers = $user_service->getRandomProducators();

$lastNews = $news_service->getLastNews(1, 6);


?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php'; ?>

<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('HowWork') ?></h2>
        <div class="row">
            <div class="cum-fun col-md-3 col-sm-6 col-xs-12">
                <span>1.</span>
                <p>
                    <?= Lang::t('HW1') ?>
                    (<a href="<?= Url::link('register') ?>"><?= Lang::t('ClickHere') ?></a>)
                </p>
            </div>
            <div class="cum-fun col-md-3 col-sm-6 col-xs-12">
                <span>2.</span>
                <p>
                    <?= Lang::t('HW2') ?>
                </p>
            </div>
            <div class="cum-fun col-md-3 col-sm-6 col-xs-12">
                <span>3.</span>
                <p>
                    <?= Lang::t('HW3') ?>
                </p>
            </div>
            <div class="cum-fun col-md-3 col-sm-6 col-xs-12">
                <span><img src="assets/images/crenguta.png" /></span>
                <p>
                    <a href="/about">
                        <?= Lang::t('HW4') ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>	

<section id="producatori" class="home-block">
    <div class="container">
        <a href="<?= Url::link('register.php') ?>" class="arrows-btn">
            
            <?= Lang::t('RegisterNow') ?>
        </a>
    </div>
</section>

<section class="home-block">
    <div class="container">
        <h2 id="judet-selector" class="center-title"><?= Lang::t('HomeSelectJudet') ?>: <div class="select-judet"><?= Judet::judetSelect($_SESSION['SelectedJudet']) ?> <i class="fa fa-caret-down"></i></div></h2>
        <div id="producatori-wrap" class="row">
            <?php foreach ($randomUsers as $key => $producator) { ?>
                <?php if (count($producator['Categories']) > 0) { ?>
                    <div class="profile-item col-md-4 col-sm-6 col-xs-12">
                        <a href="<?= Url::link($key . '-' . Url::strToUrl($producator['User']['Name'])) ?>">
                            <div class="profile-item-img">
                                <span class="red-square"></span>
                                <img src="<?= Url::link('uploads/profile/' . $producator['User']['Image']) ?>" alt="">
                            </div>
                            <h3><?= $producator['User']['Name'] ?></h3>
                            <!--p><strong>Caută:</strong> 20 familii (40 persoane)</p-->
                            <p><strong><?= Lang::t('Offer') ?>:</strong> <?= implode(', ', $producator['Categories']) ?></p>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if (count($randomUsers) > 6) { ?>
        <div class="text-center">
            <button id="more-users" class="red-btn"><?= Lang::t('More') ?>...</button>
        </div>
        <?php } ?>
    </div>
</section>


<section class="home-block">
    <div class="container">
        <a href="<?= Url::link('sugereaza.php') ?>" class="arrows-btn">
            <?= Lang::t('SuggestT') ?>
        </a>
    </div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

    <div class="nat-patt"></div>
</section>

<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('LastFromPress') ?></h2>
        <div class="row">
            <?php foreach ($lastNews as $news) { ?>
            <div class="news-item col-md-4 col-sm-6 col-xs-12">
                <a href="<?= Url::link('blog/' . $news['ID'] . '-' . Url::strToUrl($news['Title'])) ?>">
                    <h2><?= $news['Title'] ?></h2>
                    <span>
                        <?= mb_substr(strip_tags($news['Text']), 0, 234) ?>...
                    </span>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>

<section class="home-block partners-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('OurPartners') ?></h2>


        <a href="" target="_blank">
            <img src="<?= Url::link('assets/images/partners/radacini.png') ?>">
        </a>
        <a href="" target="_blank">
            <img src="<?= Url::link('assets/images/partners/micu.png') ?>">
        </a>
        <a href="" target="_blank">
            <img src="<?= Url::link('assets/images/partners/brno.png') ?>">
        </a>
        <a href="" target="_blank">
            <img src="<?= Url::link('assets/images/partners/erasmus.png') ?>">
        </a>

    </div>

</section>

<script>

var offset = 0;

$('#judet-selector select[name=Judet]').change(function(){
    var judetID = $(this).val();
    $.post('<?= Url::link('ajax/get-producatori-by-judet.php') ?>', {JudetID: judetID}, function(json){
        $('#producatori-wrap').html(json.html);
        offset = 0;
    }, 'json');
});

$('#more-users').click(function(){
    $.post('<?= Url::link('ajax/get-producatori-by-judet.php') ?>', {JudetID: $('#judet-selector select[name=Judet]').val(), offset: offset}, function(json){
        $('#producatori-wrap').html(json.html);
        offset = offset + 6;
        if (offset > json.count)
        {
            $('#more-users').hide();
        }
    }, 'json');
});

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>
