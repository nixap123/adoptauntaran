<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';

$product_stock_service = new ProductStock();
$order_service = new Order();

$product_list = $product_stock_service->getActiveProductStocks($_SESSION['UserID']);

$popularProducts = $order_service->mustPopularProducts($_SESSION['UserID']);

$mustActiveUsers = $order_service->mustAcvtiveClients($_SESSION['UserID']);

$biggestStocks = $order_service->bigestProductStocks($_SESSION['UserID']);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/head.php'; ?>

<style>
    text[text-anchor="middle"]{
        opacity: 0;
    }
</style>

<script type="text/javascript">
    
    $(function () {
        var popularProducts = new Morris.Bar({
            element: 'popular-products',
            data: <?= json_encode($popularProducts) ?>,
            xkey: 'product',
            ykeys: ['quantity'],
            labels: ['Cantitatea'],
            barColors: ['#cc3333'],
            hideHover: true,
            resize: true,
            stacked: true,
            parseTime: false
        });
        var activeUsers = new Morris.Bar({
            element: 'must-active-users',
            data: <?= json_encode($mustActiveUsers) ?>,
            xkey: 'name',
            ykeys: ['quantity'],
            labels: ['Cantitatea'],
            barColors: ['#cc3333'],
            hideHover: true,
            resize: true,
            stacked: true,
            parseTime: false
        });
        var biggestStocks = new Morris.Bar({
            element: 'biggest-stocks',
            data: <?= json_encode($biggestStocks) ?>,
            xkey: 'product',
            ykeys: ['quantity'],
            labels: ['Cantitatea'],
            barColors: ['#cc3333'],
            hideHover: true,
            resize: true,
            stacked: true,
            parseTime: false
        });
    });

</script>

<section>
    <div class="container">
        <h2 class="center-title"><?= Lang::t('EditProducts') ?></h2>
        <div class="row">
            <div class="col-md-4 text-center">
                <h4><?= Lang::t('MostOrderedProducts') ?></h4>
                <div id="popular-products" style="height: 200px;">
                    
                </div>
            </div>
            <div class="col-md-4 text-center">
                <h4><?= Lang::t('MostActiveUsers') ?></h4>
                <div id="must-active-users" style="height: 200px;">
                    
                </div>
            </div>
            <div class="col-md-4 text-center">
                <h4><?= Lang::t('LargestStocks') ?></h4>
                <div id="biggest-stocks" style="height: 200px;">
                    
                </div>
            </div>
        </div>
        <div>
            <button data-toggle="modal" data-target=".edit-ps-modal" data-ps-id="0" class="btn btn-success btn-sm edit-ps"><i class="fa fa-plus"></i> <?= Lang::t('AddProduct') ?></button>
        </div>
        <br />
        <div id="ps-table-messages">
            
        </div>
        <div id="ps-table-wrap">
            
        </div>
    </div>
</section>

<div class="modal fade edit-ps-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= Lang::t('AddProduct') ?>
            </div>
            <div class="modal-body">
                <div id="ps-form-messages">
                    
                </div>
                <form id="edit-ps-form">
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-modal" data-dismiss="modal"><?= Lang::t('Close') ?></button>
                <button id="save-ps" type="button" class="btn btn-primary"><?= Lang::t('Save') ?></button>
            </div>
        </div>
    </div>
</div>

<script>

    var PageJS = {
        
        getPSList: function()
        {
            $.post('<?= Url::link('ajax/product_stock/list.php') ?>', {}, function(html){
                $('#ps-table-wrap').html(html);
                
                var count = $('#ps-table tbody tr').length;
                $('#user-ps-count').html(count);
            });
        },
                
        getPSEditForm: function(psId)
        {
            $('#ps-form-messages').empty();
            $.post('<?= Url::link('ajax/product_stock/get.php') ?>', {psID: psId}, function(html){
                $('#edit-ps-form').html(html);
            });
        },
        
        productSave: function ()
        {
            $('#ps-form-messages').empty();
            $.post('<?= Url::link('ajax/product_stock/edit.php') ?>', $('#edit-ps-form').serialize(), function(json){
                $('#ps-form-messages').html(json.messages);
                
                if (json.error != true)
                {
                    $('#edit-ps-form input[name=psID]').val(json.psID);
                    PageJS.getPSList();
                    setTimeout(function(){
                        $('.edit-ps-modal .close-modal').click();
                    }, 500);
                }
            }, 'json');
        },
                
        psDelete: function(psID)
        {
            $('#ps-table-messages').empty();
            $.post('<?= Url::link('ajax/product_stock/delete.php') ?>', {psID: psID}, function(json){
                if (json.error == true)
                {
                    $('#ps-table-messages').html(json.messages);
                }
                PageJS.getPSList();
            }, 'json');
        }
        
    };
    
    $(document).ready(function(){
        PageJS.getPSList();
        
        $('body').on('click', '.edit-ps', function(){
            var psId = $(this).attr('data-ps-id');
            PageJS.getPSEditForm(psId);
        });
        
        $('#save-ps').click(function(){
            PageJS.productSave();
        });
        
        $('body').on('click', '.delete-ps', function(){
            if (confirm('Confirm?'))
            {
                var psID = $(this).attr('data-ps-id');
                PageJS.psDelete(psID);
            }
        });
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/footer.php'; ?>