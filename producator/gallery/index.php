<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/check.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/check.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Upload.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserPhoto.php';

    $image_service = new Image();
    $user_photo_service = new UserPhoto();
    
    $userID = $_SESSION['UserID'];
    
    if (isset($_GET['delImg']))
    {
        $imgID = (int)$_GET['delImg'];
        
        $user_photo_service->deleteByID($imgID);
        Url::redirect('producator/gallery/index.php');
    }
    
    if (Request::isPost())
    {
        $images = Upload::file('files[]', 'uploads/gallery', ['jpg', 'png']);

        if (!empty($images))
        {
            foreach ($images as $img)
            {
                $thumb = 'thumb_' . $img;
                Upload::resize(250, $_SERVER['DOCUMENT_ROOT'] . '/uploads/gallery/' . $thumb, $_SERVER['DOCUMENT_ROOT'] . '/uploads/gallery/' . $img);
                Upload::resize(1200, $_SERVER['DOCUMENT_ROOT'] . '/uploads/gallery/' . $img, $_SERVER['DOCUMENT_ROOT'] . '/uploads/gallery/' . $img);

                $imgID = $image_service->insert([
                    'Thumb' => $thumb,
                    'Image' => $img
                ]);
                $user_photo_service->insert([
                    'UserID' => $_SESSION['UserID'],
                    'ImageID' => $imgID
                ]);
            }
            
            Validator::setSuccess(Lang::t('ImagesUploaded'));
        }
    }
    
    $user_photos = $user_photo_service->getByUserID($userID);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Gallery') ?></h2>

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <?= Validator::showMessages() ?>
                    <div>
                        <form method="post" class="form-inline" enctype="multipart/form-data">
                            <div class="form-group">
                                <input name="files[]" type="file" multiple class="form-control" />
                            </div>
                            <div class="form-group">
                                <button type="submit" name="submit" class="btn bg-primary"><?= Lang::t('Upload') ?></button>
                            </div>
                        </form>
                    </div>
                    <hr />
                    <div class="row">
                        <?php foreach ($user_photos as $photo) { ?>
                        <div class="col-md-3 text-center">
                            <a style="margin:15px 0; padding:5px 0; border-bottom: 5px solid #cc3333; display: block; height: 150px; overflow: hidden;" rel="gallery" class="lightbox" href="<?= Url::link('uploads/gallery/' . $photo['Image']) ?>">
                                <img src="<?= Url::link('uploads/gallery/' . $photo['Thumb']) ?>" class="img-thumbnail" />
                            </a>
                            <a href="<?= Url::link('producator/gallery/index', ['delImg' => $photo['upID']]) ?>" style="cursor: pointer"><i class="fa fa-trash-o"></i></a>
                        </div>
                        <?php } ?>
                        
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </div>
</section><!-- /.main-content -->

<script>
    
    PageJS = {
        
      
        
    };
    
    $(document).ready(function(){
        
        $('.lightbox').fancybox();
        
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/footer.php'; ?>