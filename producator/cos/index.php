<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Category.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';

$product_stock_service = new ProductStock();

$product_list = $product_stock_service->getActiveProductStocks($_SESSION['UserID']);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/head.php'; ?>

<section>
    <div class="container">
        <h2 class="center-title"><?= Lang::t('EditCart') ?></h2>
        <div>
            <button data-toggle="modal" data-target=".edit-cos-modal" data-cos-id="0" class="btn btn-success btn-sm edit-cos"><i class="fa fa-plus"></i> <?= Lang::t('Add') ?></button>
        </div>
        <br />
        <div id="cos-table-messages">
            
        </div>
        <div id="cos-table-wrap">
            
        </div>
    </div>
</section>

<div class="modal fade edit-cos-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= Lang::t('AddCart') ?>
            </div>
            <div class="modal-body">
                <div id="cos-form-messages">
                    
                </div>
                <form id="edit-cos-form" enctype="multipart/form-data">
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Lang::t('Cart') ?></button>
                <button id="save-cos" type="button" class="btn btn-primary"><?= Lang::t('Save') ?></button>
            </div>
        </div>
    </div>
</div>

<script>

    var PageJS = {
        
        getCosList: function()
        {
            $.post('<?= Url::link('ajax/cos/list.php') ?>', {}, function(html){
                $('#cos-table-wrap').html(html);
                
                var count = $('#ps-table tbody tr').length;
                $('#user-cos-count').html(count);
            });
        },
                
        getCosEditForm: function(cosID)
        {
            $.post('<?= Url::link('ajax/cos/get.php') ?>', {cosID: cosID}, function(html){
                $('#edit-cos-form').html(html);
            });
        },
        
        cosSave: function ()
        {
            $('#cos-form-messages').empty();
            $.post('<?= Url::link('ajax/cos/edit.php') ?>', $('#edit-cos-form').serialize(), function(json){
                $('#cos-form-messages').html(json.messages);
                
                if (json.error != true)
                {
                    $('#edit-cos-form input[name=cosID]').val(json.cosID);
                    PageJS.getCosList();
                }
            }, 'json');
        },
                
        cosDelete: function(cosID)
        {
            $('#cos-table-messages').empty();
            $.post('<?= Url::link('ajax/cos/delete.php') ?>', {cosID: cosID}, function(json){
                if (json.error == true)
                {
                    $('#cos-table-messages').html(json.messages);
                }
                PageJS.getCosList();
            }, 'json');
        },
                
        cosAddProduct: function()
        {
            $('#add-product-to-cos-form .has-error').removeClass('has-error');
            var valid = true;
            
            var productID = $('#added-product').val();
            var quantity = $('#added-product-quntity').val();
            var productName = $('#added-product option:selected').text();
            var categoryName = $('select[name=CategoryID] option:selected').text();
            
            if (productID == undefined || parseInt(productID) == 0)
            {
                $('#added-product').closest('.form-group').addClass('has-error');
                valid = false;
            }
            
            if (quantity == '' || parseFloat(quantity) == 0)
            {
                $('#added-product-quntity').closest('.form-group').addClass('has-error');
                valid = false;
            }
            
            if (valid)
            {
                
                if ($('input[name="psID[' + productID + ']"]').length == 0)
                {
                    $('#added-product option[value!=0]').remove();
                    $('#added-product-quntity').val('');
                    $('select[name=CategoryID]').val(0);
                    
                    $('#cos-product-list table tbody').append('<tr><td><input type="hidden" name="psID[' + productID + ']" value="' + productID + '" />' + productName + '</td><td>' + categoryName + '</td><td class="text-center"><input type="hidden" name="psQuantity[' + productID + ']" value="' + quantity + '" />' + quantity + '</td><td><a onclick="$(this).closest(\'tr\').remove()" style="cursor:pointer;"><i class="fa fa-trash"></i></a></td></tr>');
                }
                else
                {
                    alert('Product allready added');
                }
            }
        }
        
    };
    
    
    var ImageUpload = {
    
        init: function()
        {
            $('body').on('change', 'input.ajax-image[type=file]', function(event){
                var files = event.target.files;
                
                var that = $(this);
                
                var data = new FormData();
                
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });
                
                $.ajax({
                    url: '<?= Url::link('ajax/cos/image-upload.php') ?>',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR)
                    {
                        $.each(data, function(key, val){
                            $('#image-wrap').html('<div><img style="width: 90px;" src="<?= Url::link('uploads/cos') ?>/' + val.thumb + '" /><input type="hidden" name="ImageID" value="' + val.id + '" /><a onclick="$(this).closest(\'div\').remove();"> Delete</a></div>');
                        });
                    }
                });
            });
        }
    
    };
    ImageUpload.init();
    
    
    $(document).ready(function(){
        PageJS.getCosList();
        
        $('body').on('click', '.edit-cos', function(){
            var cosId = $(this).attr('data-cos-id');
            PageJS.getCosEditForm(cosId);
        });
        
        $('#save-cos').click(function(){
            PageJS.cosSave();
        });
        
        $('body').on('click', '.delete-cos', function(){
            if (confirm('Confirm?'))
            {
                var cosID = $(this).attr('data-cos-id');
                PageJS.cosDelete(cosID);
            }
        });
        
        $('body').on('click', '#add-product-to-cos', function(){
            PageJS.cosAddProduct();
        });
    });

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/footer.php'; ?>