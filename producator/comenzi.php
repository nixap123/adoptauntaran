<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/check.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$order_service = new Order();

$orderList = $order_service->getProducatorOrderList($_SESSION['UserID']);

$monthOrderData = $order_service->getForMonthOrderChart();

$mustActiveUsers = $order_service->mustAcvtiveClients($_SESSION['UserID']);

$biggestStocks = $order_service->bigestProductStocks($_SESSION['UserID']);

?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/head.php'; ?>

<style>
    #biggest-stocks text[text-anchor="middle"], #must-active-users text[text-anchor="middle"] {
        opacity: 0;
    }
    text[text-anchor="middle"]{
        writing-mode: tb-rl;
    }
</style>

<script type="text/javascript">
    
    $(function () {
        new Morris.Bar({
            element: 'month-orders',
            data: <?= json_encode($monthOrderData) ?>,
            xkey: 'Month',
            ykeys: ['Amount'],
            labels: ['Suma lei'],
            barColors: ['#cc3333'],
            hideHover: true
        });
        var activeUsers = new Morris.Bar({
            element: 'must-active-users',
            data: <?= json_encode($mustActiveUsers) ?>,
            xkey: 'name',
            ykeys: ['quantity'],
            labels: ['Cantitatea'],
            barColors: ['#cc3333'],
            hideHover: true,
            resize: true,
            stacked: true,
            parseTime: false,
            hoverCallback: function(index, options, content) {
                return(content + '<br>' + '<a href="' + options.data[0].link +'">Vezi client</a>');
            }
        });
        var biggestStocks = new Morris.Bar({
            element: 'biggest-stocks',
            data: <?= json_encode($biggestStocks) ?>,
            xkey: 'product',
            ykeys: ['quantity'],
            labels: ['Cantitatea'],
            barColors: ['#cc3333'],
            hideHover: true,
            resize: true,
            stacked: true,
            parseTime: false
        });
    });

</script>

<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('Orders') ?></h2>
        <div class="row">
            <div class="col-md-4 text-center">
                <h4><?= Lang::t('TotalOrdersInLastMonth') ?></h4>
                <div id="month-orders" style="height: 200px;">
                    
                </div>
            </div>
            <div class="col-md-4 text-center">
                <h4><?= Lang::t('MostActiveUsers') ?></h4>
                <div id="must-active-users" style="height: 200px;">
                    
                </div>
            </div>
            <div class="col-md-4 text-center">
                <h4><?= Lang::t('LargestStocks') ?></h4>
                <div id="biggest-stocks" style="height: 200px;">
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 cart-inn">
                <h3 class="cart-title"><?= Lang::t('FinishedOrders') ?></h3>
                <table class="table table-striped dataTable">
                    <thead>
                        <tr>
                            <th class="text-center"><?= Lang::t('ID') ?></th>
                            <th><?= Lang::t('Client') ?></th>
                            <th><?= Lang::t('Phone') ?></th>
                            <th><?= Lang::t('Date') ?></th>
                            <th><?= Lang::t('Status') ?></th>
                            <th><?= Lang::t('TotalPrice') ?></th>
                            <th><?= Lang::t('TVA') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orderList as $order) { ?>
                        <tr order-id="<?= $order['ID'] ?>">
                            <td class="text-center"><?= $order['ID'] ?></td>
                            <td><?= $order['ClientName'] ?></td>
                            <td><?= $order['Phone'] ?></td>
                            <td><?= DateHelper::toDMY($order['Date']) ?></td>
                            <td><?= Order::orderDiliveryStatusSelect($order['DeliveryStatus']) ?></td>
                            <td><?= (float)$order['Amount'] ?> lei</td>
                            <td><?= $order['Amount'] * Config::$TVARate ?> lei</td>
                            <td><a target="_blank" href="<?= Url::link('reports/report.php', ['report' => 'producator-order', 'data' => ['orderID' => $order['ID']]]) ?>"><i class="fa fa-file-pdf-o"></i></a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>				
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>

<script>

$(document).ready(function(){
    
    $('select[name=DeliveryStatus]').change(function(){
        var status = $(this).val();
        var orderID = $(this).closest('tr').attr('order-id');
        
        $.post('<?= Url::link('ajax/order/status.php') ?>', {orderID: orderID, status: status}, function(){
            notification('<?= Lang::t('StatusChanged') ?>', 'success');
            $('tr[order-id=' + orderID + '] select[name=DeliveryStatus]').attr('disabled', 'disabled').val(1);
        });
    });
    
});

</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/footer.php'; ?>