<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
    
    if (empty($_SESSION['UserID']))
    {
        Url::redirect('login.php');
    }
    
    $user_service = new User();
    
    $USER = $user_service->getByID($_SESSION['UserID']);
    
    if ($USER['Type'] != User::TypeProducator)
    {
        Url::redirect();
    }