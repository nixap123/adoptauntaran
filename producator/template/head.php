<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php'; 
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php'; 
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Notification.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Order.php';

$user_service = new User();
$user_info_service = new UserInfo();
$image_service = new Image();
$cart_week_service = new CartWeek();
$notification_service = new Notification();
$order_service = new Order();

$USER = $user_service->getByID($_SESSION['UserID']);

if ($USER['Type'] != User::TypeProducator)
{
    Url::redirect();
}

if (isset($_SESSION['TempUserID']))
{
    $temp_user = $user_service->getByID($_SESSION['TempUserID']);
}

$USER_INFO = $user_info_service->getByUserID($USER['ID']);

$user_logo = $image_service->getByID($USER_INFO['Photo']);

$cartCount = $cart_week_service->getCartCount($USER['ID']);

$unread_notifications_count = $notification_service->getCountUnreaded($_SESSION['UserID']);

$order_count = $order_service->getProducatorOrderCount($_SESSION['UserID']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Adopta un taran</title>
        <meta charset="UTF-8" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/styles.css') ?>" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap-theme.min.css') ?>">
        
        <link rel="stylesheet" href="<?= Url::link('assets/js/fancybox/jquery.fancybox.css') ?>">

        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
        
        <link rel="stylesheet" href="<?= Url::link('assets/css/select2.min.css') ?>">

        <script src="<?= Url::link('assets/js/jquery-2.1.4.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/jquery.dataTables.min.js') ?>"></script>

        <script src="<?= Url::link('assets/js/bootstrap.min.js') ?>"></script>
        
        <script src="<?= Url::link('assets/js/main.js') ?>"></script>

        <link rel="stylesheet" type="text/css" href="<?= Url::link('assets/css/imgareaselect-default.css') ?>" />
        <script type="text/javascript" src="<?= Url::link('assets/js/jquery.imgareaselect.pack.js') ?>"></script>

        <!--[if lte IE 8]>
                <script src="<?= Url::link('assets/js/html5shiv.min.js') ?>"></script>
                <script src="<?= Url::link('assets/js/respond.min.js') ?>"></script>
        <![endif]-->

        <link rel="stylesheet" href="<?= Url::link('assets/css/bootstrap-datepicker3.min.css') ?>" />

        <link rel="stylesheet" href="<?= Url::link('assets/css/dataTables.min.css') ?>" />

        <script src="<?= Url::link('assets/js/jquery.nestable.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/bootstrap-datepicker.min.js') ?>"></script>
        <script src="<?= Url::link('assets/js/jquery.price_format.2.0.min.js') ?>"></script>
        
        <script src="<?= Url::link('assets/js/jquery.ui.widget.js') ?>"></script>
        <script src="<?= Url::link('assets/js/jquery.iframe-transport.js') ?>"></script>
        <script src="<?= Url::link('assets/js/jquery.fileupload.js') ?>"></script>
        <script src="<?= Url::link('assets/js/fancybox/jquery.fancybox.js') ?>"></script>
        <script src="<?= Url::link('assets/js/select2.min.js') ?>"></script>
        
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        
        <script type="text/javascript" src="<?= Url::link('assets/js/jquery.noty.packaged.min.js') ?>"></script>
        
        <script>
        function notification(text, type, timeout) {
            var n = noty({
                text        : text,
                type        : type,
                dismissQueue: true,
                timeout     : timeout == undefined ? 3000 : timeout,
                closeWith   : ['click'],
                layout      : 'topRight',
                theme       : 'defaultTheme',
                maxVisible  : 10
            });
        }
        </script>

    </head>
    <body class="admin-panel">
        
        <div class="cssload-container">
            <div class="cssload-whirlpool"></div>
        </div>
        
        <div class="temp-user-block">
            <div class="container">
                <span><?= Lang::t('You logged by') ?>: (<?= $USER['Type'] ?>) </span>
                <a class="go-back-btn" href="<?= Url::link('client/index.php') ?>"><i class="fa fa-external-link"></i> <?= Lang::t('ClientAccount') ?></a>
                <?php if (isset($_SESSION['TempUserID'])) { ?>
                <a class="go-back-btn" href="<?= Url::link('gal/account_back.php') ?>"><i class="fa fa-external-link"></i> <?= Lang::t('AdminPanel') ?></a>
                <?php } ?>
            </div>
        </div>

        <div class="top-bar ">
            <div class="container">
                <div class="row">
                    <div class="text-left donate-bar col-md-3">
                        <?= Lang::t('ManufacturerPanel') ?>
                    </div>
                    <div class="text-right user-bar col-md-9">
                        <a href="<?= Url::switchLang('ro') ?>"><img src="/assets/images/ro.png" /></a>
                        <a href="<?= Url::switchLang('en') ?>"><img src="/assets/images/en.png" /></a>&nbsp;&nbsp;&nbsp;
                        <a class="notification-menu" href="<?= Url::link('producator/notification.php') ?>"><span class="unread-notifications"><?= $unread_notifications_count ?></span> <?= Lang::t('Notification') ?></a>  
                        <a class="notification-menu" href="<?= Url::link('producator/feedback.php') ?>"><span class="unread-notifications"><?= $unread_notifications_count ?></span> <?= Lang::t('Feedback') ?></a>  
                        <a class="notification-menu" href="<?= Url::link('producator/gallery/index.php') ?>"><?= Lang::t('Gallery') ?></a>  
                        <a class="notification-menu" href="<?= Url::link('producator/news/index.php') ?>"><?= Lang::t('News') ?></a>  
                        <a class="notification-menu" href="<?= Url::link('producator/profile/index.php') ?>"><?= Lang::t('Profile') ?></a>  
                        <a class="notification-menu" href="<?= Url::link('producator/setari/index.php') ?>"><?= Lang::t('Settings') ?></a>  
                        <a href="<?= Url::link('logout.php') ?>"><?= Lang::t('Logout') ?></a>
                    </div>
                </div>
            </div>
        </div>

        <header class="header acc">
            <div class="container">
                <div class="row">
                    <div class="logo-bar col-md-2">
                        <a href="<?= Url::link('producator/index') ?>" title=""><img src="<?= Url::link('assets/images/logo.png') ?>" alt=""></a>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('producator/produse/index') ?>">
                                        <i class="fa fa-shopping-basket"></i>
                                        <strong><?= ProductStock::getProductStockCount($_SESSION['UserID']) ?></strong>
                                        <span><?= Lang::t('Products') ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('producator/cos/index') ?>">
                                        <i class="fa fa-shopping-cart"></i>
                                        <strong><?= $cartCount ?></strong>
                                        <span><?= Lang::t('WeekCarts') ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="acc-cart">
                                    <a href="<?= Url::link('producator/comenzi') ?>">
                                        <i class="fa fa-shopping-basket"></i>
                                        <strong><?= $order_count ?></strong>
                                        <span><?= Lang::t('Orders') ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="user-bar col-md-3">
                                <div class="acc-inn">
                                    <a id="acc-photo" class="acc-photo">
                                        <?php if (!empty($user_logo['ID'])) { ?>
                                        <img src="<?= Url::link('uploads/profile/' . $user_logo['Thumb']) ?>" />
                                        <?php } else { ?>
                                        <img src="<?= Url::link('assets/images/no_profile_img.jpg') ?>" />
                                        <?php } ?>
                                    </a>
                                    <input class="hidden" id="inputfile-profile" type="file" name="" />
                                    <div class="clearfix"></div>
                                    <div class="dropdown">
                                        <p id="open-profile" data-toggle="dropdown" role="button">
                                            <span><?= Lang::t('Welcome') ?>, <?= $USER_INFO['Name'] ?>!</span>
                                            <i class="fa fa-caret-down"></i>
                                        </p>
                                        <ul class="pull-right dropdown-menu" aria-labelledby="open-profile">
                                            <li><a href=""><i class="fa fa-cogs"></i> <?= Lang::t('Settings') ?></a></li>
                                            <li><a href="<?= Url::link('logout.php') ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="nat-patt"></div>
        </header>

        <?php if ($USER['Status'] == User::StatusInactive) { ?>
        <h2 class="container"><?= Lang::t('WaitConfirmation') ?></h2>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/producator/template/footer.php'; exit; ?>
        <?php } ?>