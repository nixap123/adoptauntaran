<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/News.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

$news_service = new News();

$news_list = $news_service->getListForEdit($_SESSION['UserID']);

if (isset($_GET['delID']))
{
    $news_service->delete([
        'UserID' => $_SESSION['UserID'],
        'ID' => (int)$_GET['delID']
    ]);
    
    Url::redirect('producator/news/index.php');
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/head.php';
?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title text-left"><?= Lang::t('News') ?></h2>

        <div>
            <a href="<?= Url::link('producator/news/add.php') ?>"><button class="btn btn-success"><i class="fa fa-plus"></i> <?= Lang::t('AddNews') ?></button></a>
        </div>
        <br />
        <div>
            <table class="table table-striped dataTable">
                <thead>
                    <tr>
                        <th><?= Lang::t('Name') ?></th>
                        <th><?= Lang::t('Description') ?></th>
                        <th><?= Lang::t('Date') ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($news_list as $row) { ?>
                <tr>
                    <td><?= $row['Title'] ?></td>
                    <td><?= mb_substr(strip_tags($row['Text']), 0, 80) ?>...</td>
                    <td><?= DateHelper::toDMY($row['Date']) ?></td>
                    <td>
                        <a href="<?= Url::link('producator/news/add', ['id' => $row['ID']]) ?>"><i class="fa fa-pencil blue"></i></a>&nbsp;
                        <a href="<?= Url::link('producator/news/index', ['delID' => $row['ID']]) ?>"><i class="fa fa-trash red"></i></a>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            
        </div>
    </div>
</section>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/producator/template/footer.php'; ?>