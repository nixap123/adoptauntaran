<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/CartWeek.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Setting.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/ProductStock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserPhoto.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/News.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserRating.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/DateHelper.php';

if (!isset($_GET['id']))
{
    Url::redirect();
}

$user_service = new User();
$user_info_service = new UserInfo();
$cart_week_service = new CartWeek();
$setting_service = new Setting();
$product_stock_service = new ProductStock();
$user_photo_service = new UserPhoto();
$news_service = new News();
$city_service = new City();

$user = $user_service->getByID($_GET['id']);

if (empty($user['ID']))
{
    Url::redirect();
}

if (isset($_SESSION['UserID']))
{
    $current_user = $user_service->getByID($_SESSION['UserID']);
}

$user_info = $user_info_service->getByUserID($user['ID']);

$user_place = $city_service->getByID($user_info['LocationID']);

$userGal = [];
if ($user['ParentID'] > 0)
{
    $userGal = $user_info_service->getByUserID($user['ParentID']);
}

$res = $setting_service->getWhere(['UserID' => 1, 'Name' => 'DescriptionLength']);
$settings = reset($res);
$descriptionLength = $settings['Value'];

$carts = $cart_week_service->getCurrentWeekCarts($user_info['UserID']);

$userProducts = $product_stock_service->getProductStocksForFrontend($user['ID']);

$userPhotos = $user_photo_service->getByUserID($user['ID']);

$userContacts = $user_info_service->getForFrontend($user['ID']);

$userNews = $news_service->getLastNews($user['ID'], 2);

$deliveryDays = $setting_service->getUserDeliveryDays($user['ID']);

if ($user['Type'] == User::TypeGal)
{
    $galUsers = $user_service->getGalProducatori($user['ID']);
}

$TITLE = $user_info['Name'];

if (!empty($current_user['ID']) && $current_user['Type'] == User::TypeClient)
{
    require_once $_SERVER['DOCUMENT_ROOT'] .'/client/template/head.php';
}
else
{
    require_once $_SERVER['DOCUMENT_ROOT'] .'/template/head.php';
}

?>

<section class="home-block">
    <div class="container">
        <div class="cart-messages">
            
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- Cose saptamanale -->
                <?php if (count($carts) > 0) { ?>
                <h2 class="text-left center-title margin-10"><?= Lang::t('ThisWeek') ?></h2>
                <?php foreach ($carts as $cart) { ?>
                <?php $cartData = reset($cart); ?>
                <div class="week-cart-block">
                    <div class="row">
                        <div class="col-md-2 week-cart-icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="col-md-5">
                            <strong><?= Lang::t('WeekCartContain') ?>:</strong>
                            <ul>
                                <?php foreach ($cart as $product) { ?>
                                <li><?= (float)$product['ProductQuantity'] ?> <?= Unit::getName($product['UnitID']) ?> <?= $product['ProductName'] ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-md-5 week-cart-price">
                            <span><?= (float)$cartData['Price'] ?> <sup>lei</sup></span>
                            <a onclick="<?= isset($_SESSION['UserID']) ? ("return Cart.add(" . $cartData['CartWeekID'] . ", 1, Cart.typeCart)") : "notification('Pentru a putea comanda va rugăm să vă înregistrați / logati.', 'warning', 10000)" ?>" data-cos-id="<?= $cartData['CartWeekID'] ?>" class=""><i class="fa fa-plus-circle"></i> adaugă în coș</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
                
                <!-- Gal producatori -->
                <?php if (!empty($galUsers)) { ?>
                <h2 class="text-left center-title margin-10"><?= Lang::t('Manufacturers') ?> <?= $user_info['Name'] ?></h2>
                <div class="row gal-producatori">
                    <?php foreach ($galUsers as $key => $producator) { ?>
                    <?php if (count($producator['Categories']) > 0) { ?>
                        <div class="profile-item col-md-4 col-sm-6 col-xs-12">
                            <a href="<?= Url::link($key . '-' . Url::strToUrl($producator['User']['Name'])) ?>">
                                <div class="profile-item-img">
                                    <span class="red-square"></span>
                                    <img src="<?= Url::link('assets/images/_taran.png') ?>" alt="">
                                </div>
                                <h3><?= $producator['User']['Name'] ?></h3>
                                <p><strong><?= Lang::t('Offer') ?>:</strong> <?= implode(', ', $producator['Categories']) ?></p>
                            </a>
                        </div>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>
                
                <!-- Produse -->
                <?php if (count($userProducts) > 0) { ?>
                <h2 class="text-left center-title margin-10"><?= Lang::t('Products') ?> <?= $user_info['Name'] ?></h2>
                <div class="profile-products">
                    <ul>
                        <?php foreach ($userProducts as $catID => $row) { ?>
                        <?php $category = reset($row); ?>
                        <li><a data-toggle="collapse" href="#cat-<?= $catID ?>"><i class="fa fa-plus-circle"></i> <?= $category['CategoryName'] ?></a>
                            <ul class="collapse" id="cat-<?= $catID ?>">
                                <?php foreach ($row as $product) { ?>
                                <li data-ps-id="<?= $product['psID'] ?>" class="product-item">
                                    <strong><?= $product['ProductName'] ?></strong>
                                    <span><?= (float)$product['Price'] ?> lei / <?= Unit::getName($product['UnitID']) ?></span>
                                    <input type="number" min="1" max="<?= $product['Sold'] ?>" step="1" value="1" name="product-quantity">
                                    <a onclick="<?= isset($_SESSION['UserID']) ? ("return Cart.add(" . $product['psID'] . ", $(this).closest('.product-item').find('input[name=product-quantity]').val(), Cart.typeProduct)") : "notification('Pentru a putea comanda va rugăm să vă înregistrați / logati.', 'warning', 10000)" ?>" href="#"><i class="fa fa-plus-circle"></i></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
                
                <!-- Livrare -->
                <div>
                    <?php if (!empty($deliveryDays['AmLivrare'])) { ?>
                    <?php unset($deliveryDays['AmLivrare']); ?>
                    <h2 class="text-left center-title margin-10"><?= Lang::t('DeliveryDays') ?></h2>
                    <table class="table">
                        <?php foreach ($deliveryDays as $label => $cities) { ?>
                        <tr><td><strong><?= Lang::t($label) ?>:</strong></td><td><?= $cities ?></td></tr>
                        <?php } ?>
                    </table>
                    <?php } ?>
                </div>
                
                <!-- Rating -->
                <div>
                    <?= UserRating::ratingWidget($user['ID'], true, $user_info['Name']) ?>
                </div>
            </div>
            <div class="profile-desc col-md-6">
                <!-- User Description -->
                <h2 class="text-left center-title margin-10">Despre <?= $user_info['Name'] ?></h2>
                <?php if ($userGal['UserID'] > 0) { ?>
                <p><a href="<?= Url::link($userGal['UserID'] . '-' . Url::strToUrl($userGal['Name'])) ?>"><?= Lang::t('BelongToGal') ?> #<?= $userGal['Name'] ?></a></p>
                <?php } ?>
                <p id="user-description"></p>
                <a id="user-description-toggle" href="#"><?= Lang::t('ViewMore') ?></a>
                
                <!-- Galerie foto -->
                <?php if (!empty($userPhotos)) { ?>
                <h2 class="text-left center-title margin-10"><?= Lang::t('PhotoGallery') ?> <?= $user_info['Name'] ?></h2>
                <div class="profile-gallery">
                    <div class="row">
                        <?php foreach ($userPhotos as $photo) { ?>
                        <div class="col-md-3">
                            <a rel="user-gallery" class="lightbox" href="<?= Url::link('uploads/gallery/' . $photo['Image']) ?>">
                                <img src="<?= Url::link('uploads/gallery/' . $photo['Thumb']) ?>">
                            </a>
                        </div>
                        <?php } ?>
                    </div>		
                </div>
                <?php } ?>
                
                <!-- Contatcte -->
                <h2 class="text-left center-title margin-10">Contact <?= $user_info['Name'] ?></h2>
                <address class="profile-contacts">
                    <p><i class="fa fa-location-arrow"></i> <?= $userContacts['Address'] ?>, <?= $userContacts['CityName'] ?> jud. <?= $userContacts['JudetName'] ?> (<a style="cursor: pointer;" data-toggle="modal" data-target="#producator-place-modal"><?= Lang::t('ViewMap') ?></a>)</p>
                    <p><i class="fa fa-phone"></i> <?= $userContacts['Phone'] ?></p>
                    <p><i class="fa fa-envelope"></i> <a data-toggle="modal" data-target="#contact-producator-modal" href="#"><?= Lang::t('SendAMessageFor') ?> <?= $user_info['Name'] ?></a></p>
                </address>
                
                <!-- Noutati -->
                <?php if (count($userNews) > 0) { ?>
                <h2 class="text-left center-title margin-10"><?= Lang::t('NewsFrom') ?> <?= $user_info['Name'] ?></h2>
                <div class="profile-news">
                    <?php foreach ($userNews as $news) { ?>
                    <article>
                        <a href="<?= Url::link('blog/' . $news['ID'] . '-' . Url::strToUrl($news['Title'])) ?>"><?= $news['Title'] ?></a>
                        <time>Adăugat de <?= $user_info['Name'] ?> pe <?= DateHelper::toDMY($news['Date']) ?></time>
                    </article>
                    <?php } ?>
                    <a href="<?= Url::link('blog/user/' . $user['ID'] . '-' . Url::strToUrl($user_info['Name'])) ?>"><?= Lang::t('ViewMore') ?></a>
                </div>
                <?php } ?>
            </div>
        </div>
        <div>
            <a href="<?= Url::link(isset($_SESSION['UserID']) ? 'client/producatori.php' : '#producatori') ?>" class="arrows-btn">
                <?= Lang::t('ViewAll') ?><br />
                <?= Lang::t('Manufacturers') ?><br />
                <?= Lang::t('FromZone') ?>
            </a>
        </div>


        
    </div>
</section>

<div id="contact-producator-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= Lang::t('ContactManufacturer') ?>
            </div>
            <div class="modal-body">
                <form id="contact-producator-form">
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Name') ?></label>
                        <input required type="text" name="Name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Email') ?></label>
                        <input required type="email" name="Email" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Phone') ?></label>
                        <input type="text" name="Phone" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= Lang::t('Message') ?></label>
                        <textarea required style="min-height: 100px;" name="Message" class="form-control"></textarea>
                    </div>
                    <div>
                        <input type="hidden" name="ReceiverID" value="<?= $user['ID'] ?>" />
                        <input type="submit" class="hidden submit" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Lang::t('Close') ?></button>
                <button onclick="$('#contact-producator-form .submit').click()" type="button" class="btn btn-primary"><?= Lang::t('Send') ?></button>
            </div>
        </div>
    </div>
</div>

<div id="producator-place-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= Lang::t('ManufacturerOnMap') ?>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="500px" src="http://maps.google.com/maps?q=<?= (float)$user_place['Lat'] ?>,<?= (float)$user_place['Long'] ?>&z=12&output=embed"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Lang::t('Close') ?></button>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {
        
        var longString = '<?= $user_info['Description'] ?>';
        var contentDiv = $("#user-description");
        var contentDivToggle = $("#user-description-toggle");
        contentDiv.html(longString.substr(0, <?= (int)$descriptionLength ?>) + "...");
        
        contentDivToggle.click(function (e){
            e.preventDefault();
            if ($(this).hasClass('open'))
            {
                $(this).removeClass('open');
                contentDiv.html(longString.substr(0, <?= (int)$descriptionLength ?>) + "...");
            }
            else
            {
                $(this).addClass('open');
                contentDiv.html(longString);
            }
        });
        
        $('a.lightbox').fancybox();
        
        $('#contact-producator-form').submit(function(e){
            e.preventDefault();
            
            $.post('<?= Url::link('ajax/feedback/send.php') ?>', $('#contact-producator-form').serialize(), function(){
                $('#contact-producator-form').trigger('reset');
                $('#contact-producator-modal').modal('hide');
            });
        });
        
    });

</script>

<?php 

if (!empty($current_user['ID']) && $current_user['Type'] == User::TypeClient)
{
    require_once $_SERVER['DOCUMENT_ROOT'] .'/client/template/footer.php';
}
else
{
    require_once $_SERVER['DOCUMENT_ROOT'] .'/template/footer.php';
}

?>