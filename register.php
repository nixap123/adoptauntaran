<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Judet.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Bank.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Image.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Notification.php';

$judet_service = new Judet();
$bank_service = new Bank();
$user_info_service = new UserInfo();
$image_service = new Image();
$notification_service = new Notification();

$judet_list = $judet_service->getAll();
$banks_list = $bank_service->getAll();

$judets = [];
foreach ($judet_list as $judet) {
    $judets[$judet['ID']] = $judet['Name'];
}

$banks = [];
foreach ($banks_list as $bank) {
    $banks[$bank['ID']] = $bank['Name'];
}

if (Request::isPost()) {
    $Email = Validator::validate('Email', Validator::ValidateEmail, 'Email');
    $Password = Validator::validate('Password', Validator::ValidateEmpty, 'Password');

    $ClientType = Validator::validate('ClientType', Validator::ValidateEmpty, 'ClientType');
    $AccountType = Validator::validate('AccountType', Validator::ValidateEmpty, 'InteresedIn');
    $Name = Validator::validate('Name', Validator::ValidateEmpty, 'Name');
    $Phone = Validator::validate('Phone', Validator::ValidateEmpty, 'Phone');

    $Address = Validator::validate('Address', Validator::ValidateEmpty, 'Address');
    $Judet = Validator::validate('Judet', Validator::ValidateEmpty, 'County');
    $Location = Validator::validate('Location', Validator::ValidateEmpty, 'Location');
    $PasswordConfirmation = Validator::validate('PasswordConfirmation', Validator::ValidateEmpty, 'PasswordConfirmation');
    $Accept = Validator::validate('Accept', Validator::ValidateEmpty, 'Accept');
    $FirmName = '';
    $VATPayer = Request::post('VATPayer') == 1 ? 1 : 0;

    if ($ClientType == User::ClientTypePF) {
        $Cnp = Validator::validate('Cnp', Validator::ValidateEmpty, 'CNP');
    } else {
        $Cui = Validator::validate('Cui', Validator::ValidateEmpty, 'CUI/CIF');
        $BankAccount = Validator::validate('BankAccount', Validator::ValidateEmpty, 'BankAccount');
        $BankID = Validator::validate('BankID', Validator::ValidateNumber, 'Bank');
        $RegNumber = Validator::validate('RegNumber', Validator::ValidateEmpty, 'CompanyRegistrationNumber');
        $FirmName = Validator::validate('FirmName', Validator::ValidateEmpty, 'CompanyName');
    }

    if (!Validator::hasErrors()) {
        Validator::validate_equal('Password', 'PasswordConfirmation', 'Password', 'PasswordConfirmation');

        if (!Validator::hasErrors()) {
            $user_service = new User();

            $user = $user_service->getWhere(['UserName' => $Email, 'Status' => User::StatusActive], 1);

            if (isset($user[0]['ID'])) {
                Validator::setError(Lang::t('EmailIsAlreadyRegistered'));
            }
        }

        if (!Validator::hasErrors()) {
            $Photo = Upload::file('Photo', 'uploads/profile', ['jpg', 'png']);

            $imgID = 0;
            if (!empty($Photo)) {
                $source = $_SERVER['DOCUMENT_ROOT'] . '/uploads/profile/';

                $originalFile = $source . $Photo;
                $thumb = $source . 'thumb_' . $Photo;

                $thumb = Upload::resize(300, $thumb, $originalFile);
                $image = Upload::resize(1200, $originalFile, $originalFile);

                $imgID = $image_service->insert([
                    'Thumb' => 'thumb_' . $Photo,
                    'Image' => $Photo
                ]);
            }

            $userID = $user_service->insert([
                'UserName' => $Email,
                'Password' => $Password,
                'Type' => $AccountType,
                'ClientType' => $ClientType,
                'Status' => $AccountType == User::TypeGal || $AccountType == User::TypeProducator ? User::StatusInactive : User::StatusActive,
                'RegDate' => date('c')
            ]);

            $user_info_service->insert([
                'UserID' => $userID,
                'Name' => $Name,
                'Phone' => $Phone,
                'Photo' => $imgID,
                'Address' => $Address,
                'JudetID' => $Judet,
                'LocationID' => $Location,
                'CNP' => $Cnp,
                'CUI' => $Cui,
                'RegNumber' => $RegNumber,
                'BankAccount' => $BankAccount,
                'BankID' => $BankID,
                'FirmName' => $FirmName,
                'VATPayer' => $VATPayer
            ]);

            if ($AccountType == User::TypeGal || $AccountType == User::TypeProducator) {
                $notification_service->send(1, 'New ' . $AccountType . ' Registration: ' . $Name, '', Notification::TypeSuccess, Notification::CategoryUser);
                Url::redirect('locations.php', ['id' => $userID]);
            } else {
                $_SESSION['UserID'] = $userID;
                Url::redirect('client/index');
            }
        }
    }
}
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/head.php'; ?>


<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('RegisterOnTheSite') ?></h2>
        <div class="row">
            <form id="login-form" class="register-page login-form col-md-12 form-horizontal" method="post" enctype="multipart/form-data">

                <?= Validator::showMessages() ?>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('ClientType') ?></label>
                                <div class="col-md-8 text-center">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>
                                                <input  required onclick="Register.setFormPF()" <?= Request::post('ClientType') == 'PF' ? 'checked' : '' ?> type="radio" name="ClientType" value="PF"  /> 
                                                <span class="login-lbl"><?= Lang::t('Individual') ?></span>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label>
                                                <input required onclick="Register.setFormPJ()" <?= Request::post('ClientType') == 'PJ' ? 'checked' : '' ?> type="radio" name="ClientType" value="PJ"  /> 
                                                <span class="login-lbl"><?= Lang::t('Legal') ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label id="name-label" class="col-md-4 control-label"><?= Lang::t('Name') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="Name" value="<?= Request::post('Name') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div id="firm-name-control" class="form-group">
                            <div class="row">
                                <label id="name-label" class="col-md-4 control-label"><?= Lang::t('CompanyName') ?></label>
                                <div class="col-md-8">
                                    <input type="text" name="FirmName" value="<?= Request::post('FirmName') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Email') ?></label>
                                <div class="col-md-8">
                                    <input required type="email" name="Email" value="<?= Request::post('Email') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Phone') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="Phone" value="<?= Request::post('Phone') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div id="cnp" class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">CNP</label>
                                <div class="col-md-8">
                                    <input required type="text" name="Cnp" value="<?= Request::post('Cnp') ?>" />
                                </div>
                            </div>
                        </div>
                        <div id="cui" class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">CUI / CIF</label>
                                <div class="col-md-8">
                                    <input type="text" name="Cui" value="<?= Request::post('Cui') ?>" />
                                </div>
                            </div>
                        </div>
                        <div id="reg_number" class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('CompanyRegistrationNumber') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="RegNumber" value="<?= Request::post('RegNumber') ?>" />
                                </div>
                            </div>
                        </div>
                        <div id="cont_bancar" class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('BankAccount') ?></label>
                                <div class="col-md-8">
                                    <input  type="text" name="BankAccount"  />
                                </div>
                            </div>
                        </div>
                        <div id="banka" class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Bank') ?></label>
                                <div class="col-md-8">
                                <?= Html::form_dropdown('BankID', $banks, '', '') ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('ProfileImage') ?></label>
                                <div class="col-md-8">
                                    <input required accept=".png,.jpg,.jpeg" type="file" name="Photo" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('InteresedIn') ?></label>
                                <div class="col-md-8 text-center">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>
                                                <input required type="radio" name="AccountType" <?= Request::post('AccountType') == 'Producator' ? 'checked' : '' ?> value="Producator"  />
                                                <span class="login-lbl"><?= Lang::t('SellProducts') ?></span>

                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label>
                                                <input  required type="radio" name="AccountType" <?= Request::post('AccountType') == 'Client' ? 'checked' : '' ?> value="Client" />
                                                <span class="login-lbl"><?= Lang::t('BuyProducts') ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('PayerTo') ?> TVA</label>
                                <div class="col-md-8">
                                    <input style="width: auto; position: relative; top: 7px;" type="checkbox" name="VATPayer" <?= Request::post('VATPayer') == 1 ? 'checked' : '' ?> value="1"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Address') ?></label>
                                <div class="col-md-8">
                                    <input required type="text" name="Address" value="<?= Request::post('Address') ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('County') ?></label>
                                <div class="col-md-8">
                                <?= Html::form_dropdown('Judet', [0 => '- ' . Lang::t('SelectCounty') . '  -'] + $judets, '', 'onchange="Register.judetChange(this)"') ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Location') ?></label>
                                <div class="col-md-8">
                                <?= Html::form_dropdown('Location', ['0' => '- '. Lang::t('SelectLocation') . ' -'], '', '') ?>
                                </div>
                            </div>
                        </div>
                        <div id="pers_contact" class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('ContactPerson') ?></label>
                                <div class="col-md-8">
                                    <input  type="text" name=""  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('Password') ?></label>
                                <div class="col-md-8">
                                    <input required type="password" name="Password"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label"><?= Lang::t('ConfirmPassword') ?></label>
                                <div class="col-md-8">
                                    <input required type="password" name="PasswordConfirmation"  />
                                </div>
                            </div>
                        </div>
                        <div class="form-group accept-check">
                            <div class="row">
                                <label for="accept-check" class="col-md-11 control-label"><?= Lang::t('TermsAccept') ?></label>
                                <div class="col-md-1">
                                    <input id="accept-check" required type="checkbox" name="Accept"  />
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <button type="submit" class="arrows-btn">
                    <?= Lang::t('Create') ?><br />
                    <?= Lang::t('ANewAccount') ?>
                </button>

                <div class="form-group text-center">						
                    <p><a href="<?= Url::link('termeni') ?>"><?= Lang::t('TermsAndCondition') ?></a></p>			
                    <p><a href="<?= Url::link('confidentialitate') ?>"><?= Lang::t('PrivacyPolicy') ?></a></p>
                </div>
            </form>
        </div>
    </div>
</section>

<script>

    var Register = {
        setFormPF: function ()
        {
            $('#cnp').show().find('input').attr('required', 'required');
            $('#cui, #reg_number, #banka, #cont_bancar, #pers_contact').hide().find('input').removeAttr('required');
            $('#name-label').text('Nume / prenume');
            $('#firm-name-control').hide();
        },
        setFormPJ: function ()
        {
            $('#cnp').hide().find('input').removeAttr('required');
            $('#cui, #reg_number, #banka, #cont_bancar, #pers_contact').show();
            $('#cui input').attr('required', 'required');
            $('#name-label').text('Nume / prenume delegat');
            $('#firm-name-control').show();
        },
        judetChange: function (select)
        {
            $.post('<?= Url::link('ajax/city-by-judet.php') ?>', {JudetID: $(select).val()}, function (html) {
                $('select[name=Location]').html(html);
            });
        }

    };

<?= Request::post('ClientType') == 'PJ' ? '$("input[name=ClientType]).click()' : 'Register.setFormPF();' ?>

</script>
<style>
    #cnp, #cui, #reg_number {display: none;}
</style>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'; ?>