<?php
require_once 'core/core.php';
require_once 'services/Page.php';
require_once 'services/PageLang.php';
require_once 'services/Donate.php';
require_once 'services/Notification.php';

$notification_service = new Notification();

$donate_service = new Donate();

if (isset($_GET['cancel']))
{
    $id = (int)$_GET['cancel'];
    
    $donate_service->update([
        'Status' => Donate::StatusCanceled
    ], [
        'ID' => $id
    ]);
    
    $_SESSION['payment'] = 0;
    Url::redirect('doneaza.php');
}

if (isset($_GET['success']))
{
    $id = (int)$_GET['success'];
    
    $donate = $donate_service->getByID($id);
    
    if (empty($donate['ID']) || $donate['Status'] !== Donate::StatusPending)
    {
        $_SESSION['payment'] = 0;
        Url::redirect('doneaza.php');
    }
    
    $donate_service->update([
        'Status' => Donate::StatusPaid
    ], [
        'ID' => $id
    ]);
    
    $str = 'Donație de la: ' . $donate['Name'];
    $text = 'Suma: ' . $donate['Amount'];
    
    $notification_service->send(1, $str, $text, Notification::TypeInfo, Notification::CategoryUser);
    
    $_SESSION['payment'] = 1;
    $_SESSION['name'] = $donate['Name'];
    Url::redirect('doneaza.php');
}

$TITLE = Lang::t('DonateForThisProject');

require_once 'template/head.php';
?>



<section class="home-block">
    <div class="container">
        <h2 class="center-title"><?= Lang::t('DonateForProject') ?></h2>
        <div class="row">
            <form id="donate-form" class="login-form col-md-7 col-sm-10 col-xs-10 form-horizontal" method="post">
                
                <?php if (isset($_SESSION['payment'])) { ?>
                    <?php if ($_SESSION['payment'] == 1) { ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="<?= Lang::t('Close') ?>"><span aria-hidden="true">&times;</span></button>
                        <div><?= Lang::t('Thanks') ?>, <?= $_SESSION['name'] ?>!</div>
                    </div>
                    <?php } else { ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="<?= Lang::t('Close') ?>"><span aria-hidden="true">&times;</span></button>
                        <div><?= Lang::t('Canceled') ?>!</div>
                    </div>
                    <?php } ?>
                    <?php unset($_SESSION['payment']) ?>
                    <?php unset($_SESSION['name']) ?>
                <?php } ?>

                <div id="donate-error"></div>

                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('Name') ?></label>
                    <div class="col-md-9">
                        <input required type="text" name="Name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('Address') ?></label>
                    <div class="col-md-9">
                        <input type="text" name="Address" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('Phone') ?></label>
                    <div class="col-md-9">
                        <input type="text" name="Phone" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('Email') ?></label>
                    <div class="col-md-9">
                        <input required type="email" name="Email" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('DonateAmount') ?>:</label>
                    <div class="col-md-9">
                        <input required min="1" step="1" type="number" name="Amount" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?= Lang::t('PaymentMethod') ?></label>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <label class="fake-radio">
                                    <input required type="radio" name="PaymentMode" value="paypal" />  
                                    <img class="login-lbl img-responsive" src="/assets/images/paypal.png" />
                                </label>
                                
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <label class="fake-radio">
                                    <input type="radio" name="PaymentMode" value="card" />
                                    <img class="login-lbl img-responsive" src="/assets/images/card.png" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5 control-label"><?= Lang::t('ForBankTransfer') ?>:</label>
                    <div class="col-md-7 ">
                        <span>
                            Asociatia Crestem Romania Impreuna<br />
                            str. Carpatilor nr. 60, Brasov,<br />
                            CIF: 34521305<br />
                            Ro18 FNNB 0029 0309 8016 RO02<br />
                            Credit Europe Bank Brasov<br />
                        </span>
                    </div>
                </div>
                <div>
                    <button type="submit" class="arrows-btn">
                        <?= Lang::t('Donate') ?><br />
                        <?= Lang::t('Now') ?>!
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>

<script>

    $(document).ready(function(){
        $('#donate-form').submit(function(event){
            event.preventDefault();

            $.post('<?= Url::link('ajax/payment/donate.php') ?>', $('#donate-form').serialize(), function(json){
                if (json.success === true)
                {
                    var url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_donations&business=<?= urlencode(Config::$PaypalEmail) ?>&lc=RO&item_name=Adopta%20un%20taran&item_number=' + json.result.id + '&amount=' + encodeURI(json.result.amount) + '&currency_code=EUR&no_note=1&no_shipping=1&rm=1&return=<?= urlencode(Config::$BaseUrl) ?>doneaza?success%3d' + json.result.id + '&id%3d' + json.result.id + '&cancel_return=<?= urlencode(Config::$BaseUrl) ?>doneaza%3fcancel%3d' + json.result.id + '&id%3d' + json.result.id + '&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted';
                    window.location.href = url;
                }
                else
                {
                    $('#donate-error').html(json.errors);
                }
            }, 'json');
        });
    });

</script>


<?php
require_once 'template/footer.php';
?>


