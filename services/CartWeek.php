<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class CartWeek extends Database
{
    
    const StatusActive = 'Active';
    const StatusInactive = 'Inactive';

    
    public function getTableName()
    {
        return 'CartWeek';
    }

    public function getForEdit($cosID, $userID)
    {
        $cosID = (int)$cosID;
        
        $res = $this->query("
            select p.ID as ProductID, i.Thumb as Image, ps.ID as psID, p.CategoryID, pl.Name as ProductName, cl.Name as CategoryName, cwp.Quantity as ProductQuantity, cwp.Price as ProductPrice, cwp.ID as CartWeekProductID, cw.ID as CartWeekID, cw.Price as CartPrice, cw.Quantity as CartQuantity, cw.ExpirationDate as CartDate, cw.StartDate, cw.Regular, cw.Photo, cw.`Status` from CartWeek as cw 
            left join CartWeekProduct as cwp on cwp.CartWeekID = cw.ID 
            left join ProductStock as ps on ps.ID = cwp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID() . " 
            left join Image as i on i.ID = cw.Photo 
            where cw.ID = $cosID and cw.UserID = $userID 
        ");
        
        $result = [];
        foreach ($res as $row)
        {
            $result[$row['psID']] = $row;
        }
        
        return $result;
    }
    
    public function getListByUserID($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("
            select p.ID as ProductID, p.CategoryID, p.UnitID, pl.Name as ProductName, cl.Name as CategoryName, cwp.Quantity as ProductQuantity, cwp.Price as ProductPrice, cwp.ID as CartWeekProductID, cw.ID as CartWeekID, cw.Price as CartPrice, cw.Quantity as CartQuantity, cw.ExpirationDate as CartDate, cw.Created as CartCreatedDate, cw.Regular, cw.Photo, cw.`Status` from CartWeek as cw 
            left join CartWeekProduct as cwp on cwp.CartWeekID = cw.ID 
            left join ProductStock as ps on ps.ID = cwp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID() . " 
            where cw.UserID = $userID
        ");
        
        $result = [];
        foreach ($res as $row)
        {
            $result[$row['CartWeekID']][] = $row;
        }
        
        return $result;
    }
    
    public function getCartCount($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("select count(*) as count from `CartWeek` where UserID = $userID");
        $res = reset($res);
        return $res['count'];
    }
    
    public function getCurrentWeekCarts($userID)
    {
        $userID = (int)$userID;
        
        $carts = $this->query("
            select cw.ID as CartWeekID, cw.Photo, cw.Price, cwp.Quantity as ProductQuantity, pl.Name as ProductName, p.UnitID from CartWeek as cw 
            left join CartWeekProduct as cwp on cwp.CartWeekID = cw.ID 
            left join ProductStock as ps on ps.ID = cwp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            where cw.StartDate <= date(now()) and cw.ExpirationDate >= date(now()) and cw.UserID = $userID 
        ");
        
        $return = [];
        foreach ($carts as $cart)
        {
            $return[$cart['CartWeekID']][] = $cart;
        }
        
        return $return;
    }
    
    public function getCurrentWeekCartsByUserIDs($userIDs, $limit = 1)
    {
        $limit = (int)$limit;
        
        if (empty($userIDs))
        {
            $userIDs = [0];
        }
        
        $carts = $this->query("
            select ui.Name as UserName, i.Image, u.ID as UserID, cw.ID as CartWeekID, cw.Photo, cw.Price, cwp.Quantity as ProductQuantity, pl.Name as ProductName, p.UnitID from CartWeek as cw 
            left join CartWeekProduct as cwp on cwp.CartWeekID = cw.ID 
            left join ProductStock as ps on ps.ID = cwp.ProductStockID 
            left join User as u on u.ID = ps.UserID 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Image as i on i.ID = cw.Photo 
            left join Product as p on p.ID = ps.ProductID
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            where cw.StartDate <= date(now()) and cw.ExpirationDate >= date(now()) and cw.UserID in (" . implode(', ', $userIDs) . ") 
            order by rand()
        ");
        
        $return = [];
        foreach ($carts as $cart)
        {
            $return[$cart['CartWeekID']][] = $cart;
        }
        
        return $return;
    }
    
}