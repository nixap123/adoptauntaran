<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class CartWeekProduct extends Database
{
    
    public function getTableName()
    {
        return 'CartWeekProduct';
    }
    
    public function getByCartID($cartID)
    {
        $cartID = (int)$cartID;
        
        $res = $this->query("
            select ps.ID as psID, pl.Name as ProductName, ps.Quantity - ps.OrderedQuantity as Sold, p.UnitID, cwp.Price, cl.Name as CategoryName, cwp.Quantity from CartWeekProduct as cwp 
            left join ProductStock as ps on ps.ID = cwp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID() . " 
            where cwp.CartWeekID = $cartID
        ");
        
        $result = [];
        foreach ($res as $row)
        {
            $result[$row['psID']] = $row;
        }
        
        return $result;
    }
    
}