<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';

class Unit
{
    
    public static function unitSelect($selected = false, $langID = 1)
    {
        $langID = Request::getLangID();
        
        $res = Config::$UnitList;
        
        $data = [];
        foreach ($res as $unitID => $row)
        {
            $data[] = [
                'ID' => $unitID,
                'Name' => $row[$langID]
            ];
        }
        
        $data = ArrayHelper::keyVal($data, 'ID', 'Name');
        return Html::form_dropdown('UnitID', $data, $selected, 'class="form-control"');
    }
    
    public static function getName($unitID, $langID = 1)
    {
        $langID = Request::getLangID();
        return Config::$UnitList[$unitID][$langID];
    }
    
}