<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class UserRating extends Database
{
    
    public function getTableName()
    {
        return 'UserRating';
    }
    
    public static function ratingWidget($userID, $full = false, $name = '')
    {
        if (empty($_SESSION['UserID'])) return;
        
        if ($full)
        {
            $return = '<h2 class="text-left center-title margin-10">Rating ' . $name . '</h2>';
        }
        
        $userID = (int)$userID;
        
        $_this = new UserRating();
        
        $res = $_this->query("select floor(avg(Rating)) as `Int`, (round(avg(Rating) * 2) / 2) % 1 as `Dec`, count(*) as `Count`, round(avg(Rating), 1) as Total from UserRating where UserID = $userID group by UserID");
        
        if ($res)
        {
            $rating = reset($res);
        }
        else
        {
            $rating = ['Int' => 0, 'Dec' => 0, 'Count' => 0, 'Total' => 0];
        }
        
        $return .= "<div class=\"rating-widget\"><div class=\"profile-rating\">";
        for ($i = 1; $i <= $rating['Int']; $i++)
        {
            $return .= "<i onclick=\"Rating.set($i, $userID, this)\" class=\"fa fa-star\"></i>";
        }
        
        $half = false;
        if ($rating['Dec'] == 0.5 && $rating['Int'] != 5)
        {
            $rating['Int']++;
            $return .= "<i onclick=\"Rating.set(" . $rating['Int'] . ", $userID, this)\" class=\"fa fa-star-half-o\"></i>";
        }
        
        $i = $rating['Int'];
        while ($i < 5)
        {
            $i++;
            $return .= "<i onclick=\"Rating.set($i, $userID, this)\" class=\"fa fa-star-o\"></i>";
        }
        
        $return .= "&nbsp;<strong>" . number_format($rating['Total'], 1) . " <small>(" . $rating['Count'] . " voturi)</small></strong>";
        
        $return .= "</div></div>";
        
        return $return;
    }
    
}