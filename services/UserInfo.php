<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class UserInfo extends Database
{

    public function getTableName()
    {
        return 'UserInfo';
    }
    
    public function getByUserID($id)
    {
        $ret = $this->getWhere(['UserID' => $id], 1);
        return reset($ret);
    }
    
    public function getForFrontend($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("
            select ui.Address, c.Name as CityName, j.Name as JudetName, c.`Long`, c.Lat, ui.Phone, u.UserName from User as u
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Judet as j on j.ID = ui.JudetID 
            left join City as c on c.ID = ui.LocationID 
            where u.ID = $userID
        ");
        
        $return = reset($res);
        
        return $return;
    }
    
    public function getForReport($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("
            select 
            ui.Address, 
            c.Name as CityName, 
            j.Name as JudetName, 
            ui.Phone, 
            ui.CUI, 
            ui.Name,
            ui.RegNumber, 
            b.Name as BankName,
            ui.BankAccount,
            ui.VATPayer 
            from User as u
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Judet as j on j.ID = ui.JudetID 
            left join City as c on c.ID = ui.LocationID 
            left join Bank as b on b.ID = ui.BankID 
            where u.ID = $userID
        ");
        
        $return = reset($res);
        
        return $return;
    }
    
}