<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class News extends Database
{
    
    public function getTableName()
    {
        return 'News';
    }
    
    public function getListForEdit($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("select * from `News` as n left join `NewsLang` as nl on nl.NewsID = n.ID and nl.LangID = " . Request::getLangID() . " where n.UserID = $userID order by Date desc");
        
        return $res;
    }
    
    public function getLastNews($userID, $limit = 5)
    {
        if (is_array($userID))
        {
            $ids_str = implode(',', $userID);
        }
        else
        {
           $ids_str = (int)$userID;
        }
        
        if ($limit)
        {
            $limit = 'limit ' . (int)$limit;
        }
        else
        {
            $limit = '';
        }
        
        $res = $this->query("select * from `News` as n left join `NewsLang` as nl on nl.NewsID = n.ID and nl.LangID = " . Request::getLangID() . " where n.UserID in ($ids_str) order by Date desc $limit");
        
        return $res;
    }
    
    public function getByID($ID)
    {
        $ID = (int)$ID;
        $res = $this->query("select * from `News` as n left join `NewsLang` as nl on nl.NewsID = n.ID and nl.LangID = " . Request::getLangID() . " where n.ID = $ID limit 1");
        
        return reset($res);
    }
    
    public function getByUserID($ID)
    {
        $ID = (int)$ID;
        $res = $this->query(""
                . "select * from `News` as n "
                . "left join `NewsLang` as nl on nl.NewsID = n.ID and nl.LangID = " . Request::getLangID() . " "
                . "where n.UserID = $ID "
                . "order by n.`Date` desc"
                . "");
        
        return $res;
    }
    
}