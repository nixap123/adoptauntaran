<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class OrderCart extends Database
{
    
    const TypeCart = 'CartWeek';
    const TypeProduct = 'Product';
    
    const StatusActive = 'Active';
    const StatusPaused = 'Paused';
    const StatusInactive = 'Inactive';

        public function getTableName()
    {
        return 'OrderCart';
    }
    
}