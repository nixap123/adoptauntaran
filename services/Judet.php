<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Judet extends Database
{
    
    const StatusActive = 'Active';
    const StatusInactive = 'Inactive';
    
    const TypeAdmin= 'Admin';
    const TypeClient = 'Client';
    const TypeGal = 'Gal';
    const TypeProducator = 'Producator';
    
    const ClientTypePF = 'PF';
    const ClientTypePJ = 'PJ';

    public function getTableName()
    {
        return 'Judet';
    }
    
    public static function judetSelect($selected = false, $postName = false, $extra = '')
    {
        $_this = new Judet;
        $result = $_this->getAll();
        $data = ArrayHelper::keyVal($result, 'ID', 'Name');
		$data = [0 => '' . Lang::t('SelectCounty') . ''] + $data;
        return Html::form_dropdown($postName ? $postName : 'Judet', $data, $selected, ' ' . $extra);
    }
    
}