<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Donate extends Database
{
    
    const StatusCanceled = 'Canceled';
    const StatusPending = 'Pending';
    const StatusPaid = 'Paid';

    
    public function getTableName()
    {
        return 'Donate';
    }
    
    public static function statusSelect($selected = false)
    {
        $data = [
            self::StatusPending => Lang::t(self::StatusPending),
            self::StatusPaid    => Lang::t(self::StatusPaid)
        ];
        
        return Html::form_dropdown('DonateStatus', $data, $selected);
    }
    
}