<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Subscribe extends Database
{
    
    public function getTableName()
    {
        return 'Subscribe';
    }
    
    public function getList()
    {
        return $this->query("select * from `Subscribe` order by `Date` desc");
    }
    
}