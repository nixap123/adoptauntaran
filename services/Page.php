<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Page extends Database
{
    
    public function getTableName()
    {
        return 'Page';
    }
    
    public function getListForEdit($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("select * from `Page` as n left join `PageLang` as nl on nl.PageID = n.ID and nl.LangID = " . Request::getLangID() . " where n.UserID = $userID order by Date desc");
        
        return $res;
    }
    
    public function getByUrl($url)
    {
        $res = $this->query("
            select p.Url, pl.Title, pl.Text from Page as p 
            left join PageLang as pl on pl.PageID = p.ID and pl.LangID = " . Request::getLangID() . " 
            where p.Url = '" . $this->escape($url) . "'
        ");
        
        return reset($res);
    }
    
}