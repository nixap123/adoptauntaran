<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class OrderCartProduct extends Database
{
    
    public function getTableName()
    {
        return 'OrderCartProduct';
    }
    
}