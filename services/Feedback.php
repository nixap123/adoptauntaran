<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Feedback extends Database
{
    
    public function getTableName()
    {
        return 'Feedback';
    }
    
    public function getUserFeedback($userID = false)
    {
        if ($userID)
        {
            $where = "where f.ReceiverID = " . (int)$userID;
        }
        
        return $this->query("
                select ui.Name as Receiver, f.Name as Sender, f.Email, f.Phone, f.Message, f.`Date` from `Feedback` as f 
                left join `UserInfo` as ui on ui.UserID = f.ReceiverID 
                $where 
                order by f.`Date` desc
        ");
    }
    
}