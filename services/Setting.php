<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Judet.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/City.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';

class Setting extends Database
{
    
    const SettingsTypeBoolean = 'boolean';
    const SettingsTypeString = 'string';
    const SettingsTypeInt = 'int';
    const SettingsTypeText = 'text';
    const SettingsTypeEmail = 'email';
    const SettingsTypeJudet = 'judet';
    const SettingsTypeLocation = 'location';
    const SettingsTypeSerealized = 'serialized';
    const SettingsTypeQRCode = 'QRCode';
    const SettingsTypeDomain = 'domain';


    public function getTableName()
    {
        return 'Setting';
    }
    
    public function getSettings()
    {
        $res = $this->getAll();
        
        $result = [];
        foreach ($res as $row)
        {
            $result[$row['Type']][$row['Name']] = $row;
        }
        
        return $result;
    }
    
    public static function adminSettingsList()
    {
        return [
            'General' => [
                'Address' => ['Type' => self::SettingsTypeString],
                'Judet' => ['Type' => self::SettingsTypeJudet],
                'Location' => ['Type' => self::SettingsTypeLocation],
                'Email' => ['Type' => self::SettingsTypeEmail],
                'Phone' => ['Type' => self::SettingsTypeString],
                'Fax' => ['Type' => self::SettingsTypeString],
            ],
            'Pagini' => [
                'NewsPerPage' => ['Type' => self::SettingsTypeInt]
            ],
            'Producatori' => [
                'DescriptionLength' => ['Type' => self::SettingsTypeInt]
            ]
        ];
    }
    
    public static function userSettingsList()
    {
        return [
            'General' => [
                'Address' => ['Type' => self::SettingsTypeString],
                'Judet' => ['Type' => self::SettingsTypeJudet],
                'Location' => ['Type' => self::SettingsTypeLocation],
                'Email' => ['Type' => self::SettingsTypeEmail],
                'Phone' => ['Type' => self::SettingsTypeString],
                'Fax' => ['Type' => self::SettingsTypeString],
            ],
            'Pagini' => [
                'NewsPerPage' => ['Type' => self::SettingsTypeInt]
            ]
        ];
    }
    
    
    public static function producatorSettingsList()
    {
        return [
            'Pagini' => [
                'NewsPerPage' => ['Type' => self::SettingsTypeInt]
            ],
            'Livrare' => [
                'AmLivrare' => ['Type' => self::SettingsTypeBoolean],
                '1Luni' => ['Type' => self::SettingsTypeSerealized],
                '2Marti' => ['Type' => self::SettingsTypeSerealized],
                '3Miercuri' => ['Type' => self::SettingsTypeSerealized],
                '4Joi' => ['Type' => self::SettingsTypeSerealized],
                '5Vineri' => ['Type' => self::SettingsTypeSerealized],
                '6Sambata' => ['Type' => self::SettingsTypeSerealized],
                '7Duminica' => ['Type' => self::SettingsTypeSerealized],
            ],
            'Vănzări' => [
                'CumparaturaMinimala' => ['Type' => self::SettingsTypeInt]
            ],
            'QRCode' => [
                'QRCode' => ['Type' => self::SettingsTypeQRCode]
            ]
        ];
    }
    
    public static function galSettingsList()
    {
        return [
            'Pagini' => [
                'NewsPerPage' => ['Type' => self::SettingsTypeInt]
            ],
            'Livrare' => [
                'AmLivrare' => ['Type' => self::SettingsTypeBoolean],
                '1Luni' => ['Type' => self::SettingsTypeSerealized],
                '2Marti' => ['Type' => self::SettingsTypeSerealized],
                '3Miercuri' => ['Type' => self::SettingsTypeSerealized],
                '4Joi' => ['Type' => self::SettingsTypeSerealized],
                '5Vineri' => ['Type' => self::SettingsTypeSerealized],
                '6Sambata' => ['Type' => self::SettingsTypeSerealized],
                '7Duminica' => ['Type' => self::SettingsTypeSerealized],
            ],
            'Domain' => [
                'Domain' => ['Type' => self::SettingsTypeDomain]
            ]
        ];
    }
    
    public static function getSettingsForm($settingsConfig)
    {
        $_this = new Setting;
        $savedSettings = $_this->getWhere(['UserID' => $_SESSION['UserID']]);
        
        $groupedSettings = [];
        foreach ($savedSettings as $row)
        {
            $groupedSettings[$row['Name']] = $row;
        }
        
        $return = '<ul class="nav nav-tabs" role="tablist">';
        foreach ($settingsConfig as $group => $settings)
        {
            $return .= '<li role="presentation" class="' . (reset($settingsConfig) == $settings ? 'active' : '') . '"><a href="#tab-' . $group . '" aria-controls="' . $group . '" role="tab" data-toggle="tab">' . Lang::t($group) . '</a></li>';
        }
        $return .= '</ul><br />';
        
        $return .= '<div class="tab-content">';
        foreach ($settingsConfig as $group => $settings)
        {
            $return .= '<div role="tabpanel" class="tab-pane ' . (reset($settingsConfig) == $settings ? 'active' : '') . '" id="tab-' . $group . '">';
            foreach ($settings as $settingName => $settingData)
            {
                $value = isset($groupedSettings[$settingName]) ? $groupedSettings[$settingName]['Value'] : '';
                
                $return .= '<div class="form-group"><label class="control-label">' . Lang::t($settingName) . '</label>';
                if ($settingData['Type'] == self::SettingsTypeString)
                {
                    $return .= '<input class="form-control" type="text" name="settings[' . $group . '][' . $settingName . ']" value="' . $value . '" />';
                }
                elseif ($settingData['Type'] == self::SettingsTypeEmail)
                {
                    $return .= '<input class="form-control" type="email" name="settings[' . $group . '][' . $settingName . ']" value="' . $value . '" />';
                }
                elseif ($settingData['Type'] == self::SettingsTypeInt)
                {
                    $return .= '<input style="max-width: 200px;" class="form-control" type="number" name="settings[' . $group . '][' . $settingName . ']" value="' . $value . '" />';
                }
                elseif ($settingData['Type'] == self::SettingsTypeJudet)
                {
                    $judet = $value;
                    $postName = 'settings[' . $group . '][' . $settingName . ']';
                    $return .= Judet::judetSelect($value, $postName, 'select2 class="form-control"');
                }
                elseif ($settingData['Type'] == self::SettingsTypeLocation)
                {
                    $postName = 'settings[' . $group . '][' . $settingName . ']';
                    $return .= City::citySelect($judet, $value, $postName, 'select2');
                }
                elseif ($settingData['Type'] == self::SettingsTypeSerealized)
                {
                    if (!empty($value))
                    {
                        $value = unserialize($value);
                    }
                    
                    $user_info_service = new UserInfo();
                    $user_info = $user_info_service->getByUserID($_SESSION['UserID']);
                    
                    $postName = 'settings[' . $group . '][' . $settingName . '][]';
                    $return .= City::citySelect($user_info['JudetID'], $value, $postName, 'multiple select2 style="width: 100%;"');
                }
                elseif ($settingData['Type'] == self::SettingsTypeBoolean)
                {
                    $return .= '<div>Da <input style="display:inline-block" ' . ($value == 1 ? 'checked' : '') . ' type="radio" name="settings[' . $group . '][' . $settingName . ']" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;';
                    $return .= 'Nu <input style="display:inline-block" ' . ($value == 0 ? 'checked' : '') . ' type="radio" name="settings[' . $group . '][' . $settingName . ']" value="0" /></div>';
                }
                elseif ($settingData['Type'] == self::SettingsTypeQRCode)
                {
                    $return .= '<br /><div><a target="_blank" href="/reports/report?report=qr-code&data[size]=a4&data[UserID]=' . $_SESSION['UserID'] . '"><i class="fa fa-qrcode" aria-hidden="true"></i> QR Code A4</a></div><br />';
                    $return .= '<div><a target="_blank" href="/reports/report?report=qr-code&data[size]=a7&data[UserID]=' . $_SESSION['UserID'] . '"><i class="fa fa-qrcode" aria-hidden="true"></i> QR Code A7</a></div>';
                }
                elseif ($settingData['Type'] == self::SettingsTypeDomain)
                {
                    ob_start();
                    include $_SERVER['DOCUMENT_ROOT'] . '/gal/add-domain.php';
                    $content = ob_get_clean();
                    $return .= $content;
                }
                $return .= '</div>';
            }
            $return .= '</div>';
        }
        $return .= '</div><script>$("select[select2]").select2()</script>';
        
        return $return;
    }
    
    public static function saveSettings($post)
    {
        $_this = new Setting;
        if (isset($post['settings']))
        {
            $_this->delete(['UserID' => $_SESSION['UserID']]);
            
            foreach ($post['settings'] as $group => $settings)
            {
                foreach ($settings as $settingName => $value)
                {
                    $_this->insert([
                        'UserID' => $_SESSION['UserID'],
                        'Name' => $settingName,
                        'Value' => is_array($value) ? serialize($value) : $value,
                        'Group' => $group
                    ]);
                }
            }
        }
        
        if (isset($post['addDomain']))
        {
            $domain = $post['settings']['Domain']['Domain'];
            
            if (!empty($domain))
            {
                require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';
                $user_service = new UserInfo();
                $user = $user_service->getByUserID($_SESSION['UserID']);
                
                if (!empty($user['UserID']))
                {
                    $user_name = Url::strToUrl($user['Name']);
                    $res = file_get_contents(Url::link("domain.php?domain=$domain&user=$user_name"));
                    exit($res);
                }
            }
        }
    }
    
    public function getUserDeliveryDays($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("
            select * from Setting as s 
            where s.UserID = $userID and s.`Group` = 'Livrare' 
            order by s.Name
        ");
        
        $res2 = $this->query("select * from City");
        $cities = [];
        foreach ($res2 as $row)
        {
            $cities[$row['ID']] = $row['Name'];
        }
        
        $return = [];
        foreach ($res as $row)
        {
            $ids = empty($row['Value']) ? [] : unserialize($row['Value']);
            
            
            if (is_array($ids))
            {
                $c = [];
                foreach ($ids as $id)
                {
                    $c[] = $cities[$id];
                }
            }
            elseif ($row['Name'] == 'AmLivrare')
            {
                $c = [$row['Value']];
            }
            else
            {
                $c = [];
            }
            
            $return[$row['Name']] = implode(', ', $c);
        }
        
        return $return;
    }
    
}