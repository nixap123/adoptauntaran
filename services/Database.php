<?php

class Database
{
    
    public static $connection = null;


    public function __construct()
    {
        $connection = $this->connect();
        
        if ($connection)
        {
            static::$connection = $connection;
        }
        else
        {
            exit('Enable connect to database');
        }
    }
    
    public function connect()
    {
        return new mysqli(Config::$DbHost, Config::$DbUser, Config::$DbPassword, Config::$DbName);
    }
    
    public function getPK()
    {
        return 'ID';
    }
    
    public function getByID($id)
    {
        $return = $this->query("SELECT * FROM `" . $this->getTableName() . "` WHERE `" . $this->getPK() . "` = " . (int)$id . " LIMIT 1");
        return reset($return);
    }
    
    public function getWhere($where, $limit = null, $offset = null)
    {
        $sql = "SELECT * FROM `" . $this->getTableName() . "` WHERE " . $this->parse_where($where);
        
        if (!is_null($limit))
        {
            $sql .= " LIMIT " . (int)$limit;
        }
        
        if (!is_null($offset))
        {
            $sql .= " OFFSET " . (int)$offset;
        }
        
        return $this->query($sql);
    }
    
    public function insert($insert)
    {
        $escaped_insert = [];
        foreach ($insert as $field => $value)
        {
            $field = $this->escape($field);
            $value = $this->escape($value);
            $escaped_insert[$field] = $value;
        }
        
        $sql = "INSERT INTO `" . $this->getTableName() .  "` (`" . implode('`, `', array_keys($escaped_insert)) . "`) VALUES ('" . implode("', '", array_values($escaped_insert)) . "')";
        $this->query($sql);
        return static::$connection->insert_id;
    }
    
    public function getAll()
    {
        return $this->query('SELECT * FROM ' . $this->getTableName());
    }

    public function delete($where)
    {
        return $this->query("DELETE FROM `" . $this->getTableName() . "` WHERE " . $this->parse_where($where));
    }
    
    public function deleteByID($id)
    {
        return $this->query("DELETE FROM `" . $this->getTableName() . "` WHERE `" . $this->getPK() . "` = " . (int)$id . " LIMIT 1");
    }
    
    public function escape($str)
    {
        return static::$connection->escape_string($str);
    }

    public function query($query)
    {
        $result = static::$connection->query($query);

        if (!is_bool($result))
        {
            $return = [];
            
            while ($row = $result->fetch_assoc())
            {
                $return[] = $row;
            }
            
            return $return;
        }
    }

    private function parse_where($where)
    {
        $where_arr = [];
        foreach ($where as $field => $value)
        {
            if (is_array($value))
            {
                $esc_arr = [];
                foreach ($value as $v)
                {
                    $esc_arr[] = $this->escape($v);
                }
                
                if (empty($esc_arr)) continue;
                
                $where_arr[] = "`" . $this->escape($field) . "` in ('" . implode("', '", $esc_arr) . "')";
            }
            else
            {
                $where_arr[] = "`" . $this->escape($field) . "` = '" . $this->escape($value) . "'";
            }
        }
        
        if (count($where_arr) == 0)
        {
            return "1 = 1";
        }
        
        return implode(' AND ', $where_arr);
    }
    
    public function like($field, $value)
    {
        return $this->query("SELECT * FROM `" . $this->getTableName() . "` WHERE `" . $this->escape($field) . "` LIKE '%" . $this->escape($value) . "%'");
    }
    
    public function update($update, $where)
    {
        return $this->query("UPDATE `" . $this->getTableName() . "` SET " . $this->parse_set($update) . " WHERE " . $this->parse_where($where));
    }
    
    private function parse_set($set)
    {
        $update_arr = [];
        foreach ($set as $field => $value)
        {
            $update_arr[] = "`" . $this->escape($field) . "` = '" . $this->escape($value) . "'";
        }
        
        return implode(', ', $update_arr);
    }
    
    public function insert_batch($insert)
    {
        $data = reset($insert);
        
        if (!is_array($data))            return;
        
        $sql = "INSERT INTO `" . $this->getTableName() .  "` (`" . implode('`, `', array_keys($data)) . "`) VALUES ";
        
        $values = [];
        foreach ($insert as $row)
        {
            $values[] = "('" . implode("', '", array_values($row)) . "')";
        }
        
        $sql .= implode(', ', $values);
        
        return $this->query($sql);
    }
    
}