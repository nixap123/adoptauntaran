<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class NewsLang extends Database
{
    
    public function getTableName()
    {
        return 'NewsLang';
    }
    
    public function getByNewsID($newsID)
    {
        $newsID = (int)$newsID;
        
        $res = $this->query("
            select * from News as n 
            left join NewsLang as nl on nl.NewsID = n.ID 
            where n.ID = $newsID 
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['LangID']] = $row;
        }
        
        return $return;
    }
    
}