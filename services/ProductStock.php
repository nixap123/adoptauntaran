<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/User.php';

class ProductStock extends Database
{
    
    const StatusActive = 'Active';
    const StatusReserved = 'Reserved';
    const StatusInactive = 'Inactive';
    const StatusDeleted = 'Deleted';

    
    public function getTableName()
    {
        return 'ProductStock';
    }
    
    public function getActiveProductStocks($userID)
    {
        $res = $this->query("
            select p.ID, ps.ID as psID, p.UnitID, pl.Name as ProductName, cl.Name as CategoryName, ps.Quantity, ps.Price, ps.ExpirationDate, ps.Regular, 0 as Orders, ps.OrderedQuantity, ps.`Status` from ProductStock as ps
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join Category as c on c.ID = p.CategoryID 
            left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = " . Request::getLangID() . "  
            where ps.UserID = $userID and ps.`Status` != '" . ProductStock::StatusDeleted . "' and (ps.ExpirationDate > date(now()) OR Regular = 1)
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['psID']] = $row;
        }
        
        return $return;
    }
    
    public function getProductStocksForFrontend($userID)
    {
        $res = $this->query("
            select p.ID, p.CategoryID, ps.ID as psID, p.UnitID, pl.Name as ProductName, cl.Name as CategoryName, ps.Quantity, ps.Price, ps.ExpirationDate, ps.Regular, 0 as Orders, ps.OrderedQuantity, ps.`Status`, (ps.Quantity - ps.OrderedQuantity) as Sold from ProductStock as ps
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join Category as c on c.ID = p.CategoryID 
            left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = " . Request::getLangID() . "  
            where ps.UserID = $userID and ps.`Status` = 'Active' and (ps.ExpirationDate > date(now()) OR Regular = 1) 
            having Sold > 0
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['CategoryID']][] = $row;
        }
        
        return $return;
    }
    
    public static function getProductStockCount($userID)
    {
        $_this = new ProductStock;
        
        $res = $_this->query("
            select count(ps.ID) as count from ProductStock as ps
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join Category as c on c.ID = p.CategoryID 
            left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = " . Request::getLangID() . "  
            where ps.UserID = $userID 
        ");
        
        $result = reset($res);
        
        return isset($result['count']) ? $result['count'] : 0;
    }

    public function getForEdit($psID, $userID)
    {
        $psID = (int)$psID;
        
        $result = $this->query("
            select p.CategoryID, p.ID as ProductID, ps.Quantity, p.UnitID, ps.Price, ps.ExpirationDate, ps.Regular, ps.OrderedQuantity from ProductStock as ps 
            left join Product as p on p.ID = ps.ProductID 
            where ps.ID = $psID and ps.UserID = $userID 
            limit 1
        ");
        
        return reset($result);
    }
    
    public function getUserProductsByCategory($categoryID, $userID)
    {
        $categoryID = (int)$categoryID;
        $userID = (int)$userID;
        
        return $this->query("
            select pl.Name, ps.ID, (ps.Quantity - ps.OrderedQuantity) as Sold, ps.Price, p.UnitID from ProductStock as ps 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            where p.CategoryID = $categoryID and (ps.Quantity - ps.OrderedQuantity) > 0 and ps.UserID = $userID 
        ");
    }
    
    public function getProduseleZonei($clientID)
    {
        $user_service = new User();
        $nearests_users = $user_service->getNearestsUserIDs($clientID);
        
        if (empty($nearests_users))
        {
            $nearests_users = [0];
        }
        
        $res = $this->query("
            select ui.Name as ProducatorName, ui.UserID as ProducatorID, p.ID, p.CategoryID, ps.ID as psID, p.UnitID, pl.Name as ProductName, p.ID as ProductID, cl.Name as CategoryName, ps.Quantity, ps.Price, ps.ExpirationDate, ps.Regular, 0 as Orders, ps.OrderedQuantity, ps.`Status`, (ps.Quantity - ps.OrderedQuantity) as Sold from ProductStock as ps
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join Category as c on c.ID = p.CategoryID 
            left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = " . Request::getLangID() . " 
            left join UserInfo as ui on ui.UserID = ps.UserID 
            where ps.`Status` = 'Active' and (ps.ExpirationDate > date(now()) OR Regular = 1) and ps.Quantity > ps.OrderedQuantity and ps.UserID in (" . implode(',', array_keys($nearests_users)) . ") 
            having Sold > 0
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['CategoryID']]['Name'] = $row['CategoryName'];
            $return[$row['CategoryID']]['Products'][$row['ProductID']]['Name'] = $row['ProductName'];
            $return[$row['CategoryID']]['Products'][$row['ProductID']]['Producatori'][$row['psID']] = $row;
        }
        
//        echo '<pre>';
//        print_r($return);
//        exit('</pre>');
        
        return $return;
    }
    
}