<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Image extends Database
{
    
    public function getTableName()
    {
        return 'Image';
    }

    public static function getByImageIDs($id)
    {
        $_this = new Image;
        $id = (array)$id;
        
        if (count($id) == 0) return false;
        
        return $_this->getByIDs($id);
    }
    
    public function getByIDs($ids)
    {
        return $this->query("select * from Image where ID in (" . implode(', ', $ids) . ") ");
    }
    
}