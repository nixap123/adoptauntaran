<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Bank extends Database
{
    
    public function getTableName()
    {
        return 'Bank';
    }
    
    public static function bankSelect($selected = false)
    {
        $_this = new Bank;
        $result = $_this->getAll();
        $data = ArrayHelper::keyVal($result, 'ID', 'Name');
        return Html::form_dropdown('BankID', $data, $selected, 'class="form-control"');
    }
    
}