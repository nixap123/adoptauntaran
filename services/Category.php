<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Category extends Database
{
    
    public function getTableName()
    {
        return 'Category';
    }
    
    public function getByParentID($parentID = 0)
    {
        return $this->getWhere(['ParentID' => (int)$parentID]);
    }
    
    public function getTree()
    {
        $categories = $this->query("select c.ID, c.ParentID, cl.Name from `Category` as c left join `CategoryLang` as cl on cl.LangID = " . Request::getLangID() . " and c.ID = cl.CategoryID");
        
        $tree = $this->categoriesToTree($categories);
        
        return $this->buildTree($tree);
    }
            
    public function categoriesToTree(&$categories)
    {
        $map = array(
            0 => array('subcategories' => array())
        );

        foreach ($categories as &$category)
        {
            $category['subcategories'] = array();
            $map[$category['ID']] = &$category;
        }
        
        foreach ($categories as &$category)
        {
            $map[$category['ParentID']]['subcategories'][] = &$category;
        }

        return $map[0]['subcategories'];
    }
    
    private function buildTree($array)
    {
        $return = '<ol class="dd-list">';
        foreach ($array as $row)
        {
            $return .= '<li class="dd-item" data-id="' . $row['ID'] . '"><div class="dd-handle"> <a style="cursor:pointer;">' . $row['Name'] . '</a>';
            $return .= '<div class="pull-right action-buttons">
                    <a data-toggle="modal" data-category-id="' . $row['ID'] . '" data-target=".bs-example-modal-sm" class="blue edit-category">
                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                    </a>
                    <a class="red delete-category" data-category-id="' . $row['ID'] . '" href="#">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                    <a class="green add-product" data-product-id="0" data-category-id="' . $row['ID'] . '" data-toggle="modal" data-target=".add-product-modal" href="#">
                        <i class="ace-icon fa fa-plus bigger-130"></i>
                    </a>
            </div></div>';
            if (count($row['subcategories']) > 0)
            {
                $return .= $this->buildTree($row['subcategories']);
            }
            $return .= '</li>';
        }
        $return .= '</ol>';
        
        return $return;
    }
    
    public function getForEdit($categotyID)
    {
        return $this->query("SELECT cl.LangID, cl.Name from Category AS c left join CategoryLang as cl ON cl.CategoryID = c.ID WHERE c.ID = " . (int)$categotyID);
    }
    
    public static function categorySelect($selected = false, $langID = 1)
    {
        $langID = Request::getLangID();
        $_this = new Category;
        $result = $_this->query("
            select * from Category as c 
            left join CategoryLang as cl on cl.CategoryID = c.ID and cl.LangID = $langID
        ");
        $data = ArrayHelper::keyVal($result, 'ID', 'Name');
        $data = ['0' => 'Selectati'] + $data;
        return Html::form_dropdown('CategoryID', $data, $selected, 'class="form-control"');
    }
    
}