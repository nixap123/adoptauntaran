<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class UserPhoto extends Database
{

    public function getTableName()
    {
        return 'UserPhoto';
    }
    
    public function getByUserID($userID)
    {
        $userID = (int)$userID;
        
        return $this->query("select *, up.ID as upID from `UserPhoto` as up left join Image as i on i.ID = up.imageID where up.UserID = $userID");
    }
    
}