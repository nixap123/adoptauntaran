<?php

require_once 'core/core.php';
require_once 'Database.php';

class UserLocation extends Database
{

    public function getTableName()
    {
        return 'UserLocation';
    }
    
    public function deleteByUserID($id)
    {
        return $this->delete(['UserID' => $id]);
    }
    
}