<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/UserInfo.php';

class User extends Database
{
    
    const StatusActive = 'Active';
    const StatusInactive = 'Inactive';
    
    const TypeAdmin= 'Admin';
    const TypeClient = 'Client';
    const TypeGal = 'Gal';
    const TypeProducator = 'Producator';
    
    const ClientTypePF = 'PF';
    const ClientTypePJ = 'PJ';

    public function getTableName()
    {
        return 'User';
    }
    
    public function checkLogin()
    {
        if (isset($_SESSION['UserID']))
        {
            $user = $this->getByID((int)$_SESSION['UserID']);
            
            if (!empty($user) && $user['Status'] == self::StatusActive)
            {
                return true;
            }
        }
        
        return false;
    }
    
    public function doLogin($username, $password)
    {
        $user = $this->getWhere(['UserName' => $username, 'Password' => $password], 1);
        
        if (!empty($user[0]))
        {
            return $user[0];
        }
        else
        {
            return false;
        }
    }

    public function goToAccount($user)
    {
        Url::redirect($this->accountLink($user));
    }
    
    public function accountLink($user)
    {
        switch ($user['Type'])
        {
            case static::TypeAdmin:
                return Url::link('admin/index.php');
            break;
        
            case static::TypeGal:
                return Url::link('gal/index.php');
            break;
        
            case static::TypeProducator:
                return Url::link('producator/index.php');
            break;
        
            case static::TypeClient:
                return Url::link('client/index.php');
            break;

            default:
                return Url::link('login.php');
            break;
        }
    }
    
    public function getLastRegistrations()
    {
        return $this->query("
            select u.ID, ui.Name, c.Name as CityName, j.Name as JudetName, u.`ClientType`, u.RegDate from `User` as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Judet as j on j.ID = ui.JudetID 
            left join City as c on c.ID = ui.LocationID 
            where u.`Type` in ('" . self::TypeGal . "', '" . self::TypeProducator . "') 
            order by u.RegDate desc 
            limit 20
        ");
    }
    
    public static function statusSelect($selected = false)
    {
        $data = [
            self::StatusActive => self::StatusActive,
            self::StatusInactive => self::StatusInactive
        ];
        return Html::form_dropdown('Status', $data, $selected, 'class="form-control"');
    }
    
    public function getGalsAndProducatori()
    {
        return $this->query("
            select u.ID, u.Status, ui.Name, c.Name as CityName, j.Name as JudetName, u.`Type`, u.RegDate, ui.Phone, 0 as Products, 0 as Rating, 0 as Comments from `User` as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Judet as j on j.ID = ui.JudetID 
            left join City as c on c.ID = ui.LocationID 
            where u.`Type` in ('" . self::TypeGal . "', '" . self::TypeProducator . "') 
            order by u.RegDate desc 
            limit 20
        ");
    }
    
    public function getProducatoriOfGal($galID)
    {
        $galID = (int)$galID;
        
        return $this->query("
            select u.ID, ui.Name, c.Name as CityName, j.Name as JudetName, u.`Type`, u.RegDate, ui.Phone, 0 as Products, 0 as Rating, 0 as Comments from `User` as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Judet as j on j.ID = ui.JudetID 
            left join City as c on c.ID = ui.LocationID 
            where u.`Type` in ('" . self::TypeProducator . "') and u.ParentID = $galID 
            order by u.RegDate desc 
        ");
    }
    
    public function getAllClients()
    {
        return $this->query("
            select u.ID, u.`Status`, ui.Name, c.Name as CityName, j.Name as JudetName, u.`Type`, u.RegDate, ui.Phone, 0 as Products, 0 as Rating, 0 as Comments from `User` as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Judet as j on j.ID = ui.JudetID 
            left join City as c on c.ID = ui.LocationID 
            where u.`Type` = '" . User::TypeClient . "' 
            order by u.RegDate desc 
        ");
    }
    
    public function deleteUserByID($id)
    {
        $user_info_service = new UserInfo;
        
        $this->deleteByID($id);
        $user_info_service->delete(['UserID' => $id]);
    }
    
    public static function galSelect($selected = false)
    {
        $_this = new User;
        $result = $_this->query("
            select u.UserName, ui.Name, u.ID from User as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            where u.`Type` = '" . User::TypeGal . "'
        ");
        
        $data = [0 => 'Selectati GAL'];
        foreach ($result as $row)
        {
            $data[$row['ID']] = $row['Name'] . ' - ' . $row['UserName'];
        }
        
        return Html::form_dropdown('ParentID', $data, $selected, 'class="form-control"');
    }
    
    public function getGalsWithCoverage()
    {
        return $this->query("
            select ui.Name, ui.Coverage, c.`Long`, c.Lat from User as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join City as c on c.ID = ui.LocationID 
            where u.`Type` = '" . self::TypeGal . "'
        ");
    }
    
    public function getRandomProducators()
    {
        $randIDs = [0];
        $res = $this->query("select u.ID from User as u where u.`Type` = 'Producator' order by rand() limit 9");
        foreach ($res as $row)
        {
            $randIDs[] = $row['ID'];
        }
        
        $res = $this->query("
            select u.ID, ui.Photo, i.Image, i.Thumb, ui.Name, cl.Name as CategoryName, cl.CategoryID as CategoryID from User as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Image as i on i.ID = ui.Photo 
            left join ProductStock as ps on ps.UserID = u.ID 
            left join Product as p on p.ID = ps.ProductID 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID() . " 
            where u.`Type` = '" . User::TypeProducator . "' and u.`Status` = '" . User::StatusActive . "' and u.ID in (" . implode(', ', $randIDs) . ") and ((ps.Quantity - ps.OrderedQuantity) > 0 and ps.ExpirationDate > date(now()) or ps.Regular = 1)
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['ID']]['User'] = $row;
            if (!empty($row['CategoryName']))
            {
               $return[$row['ID']]['Categories'][$row['CategoryID']] = $row['CategoryName'];
            }
            
        }
        
        return $return;
    }
    
    public function getProducatoriByJudet($judetID, $limit = 6, $offset = 0)
    {
        $judetID = (int)$judetID;
        $limit = (int)$limit;
        $offset = (int)$offset;
        
        $LIMOFF = $limit && $offset ? "limit $limit offset $offset" : "";
        
        $where = $judetID > 0 ? "and ui.JudetID = $judetID" : '';
        
        $res = $this->query("
            select SQL_CALC_FOUND_ROWS u.ID from User as u
            left join UserInfo as ui on ui.UserID = u.ID 
            where u.`Status` = '" . User::StatusActive . "'" . " $where 
            $LIMOFF
        ");
        
        $count = $this->query('select FOUND_ROWS() as `count`');
        $count = reset($count);
        $count = $count['count'];
        
        $IDs = [0];
        foreach ($res as $row)
        {
            $IDs[$row['ID']] = $row['ID'];
        }
        
        $res = $this->query("
            select u.ID, ui.Photo, ui.Name, cl.Name as CategoryName, cl.CategoryID as CategoryID from User as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join ProductStock as ps on ps.UserID = u.ID 
            left join Product as p on p.ID = ps.ID 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID() . " 
            where u.`Type` = 'Producator' and cl.Name is not null and u.ID in (" . implode(', ', $IDs) . ")
            group by u.ID, cl.CategoryID
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['ID']]['User'] = $row;
            if (!empty($row['CategoryName']))
            {
               $return[$row['ID']]['Categories'][$row['CategoryID']] = $row['CategoryName'];
            }
            
        }
        
        return [
            'result' => $return,
            'count' => $count
        ];
    }
    
    public function getNearestsUserIDs($userID, $types = [User::TypeGal, User::TypeProducator])
    {
        $res1 = $this->query("select c.Lat, c.`Long` from `UserInfo` as ui left join City as c on c.ID = ui.LocationID where ui.UserID = $userID limit 1");
        $cityGeo = reset($res1);
        
        $lat = (float)$cityGeo['Lat'];
        $long = (float)$cityGeo['Long'];
        
        
        
        $res = $this->query("
            SELECT ui.UserID, c.ID, c.Name, c.Lat, c.`Long`, u.`Type`,
            round(( 6371 * acos( cos( radians($lat) ) * cos( radians( Lat ) ) * cos( radians( `Long` ) - radians($long) ) + sin( radians($lat) ) * sin( radians( Lat ) ) ) ), 3) AS distance, 
            ui.Coverage as Coverage 
            FROM City as c 
            left join UserInfo as ui on ui.LocationID = c.ID 
            left join User as u on u.ID = ui.UserID 
            where u.`Type` in ('" . implode("','", $types) . "') and u.`Status` = '" . User::StatusActive . "' 
            HAVING distance <= COALESCE(ui.Coverage, 50) ORDER BY distance
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['UserID']] = $row;
        }
        
        return $return;
    }
    
    public function getGalProducatori($galID)
    {
        $galID = (int)$galID;
        
        $randIDs = [0];
        $res = $this->query("select u.ID from User as u where u.`Type` = '" . User::TypeProducator . "' and u.ParentID = $galID");
        foreach ($res as $row)
        {
            $randIDs[] = $row['ID'];
        }
        
        $res = $this->query("
            select u.ID, ui.Photo, i.Thumb, ui.Name, cl.Name as CategoryName, cl.CategoryID as CategoryID from User as u 
            left join UserInfo as ui on ui.UserID = u.ID 
            left join Image as i on i.ID = ui.Photo 
            left join ProductStock as ps on ps.UserID = u.ID 
            left join Product as p on p.ID = ps.ProductID 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID(). " 
            where u.`Type` = '" . User::TypeProducator . "' and u.`Status` = '" . User::StatusActive . "' and u.ID in (" . implode(', ', $randIDs) . ") and ((ps.Quantity - ps.OrderedQuantity) > 0 and ps.ExpirationDate > date(now()) or ps.Regular = 1)
            order by rand()
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['ID']]['User'] = $row;
            if (!empty($row['CategoryName']))
            {
               $return[$row['ID']]['Categories'][$row['CategoryID']] = $row['CategoryName'];
            }
            
        }
        
        return $return;
    }
    
    public function getByCondition($ids, $type, $limit, $offset, $sort)
    {
        $ids_str = implode(',', $ids);
        
        return $this->query("
            select u.ID, ui.Name, avg(ur.Rating) as Rating, count(ps.ID) as ProductCount, i.Thumb, u.`Type` from User as u 
            left join UserInfo as ui on ui.UserID = u.ID
            left join UserRating as ur on ur.UserID = u.ID 
            left join ProductStock as ps on ps.UserID = u.ID and (ps.ExpirationDate > now() or ps.Regular = 1) 
            left join Image as i on i.ID = ui.Photo 
            where u.`Status` = '" . User::StatusActive . "' and u.`Type` = '$type' and u.ID in ($ids_str)
            group by u.ID 
            order by $sort  
            limit $limit offset $offset
        ");
    }
    
}