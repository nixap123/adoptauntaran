<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class PageLang extends Database
{
    
    public function getTableName()
    {
        return 'PageLang';
    }
    
    public function getByPageID($newsID)
    {
        $newsID = (int)$newsID;
        
        $res = $this->query("
            select * from Page as n 
            left join PageLang as nl on nl.PageID = n.ID 
            where n.ID = $newsID 
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $return[$row['LangID']] = $row;
        }
        
        return $return;
    }
    
}