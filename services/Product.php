<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Product extends Database
{
    
    public function getTableName()
    {
        return 'Product';
    }
    
    public function getForEdit($productID)
    {
        return $this->query("SELECT pl.LangID, pl.Name from Product AS p left join ProductLang as pl ON pl.ProductID = p.ID WHERE p.ID = " . (int)$productID);
    }
    
    public function getForAdmin($categoryID, $langID = 1)
    {
        $categoryWhere = $categoryID > 0 ? ' and p.CategoryID = ' . (int)$categoryID : '';
        
        return $this->query("
            SELECT p.ID, pl.LangID, pl.Name, cl.Name as CategoryName, p.UnitID from Product AS p 
            left join ProductLang as pl ON pl.ProductID = p.ID 
            left join CategoryLang as cl on cl.CategoryID = p.CategoryID and cl.LangID = " . Request::getLangID() . " 
            WHERE pl.LangID = " . Request::getLangID() . $categoryWhere
        );
    }
    
    public static function productSelect($categoryID, $selected)
    {
        $CategoryID = (int)$categoryID;
        $_this = new Product;
        
        $res = $_this->getByCategory($CategoryID);
        
        $options = ArrayHelper::keyVal($res, 'ProductID', 'Name');
        
        return Html::form_dropdown('ProductID', $options, $selected, 'class="form-control"');
    }
    
    public function getByCategory($CategoryID)
    {
        $CategoryID = (int)$CategoryID;
        
        $res = $this->query("
            select p.ID as ProductID, pl.Name, p.UnitID from Product as p 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            where p.CategoryID = $CategoryID
        ");
        
        return $res;
    }
    
}