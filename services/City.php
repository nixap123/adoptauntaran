<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class City extends Database
{
    
    public function getTableName()
    {
        return 'City';
    }
    
    public function getByJudetID($id)
    {
        return $this->getWhere(['JudetID' => (int)$id]);
    }
    
    public static function citySelect($JudetID, $selected = false, $postName = false, $extra = '')
    {
        $_this = new City;
        $result = $JudetID > 0 ? $_this->getWhere(['JudetID' => $JudetID]) : $_this->getAll();
        $data = ArrayHelper::keyVal($result, 'ID', 'Name');
        return Html::form_dropdown($postName ? $postName : 'Location', $data, $selected, 'class="form-control" ' . $extra);
    }
    
    public function getNearestCities($lat, $long, $distance = 50, $query = '')
    {
        $res = $this->query("
            SELECT c.ID, c.Name, c.Lat, c.`Long`, j.Name as JudetName, 
            round(( 6371 * acos( cos( radians($lat) ) * cos( radians( Lat ) ) * cos( radians( `Long` ) - radians($long) ) + sin( radians($lat) ) * sin( radians( Lat ) ) ) ), 3) AS distance 
            FROM City as c 
            left join `Judet` as j on j.ID = c.JudetID 
            where c.Name like '%" . $this->escape($query) . "%' 
            HAVING distance <= $distance ORDER BY c.Name 
        ");
        
        return $res;
    }
    
}