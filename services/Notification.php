<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';

class Notification extends Database
{
    
    const CategorySystem = 'cogs';
    const CategorySales = 'shopping-cart';
    const CategoryStock = 'product-hunt';
    const CategoryUser = 'user';
    
    const TypeSuccess = 'Success';
    const TypeInfo = 'Info';
    const TypeWarning = 'Warning';

    
    public function getTableName()
    {
        return 'Notification';
    }
    
    public function send($userID, $title = 'Notification', $text = '', $type = self::TypeInfo, $category = self::CategorySystem)
    {
        $userID = (int)$userID;
        
        return $this->insert([
            'UserID' => $userID,
            'Title' => $title,
            'Text' => $text,
            'Category' => $category,
            'Type' => $type,
            'Date' => date('c'),
            'Unread' => 1
        ]);
    }
    
    public function getCountUnreaded($userID)
    {
        $res = $this->query("select * from `Notification` where `UserID` = $userID and `Date` <= now() and `Unread` = 1 order by `Unread` desc, `Date` desc");
        
        return (int)count($res);
    }
    
    public function getList($userID)
    {
        $list = [
            self::TypeSuccess => [],
            self::TypeInfo => [],
            self::TypeWarning => []
        ];
        
        $res = $this->query("select * from `Notification` where `UserID` = $userID order by `Unread` desc, `Date` desc limit 50");
        
        foreach ($res as $row)
        {
            $list[$row['Type']][] = $row;
        }
        
        return $list;
    }
    
}