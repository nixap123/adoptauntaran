<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/core/core.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Database.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/OrderCart.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/services/Unit.php';

class Order extends Database
{
    
    const StatusInCart = 'InCart';
    const StatusProccessing = 'Proccessing';
    const StatusPaid = 'Paid';
    
    const DeliveryStatusLivrat = 1;
    const DeliveryStatusNeridicat = 0;


    public function getTableName()
    {
        return 'Order';
    }
    
    public function getInCartOrder($userID)
    {
        $userID = (int)$userID;
        
        return $this->query("
            select *, ocp.ID as ocpID, o.Amount as Total, oc.Price as CartPrice, oc.Quantity as CartQuantity, ocp.Price as ProductPrice, ocp.Quantity as ProductQuantity from `Order` as o 
            right join OrderCart as oc on oc.OrderID = o.ID 
            right join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID 
            where (o.`Status` = '" . Order::StatusInCart . "' or (o.`Status` <> '" . Order::StatusInCart . "' and oc.`Status` = '" . OrderCart::StatusActive . "' and oc.`Type` = '" . OrderCart::TypeCart . "')) and o.UserID = $userID
        ");
    }
    
    public function getCartContents($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("
            select oc.ID as ocID, ocp.ID as ocpID, pl.Name as ProductName, ui.Name as UserName, p.UnitID, oc.CartWeekID, oc.`Type`, o.Amount as Total, oc.Price as CartPrice, oc.Quantity as CartQuantity, ocp.Price as ProductPrice, ocp.Quantity as ProductQuantity from `Order` as o 
            left join OrderCart as oc on oc.OrderID = o.ID 
            right join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID 
            left join ProductStock as ps on ps.ID = ocp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join UserInfo as ui on ui.UserID = ps.UserID 
             where (o.`Status` = '" . Order::StatusInCart . "' or (o.`Status` <> '" . Order::StatusInCart . "' and oc.`Status` = '" . OrderCart::StatusActive . "' and oc.`Type` = '" . OrderCart::TypeCart . "')) and o.UserID = $userID 
            order by oc.CartWeekID desc
        ");
        
        $return = [
            'Carts' => [],
            'Products' => []
        ];
        foreach ($res as $row)
        {
            if ($row['Type'] == OrderCart::TypeCart)
            {
                $row['TVA'] = $row['CartPrice'] * Config::$TVARate;
                $row['TotalTVA'] = $row['TVA'] * $row['CartQuantity'];
                $row['PriceWithoutTVA'] = $row['CartPrice'] - $row['TVA'];
                $row['TotalWithoutTVA'] = $row['PriceWithoutTVA'] * $row['CartQuantity'];
                $row['Subtotal'] = $row['TotalWithoutTVA'] + $row['TotalTVA'];
                
                $return['Carts'][$row['ocID']]['cart'] = $row;
                $return['Carts'][$row['ocID']]['products'][$row['ocpID']] = $row['ProductName'];
            }
            else
            {
                $row['TVA'] = $row['ProductPrice'] * Config::$TVARate;
                $row['TotalTVA'] = $row['TVA'] * $row['ProductQuantity'];
                $row['PriceWithoutTVA'] = $row['ProductPrice'] - $row['TVA'];
                $row['TotalWithoutTVA'] = $row['PriceWithoutTVA'] * $row['ProductQuantity'];
                $row['Subtotal'] = $row['TotalWithoutTVA'] + $row['TotalTVA'];
                
                $return['Products'][$row['ocpID']] = $row;
            }
        }
        
        return $return;
    }
    
    public function getClientOrderList($userID)
    {
        $userID = (int)$userID;
        
        $orderCarts = $this->query("
            select oc.ID, ocp.ID as ocpID, ui.Name as ProducatorName, oc.Price as CartPrice, pl.Name as ProductName, ocp.Quantity as ProductQuantity, ocp.Price as ProductPrice, p.UnitID from `Order` as o 
            right join OrderCart as oc on oc.OrderID = o.ID and oc.`Type` = '" . OrderCart::TypeCart . "' and oc.`Status` = '" . OrderCart::StatusActive . "' and oc.CartWeekID > 0 
            left join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID  
            left join ProductStock as ps on ps.ID = ocp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join CartWeek as cw on cw.ID = oc.CartWeekID 
            left join UserInfo as ui on ui.UserID = cw.UserID 
            where o.UserID = $userID
        ");
        
        $orders = $this->query("
            select * from `Order` as o 
            where o.UserID = $userID and o.`Status` in ('" . Order::StatusProccessing .  "', '" . Order::StatusPaid . "')
        ");
        
        $return = [
            'orderCarts' => [],
            'orders' => $orders
        ];
        
        foreach ($orderCarts as $cart)
        {
            $return['orderCarts'][$cart['ID']]['cart'] = $cart;
            $return['orderCarts'][$cart['ID']]['products'][$cart['ocpID']] = $cart['ProductName'] . ' (' . (float)$cart['ProductQuantity'] . ' ' . Unit::getName($cart['UnitID']) . ')';
        }
        
        return $return;
    }
    
    public function getProducatorOrderList($userID)
    {
        $userID = (int)$userID;
        
        $orders = $this->query("
            select o.ID, ui.Name as ClientName, ui.Phone, o.Date, SUM(IF(oc.Type = '" . OrderCart::TypeCart . "', oc.Price, ocp.Price * ocp.Quantity)) as Amount, o.Delivered, ocp.Delivered as DeliveryStatus from `Order` as o 
            left join OrderCart as oc on oc.OrderID = o.ID 
            left join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID 
            right join ProductStock as ps on ps.ID = ocp.ProductStockID and ps.UserID = $userID
            left join UserInfo as ui on ui.UserID = o.UserID 
            where o.`Status` in ('" . Order::StatusProccessing .  "', '" . Order::StatusPaid . "') 
            group by o.ID 
            order by o.Date
        ");
        
        return $orders;
    }
    
    public function getProducatorOrderCount($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("
            select o.ID as count from `Order` as o 
            left join OrderCart as oc on oc.OrderID = o.ID 
            left join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID 
            left join ProductStock as ps on ps.ID = ocp.ProductStockID 
            left join UserInfo as ui on ui.UserID = o.UserID 
            where ps.UserID = $userID and o.`Status` in ('" . Order::StatusProccessing .  "', '" . Order::StatusPaid . "') 
            group by o.ID 
            order by o.Date
        ");
        
        return count($res);
    }
    
    public function getOrderForReportClient($orderID)
    {
        $orderID = (int)$orderID;
        
        $res = $this->query("
            select oc.ID as ocID, ocp.ID as ocpID, pl.Name as ProductName, ui.Name as UserName, p.UnitID, oc.CartWeekID, oc.`Type`, o.Amount as Total, oc.Price as CartPrice, oc.Quantity as CartQuantity, ocp.Price as ProductPrice, ocp.Quantity as ProductQuantity from `Order` as o 
            left join OrderCart as oc on oc.OrderID = o.ID 
            right join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID 
            left join ProductStock as ps on ps.ID = ocp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            left join UserInfo as ui on ui.UserID = ps.UserID 
            where o.ID = $orderID 
            order by oc.CartWeekID desc
        ");
        
        $return = [
            'Carts' => [],
            'Products' => []
        ];
        foreach ($res as $row)
        {
            if ($row['Type'] == OrderCart::TypeCart)
            {
                $row['TVA'] = $row['CartPrice'] * Config::$TVARate;
                $row['TotalTVA'] = $row['TVA'] * $row['CartQuantity'];
                $row['PriceWithoutTVA'] = $row['CartPrice'] - $row['TVA'];
                $row['TotalWithoutTVA'] = $row['PriceWithoutTVA'] * $row['CartQuantity'];
                $row['Subtotal'] = $row['TotalWithoutTVA'] + $row['TotalTVA'];
                
                $return['Carts'][$row['ocID']]['cart'] = $row;
                $return['Carts'][$row['ocID']]['products'][$row['ocpID']] = $row['ProductName'];
            }
            else
            {
                $row['TVA'] = $row['ProductPrice'] * Config::$TVARate;
                $row['TotalTVA'] = $row['TVA'] * $row['ProductQuantity'];
                $row['PriceWithoutTVA'] = $row['ProductPrice'] - $row['TVA'];
                $row['TotalWithoutTVA'] = $row['PriceWithoutTVA'] * $row['ProductQuantity'];
                $row['Subtotal'] = $row['TotalWithoutTVA'] + $row['TotalTVA'];
                
                $return['Products'][$row['ocpID']] = $row;
            }
        }
        
        return $return;
    }
    
    public function getOrderForReportProducator($orderID, $producatorID)
    {
        $orderID = (int)$orderID;
        $producatorID = (int)$producatorID;
        
        $res = $this->query("
            select oc.ID as ocID, ocp.ID as ocpID, pl.Name as ProductName, ui.Name as UserName, p.UnitID, oc.CartWeekID, oc.`Type`, o.Amount as Total, oc.Price as CartPrice, oc.Quantity as CartQuantity, ocp.Price as ProductPrice, ocp.Quantity as ProductQuantity from `Order` as o 
            left join OrderCart as oc on oc.OrderID = o.ID 
            right join OrderCartProduct as ocp on ocp.OrderCartID = oc.ID 
            left join ProductStock as ps on ps.ID = ocp.ProductStockID 
            left join Product as p on p.ID = ps.ProductID 
            left join ProductLang as pl on pl.ProductID = p.ID and pl.LangID = " . Request::getLangID() . " 
            right join UserInfo as ui on ui.UserID = ps.UserID and ps.UserID = $producatorID 
            where o.ID = $orderID 
            order by oc.CartWeekID desc
        ");
        
        $return = [
            'Carts' => [],
            'Products' => []
        ];
        foreach ($res as $row)
        {
            if ($row['Type'] == OrderCart::TypeCart)
            {
                $row['TVA'] = $row['CartPrice'] * Config::$TVARate;
                $row['TotalTVA'] = $row['TVA'] * $row['CartQuantity'];
                $row['PriceWithoutTVA'] = $row['CartPrice'] - $row['TVA'];
                $row['TotalWithoutTVA'] = $row['PriceWithoutTVA'] * $row['CartQuantity'];
                $row['Subtotal'] = $row['TotalWithoutTVA'] + $row['TotalTVA'];
                
                $return['Carts'][$row['ocID']]['cart'] = $row;
                $return['Carts'][$row['ocID']]['products'][$row['ocpID']] = $row['ProductName'];
            }
            else
            {
                $row['TVA'] = $row['ProductPrice'] * Config::$TVARate;
                $row['TotalTVA'] = $row['TVA'] * $row['ProductQuantity'];
                $row['PriceWithoutTVA'] = $row['ProductPrice'] - $row['TVA'];
                $row['TotalWithoutTVA'] = $row['PriceWithoutTVA'] * $row['ProductQuantity'];
                $row['Subtotal'] = $row['TotalWithoutTVA'] + $row['TotalTVA'];
                
                $return['Products'][$row['ocpID']] = $row;
            }
        }
        
        return $return;
    }
    
    public function getClientOrdersSum($userID)
    {
        $userID = (int)$userID;
        
        $res = $this->query("select sum(Amount) as total from `Order` where `UserID` = $userID and `Status` = '" . Order::StatusPaid . "'");
        $result = reset($res);
        return (float)$result['total'];
    }
    
    public function getForMonthOrderChart()
    {
        $res = $this->query("
            SELECT DATE_FORMAT(`Date`, '%m/%Y') AS `Month`, SUM(Amount) AS Amount FROM `Order` 
            WHERE `Status` = '" . Order::StatusPaid . "' 
            GROUP BY MONTH(`Date`) 
            ORDER BY `Date` DESC 
            LIMIT 12
        ");
        
        return array_reverse($res);
    }
    
    public static function orderDiliveryStatusSelect($selected = null)
    {
        $data = [
            self::DeliveryStatusNeridicat => Lang::t('Neridicat'),
            self::DeliveryStatusLivrat => Lang::t('Livrat'),
        ];
        
        $extra = $selected == 1 ? 'disabled' : '';
        
        return Html::form_dropdown('DeliveryStatus', $data, $selected, $extra);
    }
    
    public static function clientOrderCount($userID)
    {
        $userID = (int)$userID;
        
        $_this = new Order();
        
        $res = $_this->query("select count(*) as count from `Order` where `UserID` = $userID and `Status` = '" . Order::StatusPaid . "'");
        $res = reset($res);
        
        return $res['count'];
    }
    
    public static function clientCartCount($userID)
    {
        $userID = (int)$userID;
        
        $_this = new Order();
        
        $res = $_this->query("
            select count(*) as count from OrderCart as oc 
            left join `Order` as o on o.ID = oc.OrderID 
            where oc.`Type` = '" . OrderCart::TypeCart . "' and o.UserID = $userID and oc.`Status` = '" . OrderCart::StatusActive . "'
        ");
        $res = reset($res);
        
        return $res['count'];
    }
    
    public function mustPopularProducts($producatorID)
    {
        $producatorID = (int)$producatorID;
        
        return $this->query("
            select pl.Name as `product`, sum(ocp.Quantity) as `quantity` from OrderCartProduct as ocp 
            right join ProductStock as ps on ocp.ProductStockID = ps.ID and ps.UserID = $producatorID 
            right join ProductLang as pl on pl.ProductID = ps.ProductID and pl.LangID = " . Request::getLangID() . " 
            where ps.UserID = $producatorID
            group by ps.ProductID 
            order by quantity desc 
            limit 10
        ");
    }
    
    public function mustAcvtiveClients($producatorID)
    {
        $producatorID = (int)$producatorID;
        
        $res = $this->query("
            select ui.UserID as userID, ui.Name as name, sum(ocp.Quantity) as quantity from OrderCartProduct as ocp 
            left join OrderCart as oc on oc.ID = ocp.OrderCartID 
            left join `Order` as o on o.ID = ocp.OrderCartID 
            right join ProductStock as ps on ocp.ProductStockID = ps.ID 
            right join ProductLang as pl on pl.ProductID = ps.ProductID and pl.LangID = " . Request::getLangID() . " 
            right join UserInfo as ui on ui.UserID = o.UserID 
            where ps.UserID = $producatorID
            group by o.UserID 
            order by quantity desc 
            limit 10
        ");
        
        $return = [];
        foreach ($res as $row)
        {
            $row['link'] = Url::link($row['userID'] . '-' . Url::strToUrl($row['name']));
            $return[] = $row;
        }
        
        return $return;
    }
    
    public function bigestProductStocks($producatorID)
    {
        $producatorID = (int)$producatorID;
        
        return $this->query("
            select pl.Name as `product`, sum(ps.Quantity - ps.OrderedQuantity) as quantity from ProductStock as ps 
            left join ProductLang as pl on pl.ProductID = ps.ProductID and pl.LangID = " . Request::getLangID() . " 
            where ps.UserID = $producatorID and (ps.`ExpirationDate` > now() || ps.Regular = 1) 
            group by ps.ProductID 
            order by quantity desc 
            limit 10
        ");
    }
    
}