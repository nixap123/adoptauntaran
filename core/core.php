<?php

    session_start();
    
    set_time_limit(120);
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/config/Config.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Request.php';
    
    Request::parse();
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Url.php';
    
    Url::detectDomain();
    
    // if GAL site
    if (!Url::$IsOriginalDomain)
    {
        
    }
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Validator.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Html.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Upload.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Geo.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/ArrayHelper.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/libraries/Lang.php';